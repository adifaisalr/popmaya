$(document).ready(function(){
   // ^zy 3 jan
   var cRootUrl = 'http://localhost:8888/ideafield/popmaya/'; 
   $(".submit_comment").click(function(){
     var wall_id = $(this).prev().val();
     var comment_wall = $(this).parent().find('.comment_item').val();
     var wall_form = $(this).parent().find('.comment_item');
     var list_el = $(this).parent().find('ul');

     if(comment_wall.length>0){
         $.ajax({
           type: "POST",
           url: cRootUrl + "member/asyncro/status_comment_process",
           async: true,
           cache: false,
           timeout:50000,
           data: {req_type: 'ajax', wall_itemID : wall_id, comment : comment_wall },
           success: function(data){
              list_el.append('<li>'+comment_wall+'</li>');
              wall_form.val(''); 
           },
           error: function(XMLHttpRequest, textStatus, errorThrown){
             alert(errorThrown);
           },
         });

     }else{
       alert('Anda belum menulis komentar');
     }
     return false;
   });
   // ^zy.end
   
	$(".tab_content").hide(); 

	$("#tab ul li.first").click(function() {    
        $("#tab ul li a").removeClass("active");
        $("#tab ul li.first a").addClass("active");
        $(".tab_content").hide();
        var activeTab = $(this).find("a").attr("href");
        arr=activeTab.split('#');
        activeTab='#'+arr[1];
        $(activeTab).fadeIn();
        return false;
      });

       $("#tab ul li.middle").click(function() {
        $("#tab ul li a").removeClass("active");
        $("#tab ul li.middle a").addClass("active");
        $(".tab_content").hide();
        var activeTab = $(this).find("a").attr("href");
        arr=activeTab.split('#');
        activeTab='#'+arr[1];
        $(activeTab).fadeIn();
        return false;
      });

       $("#tab ul li.last").click(function() {
          $("#tab ul li a").removeClass("active");
        $("#tab ul li.last a").addClass("active");
        $(".tab_content").hide();
        var activeTab = $(this).find("a").attr("href");
        arr=activeTab.split('#');
        activeTab='#'+arr[1];
        $(activeTab).fadeIn();
        return false;
      });	

      $(".input-share").click(function(){
        $(".share_icons").slideDown('fast');
      });
      $(".input-share").keyup(function(){
        $('.status_text').val($(this).val());
      });
      
      $(".share_status").click(function(){
        $("#overlay").remove();
        //$(".temp_share").html('');
        $(".temp_share").hide();
        $(".btn_cs").hide();
        $('.share_photo_box').hide();
        $(".input-share").fadeIn();
        $(".share_submit").fadeIn();
        return false;
      });
      
      $(".share_voice").click(function(){
        if($(".form_note").is(':visible')){
          $("#overlay").remove();
          $(".input-share").show();
        }
        if(!$('.btn-audio-record').hasClass('recording'))
        {
            $('.btn_start_record').show();
        }
        else
        {
           $('#audio-record').show();
        }
       
        $('.share_photo_box').hide();
        $('.share_box').hide();
        $(".share_voice_link").fadeIn();
        $(".input-share").fadeOut(function(){$(".temp_share").fadeIn();});

        $(".share_submit").after($('.btn_check_sound').html());
        $(".share_submit").hide();
        return false;
      });
      
      $(".share_photo").click(function(){
        if($(".form_note").is(':visible')){
          $("#overlay").remove();
          $(".input-share").show();
        }    
        
        $(".share_submit").hide();
        $('.share_box').hide();
        $(".share_photo_link").fadeIn()
        $('.share_photo_box').show();
        $(".btn_cs").hide();
        $(".input-share").fadeOut(function(){$(".temp_share").fadeIn();});
        return false;
      });
      $(".share_movie").click(function(){
        $("#overlay").remove();
        $('.share_photo_box').hide();
        $(".share_submit").hide();
        $('.share_box').hide();
        $(".share_video_link").fadeIn();
        $(".input-share").fadeOut();
        $(".btn_submit").fadeIn();
        return false;
      });
      var editor=false;
      $(".share_note").click(function(){
        if(!$(".form_note").is(':visible')){
          $(".share").before('<div id="overlay"></div>');
          $(".input-share").hide();
          $(".share_submit").hide();
          $('.share_photo_box').hide();
          $('.share_box').hide();
          $(".share_note_link").fadeIn()
          $(".btn_cs").hide();
          $(".btn_submit").hide();
          $(".temp_share").fadeIn();
          //enable wyswyg editor
          $('.wysiwyg').remove();
          $(".note_content").wysiwyg({
              iFrameClass: "wysiwyg-input",
               css: window.jsUrl+'/jwysiwyg/editor.css',
               initialContent:$(".note_content").val()
          });

        }

        return false;
      });

      //yohan code
      $.jRecorder(
         { 
            host : window.updateurl ,
            swf_path : window.jsUrl+'jRecorder/jRecorder.swf?type=latest&content_type=video&sinceid='+$(".status-list .status-id:first").text(),
            callback_activityTime:function (time)
            {
                $('.audio-time').html(time);
            },
            callback_activityLevel:function (level)
            {
                if(level == -1)
                {
                  $('.audio-level-on').css("width",  "0px");
                }
                else
                {
                  $('.audio-level-on').css("width", level+ "%");
                }
            },
            callback_error_recording:function(){
                alert('Can\'t record audio this time. Try again later.')
            },
            callback_stop_recording:function(){
                //replace the code below to fetch content from server and show on timeline
               
            }
         }
       );
       
       var noteupload = setUploader('note_file','note');
        
       var photoupload = setUploader('photo_file','note');

});

//Yohan Code
function setUploader(elid,type){
    return new qq.FileUploader({
            // pass the dom node (ex. $(selector)[0] for jQuery users)
            element: document.getElementById(elid),
            // path to server-side upload script
            action: window.uploadurl,
            allowedExtensions:['jpg', 'jpeg', 'png', 'gif'],
            sizeLimit:1024*1024,
            debug:true,
            multiple:false,
            params: {
                type: type
            },
            onComplete: function(id, fileName, res){
                $('#'+elid).replaceWith('<span id="'+elid+'_upload"><input type="hidden" name="filepath" class="filepath" value="'+res.filepath+'"/><span class="filename">'+fileName+'</span> <span class="remove" onclick="js:removeFile(\''+elid+'\',\''+type+'\');">remove</span></span>');
            }
        });
}
function removeFile(elid,type)
{
    var filepath=$('#'+elid+'_upload input').val();
    $.post(window.uploadurl, {type:'remove',filepath:filepath}, function(data){
        $('#'+elid+'_upload').replaceWith('<div id="'+elid+'"><noscript><p>Please enable JavaScript to use file uploader.</p></noscript></div>');
        setUploader(elid,type);
    }, 'json');
}
function submitPhoto(){
    var sinceid=$(".status-list .status-id:first").text();
    //filephoto var need to replace with function to upload the file first through iframe and pass the file location on server after upload to filephoto var
    var filephoto='';
    var params={type:"latest",content_type:"photo",since:sinceid,album_id:$(".box_upload_photo .album").val(),album_title:$(".box_upload_photo .album option:selected").text(),desc:$(".box_upload_photo .desc").val(),filepath:filephoto};
    submitShare(params);
    $(".box_upload_photo .album").val('')
    $(".box_upload_photo .desc").val('');
    $('.box_upload_photo').hide();
}

function submitNote(){
    $(".note_content").wysiwyg("save");
    var sinceid=$(".status-list .status-id:first").text();
    var imagepath='';
    
    var params={type:"latest",content_type:"note",since:sinceid,note:$(".note_content").val(),imaage_path:imagepath,tags:$('.tags').val()};
    
    if($('.draft').is(':checked'))
        param.draft=1;
    if($('.agree').is(':checked'))
        param.agree=1;
    
    if(params.text!=='')
    {
        submitShare(params);
        $(".note_content").wysiwyg("clear");
        $(".note_content,.tags,.draft,.agree").val("");
        $("#overlay").remove();
        $(".temp_share").html('');
        $(".temp_share").hide();
        $(".btn_cs").hide();
        $('.share_photo_box').hide();
        $(".input-share").fadeIn();
        $(".share_submit").hide();
        $(".share_icons").slideUp('fast');
    }
    else
        alert('Your note is empty.');
    return false;
}
function createAlbum(){
    var albumtitle=$('.box_create_album .title').val();
    var duplicate=false;
    $('.box_upload_photo .album option').each(function(index){
        if($(this).text().toLowerCase()===albumtitle.toLowerCase())
        {
            duplicate=true;
        }
    });
    if(duplicate)
    {
       alert('You already have same album title.'); 
    }
    else
    {
        $('.box_upload_photo .album').append('<option value="0" selected="true">'+albumtitle+'</option>');
        showUploadPhoto();
    }
}

function showUploadPhoto(){
    $('.box_upload_photo').toggle();
    $('.box_create_album').hide(); 
}

function showCreateAlbum(){
         $('.box_upload_photo').hide();
         $('.box_create_album').toggle();
}

function recordAudio(){
       $('.btn_start_record').hide();
       $('#audio-record').show();
       if(!$('.btn-audio-record').hasClass('recording'))
       {
           $.jRecorder.record(30);
           $('.btn-audio-record').addClass('recording');
       }
       else
       {
           $.jRecorder.stop();
           $('.btn-audio-record').removeClass('recording');
            //temp solution to simulate new content
           var sinceid=$(".status-list .status-id:first").text();
            var params={type:"latest",content_type:"audio",since:sinceid};
            submitShare(params);
            //end temp solution
           
        }
      
    }
/*
* Event binding on load more button. 
* It's load more update and append to latest element of update list.                 
*/                
$(".btn-more").click(function(){
  var lastid=$(".status-id:last").text();
  $.ajax({
      url: window.updateurl,
      data:{type:"more",before:lastid},
      dataType:"json",
      beforeSend: function() {
          $(".btn-more").text('Loading Content...');
      },
      success: function( data ) {
        $(".btn-more").text('Load More...');
        $.each(data.status, function(index,val){
            var newcontent='<li><div class="status-id">'+val.id+'</div><div class="author"><img src="'+val.author.thumbnail+'" alt="'+val.author.name+'"/><div class="author-name">'+val.author.name+'</div><span class="author-id">'+val.author.id+'</span></div><div class="status-content"><div class="status-text">'+val.content.text+'</div></div><div class="clear"></div></li>';
            $("ul.status-list").hide().append(newcontent).fadeIn();
        });
      }
    });
});

/*
* Event binding on share status button
* Posting ajax request to server and prepend latest update on before first element of update list
*/
$(".share_submit").click(function(e){
    var sinceid=$(".status-list .status-id:first").text();
    var params={type:"latest",content_type:"status",since:sinceid,text:$(".status_text").val(), who_profile:$(".who_profile").val()} ;
    if(params.text!=='')
    {
        submitShare(params);
        $(".status_text").val('');
        $('.input-share').val('');
		$('.who_profile').val('');
    }
    else
        alert('Your status is empty.');
    return false;
});


/*
 *function to submit video that triger from button onclick event. I don't know why the button can't binded with onclick event from jQuery
 */
function submitVideo(){
    var sinceid=$(".status-list .status-id:first").text();
    var videolink=$(".video_link").val();
    var isurl = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    var isvideourl=/http:\/\/(?:www.)?(vimeo|youtube).com\/(?:watch\?v=)?(.*?)(?:\z|&)/;
    if(!isurl.test(videolink))
        alert('The link is not in correct URL format.');
    else if(!isvideourl.test(videolink))
        alert("The link is not from Youtube or Vimeo");
    else{
        var params={type:"latest",content_type:"video",since:sinceid,videourl:videolink};
        submitShare(params);
        $(".video_link").val('');
    }
}

/*
 * Function to submit ajax request to server and prepend content based on latest update from server
 * params:json object that will pass to http request through $_POST and $_GET variable
 */
function submitShare(params)
{
    $.ajax({
      type:"POST",
      url: window.updateurl,
      data:params,
      dataType:"json",
      success: function( data ) {
         $.each(data.status, function(index,val){
            var content='';
            switch(val.content.type)
            {
                case "text":
                    content='<div class="status-text">'+val.content.text+'</div>';
                    break;
                case "video":
                    content='<div class="status-video"><iframe width="378" height="250" src="http://www.youtube.com/embed/'+val.content.videoid+'?wmode=transparent" frameborder="0" allowfullscreen></iframe></div>';
                    break;
                case "audio":
                    content='<div class="status-audio">Your audio still in convertion. Please wait a couple minutes to listen it.</div>';
                    break;
                    break;
                case "photo":
                    content='<div class="status-photo"><img src="'+val.content.photo_url+'"/><div class="desc">'+val.content.desc+'</div><div class="album">Posted in album: '+val.content.album_title+'</div></div>';
                    break;
                case "note":
                    content='<div class="status-note">'+val.content.note+'</div>';
                    break;
                    break;
            }
            var newcontent='<li><div class="status-id">'+val.id+'</div><div class="author"><img src="'+val.author.thumbnail+'" alt="'+val.author.name+'"/><div class="author-name">'+val.author.name+'</div><span class="author-id">'+val.author.id+'</span></div><div class="status-content">'+content+'</div><div class="clear"></div></li>';
            $("ul.status-list").prepend(newcontent);
        });
      }
    });
}

//end Yohan code
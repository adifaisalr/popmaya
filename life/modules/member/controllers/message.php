<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Message extends MX_Controller 
{ /* ^zy */
	var $user_id;
	var $limit_sidebar_friend_list;
	
		
   	function __construct()
	{
   		parent::__construct();

   		$this->load->library('form_validation');
   		$this->load->model('m_member', '', TRUE);
   		$this->load->model('m_message', '', TRUE);
   		$this->load->model('home/m_home', '', TRUE);

		  $this->user_id=$this->session->userdata('user_id');
		  
		  $this->config->load('my_config');
  		$this->limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
  		
   		$this->output->enable_profiler(TRUE);
		
   }

   	function index()
	{	
		if (!$this->ion_auth->logged_in()) //not logged in
		{
			//redirect to the login page
			redirect('home', 'refresh');
		}
		else //logged in
		{
			//redirect to the profile (my profile) page
			redirect("member/message/box", 'refresh');
		}
   	}
        
    function box(){ /* zy */
	  $data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js');
	    $data['foot_script']='
			var sics = $(".share_icons"); sics.hide();
		    $(\'#slider\').nivoSlider({speed:5000});

		    $("#tab ul li.middle a").addClass("active").show(); 
 	        $("#tab2").show();

 	        $(".conf_del").click(function(){
				var url = $(this).attr("href");
				var ttl = $(this).attr("title");
				if(confirm(ttl)) {
					location.href=url;
				} else {
					return false;
				}		
			});

		    ';	      
			
			$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');

			$row=$this->m_member->get_user_profile($this->user_id);
			$data['user_id']=$this->user_id;
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['up_uid']=$row->up_uid; // ^zy 4 jan
			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->user_id, $limit_sidebar_friend_list);
			$data['ispagephoto'] = false;


			//load ads data
			$data['ads_data']=$this->global_model->get_image_ads();			
			
			// display by uri segment type : inbox, sent, trash
			switch($this->uri->segment(4)){
				case 'inbox' : 
					$page_req='pm_message_idx'; 
					$data['msg_title']="Inbox";
					$whr_msg=array('a.isTrash'=>0, 'a.toID'=>$this->session->userdata('user_id'));
					$whr=$whr_msg;
					//nisa 2 jan 2011
					$this->m_message->read_inbox_notification($this->session->userdata('user_id'));
					break;
				case 'new' : 
					$page_req='pm_message_new';
					$data['msg_title']="Baru";
					$whr=$whr_msg=array('a.isTrash'=>0, 'a.toID'=>$this->session->userdata('user_id'));
					// load friends
					$data['list_to']=$this->m_message->list_friends('200');
					break;
				case 'sent' : 
					$page_req='pm_message_idx';
					$data['msg_title']="Sent"; 
					$whr_msg=array('a.isTrash'=>0, 'a.fromID'=>$this->session->userdata('user_id'));
					$whr=$whr_msg;
					break;
				case 'trash' : 
					$page_req='pm_message_idx'; 
					$data['msg_title']="Trash";
					$whr_msg=array('a.isTrash'=>1, 'a.toID'=>$this->session->userdata('user_id'));
					$whr=$whr_msg;
					break;	
				case 'read' :
					$page_req='pm_message_read';
					// update tr_notif n_isread = 1
					$this->global_model->notify_app('edit', array('n_itemid'=>$this->uri->segment(5)), array('n_isread'=>1));

					// update tr_message isRead = 1
					//$this->m_message->update_read($this->uri->segment(5));

					$whr=$whr_msg=array('a.isTrash'=>0, 'a.fromID'=>$this->session->userdata('user_id'));
					$quepa=$this->db->get_where('tr_message', array('message_contentID'=>$this->uri->segment(5)));
					$data['reply_to_id']=$quepa->row()->fromID;
					$data['mssg_read']=$this->m_message->read_mssg($this->uri->segment(5));
					break;	
			}


			$limit=10; // jumlah pesan yang ingin ditampilkan
			
		// offset
   	   	$uri_segment = 5;
        if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($uri_segment); }
        $data['offset']= $offset;

   		// generate pagination
   		$this->load->library('pagination');
   		$config['base_url'] = site_url().'member/message/box/'.$this->uri->segment(4);
    	$data['total_rows'] = $config['total_rows'] = $this->m_message->count_all($whr);
    	$config['per_page'] = $limit;
   		$config['uri_segment'] = $uri_segment;
   		$this->pagination->initialize($config);
   		$data['pagination'] = $this->pagination->create_links();

   		$data['list_messages']=$this->m_message->list_message($whr_msg, $offset, $limit);

		//render view
		$this->template->write('head_title', 'Message Inbox • Popmaya', true);
		$this->template->write('keywords', '');
	    $this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		$this->template->write_view('sidebar_left', 'track/pm_track_side', $data, '');
		$this->template->write_view('middle_content', $page_req, $data, '');
		$this->template->write_view('sidebar_right', 'track/pm_track_side_right', $data, '');
		$this->template->render();
	}

	function send(){		
		$insert_id = $this->m_message->send_mssg();
		if($insert_id){
			// get message content ID
			$ques = $this->db->get_where('tr_message', array('ID'=>$insert_id));
			$msg_ctt_id=$ques->row()->message_contentID; 
			// insert into notification
			$this->global_model->notify_app('ins', '', array('n_type'=>9, 'n_itemid'=>$msg_ctt_id, 'n_fromuserid'=>$this->session->userdata('user_id'), 'n_foruserid'=>$this->input->post('friend_id')));
			set_flash('warn', 'success', 'Pesan berhasil di kirim.');
		}else{
			set_flash('warn', 'notice', 'Pesan gagal di kirim.');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
        
    function delete_item($mid){
    	if($this->m_message->set_trash($mid)){
			set_flash('warn', 'success', 'Pesan berhasil di hapus.');
		}else{
			set_flash('warn', 'notice', 'Pesan gagal di hapus.');
		}
		redirect($_SERVER['HTTP_REFERER']);
    }
}
?>
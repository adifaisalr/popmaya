<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends MX_Controller 
{
	var $user_id;
	var $limit_sidebar_friend_list;
			
   	function __construct()
	{
   		parent::__construct();

   		$this->load->model('m_member', '', TRUE);
   		$this->load->model('home/m_home', '', TRUE);
		
		$this->load->helper('download');
   		$this->load->library('form_validation');

	  	$this->user_id=$this->session->userdata('user_id');
		  
		$this->config->load('my_config');
  		$this->limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
  		
   		$this->output->enable_profiler(TRUE);
		
   }

   	function index()
	{	
		if (!$this->ion_auth->logged_in()) //not logged in
		{
			//redirect to the login page
			redirect('home', 'refresh');
		}
		else //logged in
		{
			//redirect to the profile (my profile) page
			redirect("member/profile", 'refresh');
		}
   	}

	//------------------------------------------------------------------------------------------------------------------------wall
    function file_upload()
    {
        
        // list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $allowedExtensions = array();
        // max file size in bytes
        $sizeLimit = 10 * 1024 * 1024;
        $this->load->library('QqFileUploader',array('allowedExtensions'=>$allowedExtensions,'sizeLimit'=>$sizeLimit));
        $uploadpath=dirname(__FILE__).'/../../../../assets/media/';
        $result = $this->qqfileuploader->handleUpload($uploadpath);
        // to pass data through iframe you will need to encode all html tags
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
    }
    
    function get_video_id( $url ) {
        if( preg_match( '/http:\/\/youtu.be/', $url, $matches) ) {
        $url = parse_url($url, PHP_URL_PATH);
        $url = str_replace( '/', '', $url);
        return $url;

        } elseif ( preg_match( '/watch/', $url, $matches) ) {
        $arr = parse_url($url);
        $url = str_replace( 'v=', '', $arr['query'] );
        return $url;

        } elseif ( preg_match( '/http:\/\/www.youtube.com\/v/', $url, $matches) ) {
        $arr = parse_url($url);
        $url = str_replace( '/v/', '', $arr['path'] );
        return $url;

        } elseif ( preg_match( '/http:\/\/www.youtube.com\/embed/', $url, $matches) ) {
        $arr = parse_url($url);
        $url = str_replace( '/embed/', '', $arr['path'] );
        return $url;

        } elseif ( preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=[0-9]/)[^&\n]+|(?<=v=)[^&\n]+#", $url, $matches) ) {
        return $matches[0];

        } else {
        return false;
        }
    }
        
   	function status_update(){
                $pagesize=10;
                $type=$this->input->get_post('type');
                $updateType=$this->input->post('content_type');
								
                //check if request is from status update post
                if($updateType!==FALSE)
                {
                    //insert status in to database here
                    switch($updateType)
                    {
                        case 'status':
                            $textStatus=$this->input->post('text');
							$who_profile=$this->input->post('who_profile');
                            $contentUpdate=array(
                                        'type'=>'text',
                                        'text'=>$textStatus
                                    );
							//nisa 26 des, insert to database
							if($who_profile!='' || ($who_profile != $this->user_id))
							{
								$to_id=$who_profile;
							}
							else
							{
								$to_id='0';
							}
							$this->m_member->share_status($this->user_id, $to_id, $textStatus, $this->session->userdata('ip_address'));
							//end nisa 26 des
                            break;
                        case 'video':
                            $videourl=$this->input->post('videourl');
                            $videoid=$this->get_video_id($videourl);
                            $contentUpdate=array(
                                        'type'=>'video',
                                        'videoid'=>$videoid
                                    );
                            break;
                        case 'audio':
                            $contentUpdate=array(
                                        'type'=>'audio',
                                    );
                            break;
                            break;
                        case 'photo':
                            $album_id=$this->input->post('album_id');
                            //check if album is not exist yet
                            if($album_id===0)
                            {
                                //create new album here and replace album_id with the id from db
                                $album_id=10;
                            }
                            $album_title=$this->input->post('album_title');
                            $filepath=$videourl=$this->input->post('filepath');
                            $desc=$this->input->post('desc');
                            $contentUpdate=array(
                                'type'=>'photo',
                                'photo_url'=>'http://www.starpulseonline.com/images/celebrity/coldplay.jpg',
                                'album_id'=>$album_id,
                                'album_title'=>$album_title,
                                'desc'=>$desc
                            );
                            break;
                        case 'note':
                            $note=$this->input->post('note');
                            $contentUpdate=array(
                                        'type'=>'note',
                                        'note'=>$note
                                    );
							//nisa 26 des, insert to database
							$this->m_member->share_note($this->user_id, 'Note', $note, $this->session->userdata('ip_address'));
							//end nisa 26 des						
                            break;
                    }
                    
                }
                
                //check if request not from status update post
                if($type==='latest')
                {
                    $since=$this->input->get_post('since');
                    
                    if($since===FALSE)
                    {
                        //load all latest update from db using page size above
                    }
                    else
                    {
                        //load only latest update since latest id
                        
                        echo json_encode(array('sinceid'=>$since,
                            'status'=>
                            array(
                                //load from database and for each status create an array like below
                                array(
                                    'id'=>0,
                                    'author'=>array(
                                      'id'=>3,
                                      'name'=>'Zufrizal Yordan',
                                      'thumbnail'=>image_asset_url('cd_thumb.jpg'),
                                    ),
                                    'content'=>$contentUpdate
                                )
                            )
                        ));

                    }
                }
                else if($type==='more')
                {
                    $after=$this->input->get('before');

					$this->load->config('my_config');
					$limit_load_more=$this->config->item('limit_load_more');
					
					$walls=$this->m_member->get_more_wall($limit_load_more, $this->user_id);

                    //load all updates after ID paging by page size
					$i=0;
					foreach($walls as $row)
					{
						$path=$this->m_member->get_profile_path($this->user_id);
						
						$status[$i]=array(
                              'author'=>array(
	                              'id'=>$row->memberID,
	                              'name'=>$row->up_name,
	                              'thumbnail'=>$path."/".$row->memberID.".jpg",
                            	),
                            	'content'=>array(
	                                'type'=>'text',
	                                'text'=>$row->info
                            	)
                        );
						$i++;
					}					

					//send to html
                    echo json_encode(array('beforeid'=>$after,
                            'status'=>$status
                        ));
                }
                
                
   	}

	/* Nisa. 26 Des 2011. beri komentar pada status */
	function status_comment_process()
	{
		$this->m_member->status_comment($this->input->post('comment'), $this->input->post('wall_itemID'), $this->user_id, $this->session->userdata('ip_address'));
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}	
	
	/* Nisa. 26 Des 2011. like status */
	function status_like_process($wall_id)
	{
		$this->m_member->status_like($wall_id, $this->user_id);
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}
	
	/* Nisa. 26 Des 2011. hapus status */
	function status_delete_process($wall_id)
	{
		$this->m_member->status_delete($wall_id);
		redirect($_SERVER['HTTP_REFERER'], 'refresh');		
	}
	//------------------------------------------------------------------------------------------------------------------------end wall
   	
	//------------------------------------------------------------------------------------------------------------------------profile
	function profile($id='',$optional='')
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}

		if(strcmp($this->uri->segment(3),"playlist")==0 || strcmp($id,"")==0 || strcmp($id,"playlist_detail")==0){
			$id=$this->user_id;
		}


		//setting up viewer
		$page_req="pm_profile_idx";
		
		
		$data['foot_js']=css_asset('add_style.css').css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').  js_asset('jwysiwyg/jquery.wysiwyg.js').js_asset('jRecorder/jRecorder.js').js_asset('fileuploader.js').js_asset('jPlayer/jquery.jplayer.min.js');
      	$data['sound_js'] = js_asset('sound.js');
		$data['foot_script']='
	        var sics = $(".share_icons"); sics.hide();
	        $(\'#slider\').nivoSlider({speed:5000});

	        $("#tab ul li.first a").addClass("active").show(); 
	          	$("#tab1").show();
	        
	        /* ^zy */
			var the_player = $("#song_player");

			/* ^zy */
			 	var the_player = $("#song_player");

			 	the_player.jPlayer({
			 	    ready: function(){
						console.log("player ready."); 	           			
			 	    },
			 	    play: function(){
			 	    	console.log("play file.");	
			 	    },
			 	    volume: "0.4",
			 	    swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});           	

				var prev_val=0;
			 	$("#wrapper .btn_play").click(function(e){
			 	    the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
				});

				$(".add_to_playlist").click(function(){
					$.post("'.site_url().'track/add_to_playlist", { track_id: $(this).attr("href") }, function(data) {
   						$(".list-left p.bottom").html();
   						alert(data);
   						$(".list-left p.bottom").html("Data berhasil di tambahkan."); /* harusnya disini load content ajax utk playlist */
 					});
					return false;	
				});
			/* ^zy.end */
	      
	      ';	      
		$data['banner_image'] = $this->m_member->get_image_banner_profile();
		if($id=='') //my profile
		{
			$row=$this->m_member->get_user_profile($this->user_id);
		}
		else //other member profile
		{
			$row=$this->m_member->get_user_profile($id);
		}
		
		//setting data
		if($this->user_id==$id)
		{
			$data['user_id']=$this->user_id;
			$data['image_path']=$this->m_member->get_profile_path($this->user_id);
		}
		else
		{
			$data['user_id']=$row->up_uid;
			$data['image_path']=$this->m_member->get_profile_path($row->up_uid);
			
			$data['my_user_id']=$this->user_id;
			$data['my_name']=$this->m_member->get_user_profile($this->user_id)->up_name;
		}
		$data['name']=$row->up_name;
		$data['alias']=$row->up_alias;		
		
		$data['gender_code']=$row->up_gender;
		$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
		$data['city']=$row->up_city;
		$data['country']=$row->up_country;
		
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
		
		//tambahan 2 Januari 2012
		//adi
		//get artist by member
		$data['artist_list']=$this->m_member->get_artist_member($row->up_uid, 3);
		$data['count_my_artist']=$this->m_member->get_count_artist_member($row->up_uid);
			
		$data['up_uid']=$row->up_uid; // ^zy 4 jan

		// ini temporary fave song, harusnya pake yang di atas, kalo si create artist udah beres
		//$data['song_list']=$this->m_member->get_fave($this->user_id, $limit);	
		
		$data['recommended_track']=$this->m_home->get_recommended_track();
		$new_track_limit=$this->config->item('limit_new_track');
		$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			
	
		$data['num_playlist_like'] = 0;
		if(strcmp($id,$this->user_id)==0){
			$data['is_my_playlist'] = true;
		}else{
			$data['is_my_playlist'] = false;
		}
		$data['is_detail_playlist'] = false;
		$data['is_edit_playlist'] = false;
		if(strcmp($id,$this->user_id)==0 && ($this->input->post("playlist_name") || strcmp($this->uri->segment(3),"playlist")==0)){
			
			$data['foot_script']='
				var sics = $(".share_icons"); sics.hide();
				$(\'#slider\').nivoSlider({speed:5000});

				$("#tab ul li.last a").addClass("active").show(); 
				$("#tab3").show();
				';	
			if($this->input->post("playlist_name")){
				//echo "Edit playlist";
				
				$playlist = array(
					'playlist_name' => $this->input->post('playlist_name') ,
					'user_id' => $this->user_id					
				);
				$data['last_insert_id'] = $this->m_member->save_playlist($playlist);
				redirect('member/profile/playlist/'.$data['last_insert_id']);
			}
			$data['is_edit_playlist'] = true;
			$data['last_insert_id'] = $this->uri->segment(4);				
			//echo "Edit playlist";
			$data['list_songs'] = $this->m_member->get_all_song();
			$data['one_playlist'] = $this->m_member->get_one_playlist($this->uri->segment(4));
		}
		
		if(strcmp($this->uri->segment(3),"playlist_detail")==0){
			$data['foot_script']='
				var sics = $(".share_icons"); sics.hide();
				$(\'#slider\').nivoSlider({speed:5000});

				$("#tab ul li.last a").addClass("active").show(); 
				$("#tab3").show();
				';	
			$data['num_playlist_like'] = $this->m_member->get_num_like_playlist($this->uri->segment(4));
			$data['is_detail_playlist'] = true;
			$data['one_playlist'] = $this->m_member->get_one_playlist($this->uri->segment(4));
			
		}
		//load user playlist
		$data['user_playlist'] = $this->m_member->get_user_playlist($id);
		//print_r($data['user_playlist']);
		
		$data['ispagephoto'] = false;
		
		//nisa 26 des 
		$type_text=$this->uri->segment(4);
		if($type_text=='status') $type='1';
		else if($type_text=='voice') $type='2';
		else if($type_text=='video') $type='4';
		else if($type_text=='note') $type='5';
		else $type='';
		
		$data['texts']=$this->m_member->get_status_update($row->up_uid, $type);
		$data['text_comments']=$this->m_member->get_status_comment($row->up_uid);			
		//end nisa 26 des			
				
			//ditambah oleh julian 3 januari 2012
		$data['comment_display']=$this->m_member->get_playlist_comment($this->uri->segment(4));
		
		//render view
		$this->template->write('head_title', 'Profile');
		$this->template->write('keywords', '');
//		$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		$this->template->write_view('sidebar_left', 'pm_side_newbie', $data, '');
		$this->template->write_view('middle_content', 'pm_profile_idx', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();	      			
	} 
	
	function like_playlist($id=""){
		$like = array(
			'playlist_id' => $this->uri->segment(3) ,
			'member_id' => $this->user_id				
		);
		
		$this->m_member->like_playlist($like);
		redirect('member/profile/playlist_detail/'.$this->uri->segment(3),'refresh');
	}
	
	function add_playlist($param){
		
		$playlist = array(
			'playlist_id' => $this->uri->segment(4) ,
			'song_id' => $this->uri->segment(3)				
		);
		
		$this->m_member->add_song_playlist($playlist);
	}
	
	function delete_playlist($id){
		$this->m_member->delete_playlist($id);
		redirect("member/profile","refresh");
	}
	
	function copy_playlist($param){
		$copied_playlist_id =  $this->uri->segment(3);
		$copied_playlist = $this->m_member->get_one_playlist($copied_playlist_id);
		$song=0;
		$last_id=0;
		foreach($copied_playlist as $new_playlist){
			if($song==0){
				//create playlist
				$playlist = array(
					'playlist_name' => $new_playlist->playlist_name." Copied" ,
					'user_id' => $this->user_id					
				);
				$last_id = $this->m_member->save_playlist($playlist);
			}
			$new_song = array(
				'playlist_id' => $last_id ,
				'song_id' => $new_playlist->song_id					
			);
			$this->m_member->add_song_playlist($new_song);
			$song++;
		}
		//$inserted_id = $this->m_member->save_playlist($playlist);
	}
	
	function remove_song_playlist($param){	
		//echo "delete ".$this->uri->segment(3)." , ".$this->uri->segment(4);
		$this->m_member->delete_song_playlist($this->uri->segment(3),$this->uri->segment(4));
	}
  
  	function profile_edit()
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}

   		$this->load->library('form_validation');
		$this->load->helper('form');
		
		//setting up viewer
		$page_req="pm_profile_idx";
		$data['foot_js']=js_asset('jquery.imgareaselect.pack.js');
      	$data['foot_script']="
			function preview(img, selection) {
			    if (!selection.width || !selection.height)
			        return;

			    var scaleX = 100 / selection.width;
			    var scaleY = 100 / selection.height;

			    $('#preview img').css({
			        width: Math.round(scaleX * 300),
			        height: Math.round(scaleY * 300),
			        marginLeft: -Math.round(scaleX * selection.x1),
			        marginTop: -Math.round(scaleY * selection.y1)
			    }); 
			}

			$(function () {
			    $('#photo').imgAreaSelect({ aspectRatio: '1:1', handles: true,
			        fadeSpeed: 200, onSelectChange: preview });
			});	      
		";		
		//setting data
		$row=$this->m_member->get_user_profile($this->user_id);
		
		//sidebar
		$data['user_id']=$this->user_id;
		$data['image_path']=$this->m_member->get_profile_path($this->user_id);
		$data['name']=$row->up_name;
		$data['alias']=$row->up_alias;	

		$data['gender_code']=$row->up_gender;
		$data['gender']=$row->up_gender=='M'? "Male" : "Female";
		$data['city']=$row->up_city;
		$data['country']=$row->up_country;
		$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
		$data['ispagephoto'] = false;
		
		//tambahan 2 Januari 2012
		//adi
		//get artist by member
		$data['artist_list']=$this->m_member->get_artist_member($this->user_id, 3);

		//sidebar, my song list
  		$limit=$this->config->item('limit_sidebar_fave_song_list');
  		$data['song_list']=$this->m_member->get_fav_song($this->user_id, $limit);		

  		//music sidebar, suggest song drop down menu		
  		$fav_genre=$this->m_member->get_fav_genre($this->user_id);
  		$fav_genre=explode(", ", $fav_genre);
  		$genres=$this->m_member->get_genre();
  		$i=0;
  		$fav_genre_display='';

		if($fav_genre[0]) //have favourite genre
		{
			foreach($genres as $g)
			{
				if($g->g_id==$fav_genre[$i])
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($genres as $g)
			{
				$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
				$i++;
			}				
		}	
		$data['suggest_song_option']=$fav_genre_display;
		$genre_id=$fav_genre_display[0]['id'];
  		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
  		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);

		//personal info
		$data['last_name']=$row->up_lastname;
		$data['email']=$row->ul_email;
		$data['email_type']=$row->up_emailhide;
		$data['nick_name']=$row->up_nickname;
		$data['nick_name_type']=$row->up_nicknamehide;
		$data['mobile']=$row->up_mobile;
		$data['mobile_type']=$row->up_mobilehide;		
		$data['total_point_privacy_type']=$row->up_totalpointprivacy;				
		$data['gender_type']=$row->up_genderhide;		
		
		//birthday
		$birthday=explode("-", $row->up_birthday);		
		$data['birth_month']=$birthday[1];
		$data['birth_date']=$birthday[2];
		$data['birth_year']=$birthday[0];
		$data['birthday_type']=$row->up_birthyearhide;
		
		//status
		$data['statuses']=$this->m_member->get_status();
		$data['status']=$row->up_status;
		$data['status_type']=$row->up_statushide;
		
		//country
		$data['countries']=$this->m_member->get_country();
		$data['country_code']=$row->up_countrycode;
		$data['states']=$this->m_member->get_state();
		$data['state_code']=$row->up_state;
		$data['cities']=$this->m_member->get_city();
		$data['city_code']=$row->up_cityid;
		$data['address']=$row->up_address;
		$data['zip_code']=$row->up_zipcode;
		$data['zip_code_type']=$row->up_zipcodehide;
		
		//work
		$row1=$this->m_member->get_user_work($this->user_id);
		if($row1!='kosong')
		{
			$data['occupation']=$row1->uw_occupation;
			$data['occupation_type']=$row1->uw_occupationhide;
			$data['companies']=$row1->uw_companies;
			$data['companies_type']=$row1->uw_companieshide;
			$data['school']=$row1->uw_school;
			$data['school_type']=$row1->uw_schoolhide;
		}
		else
		{
			$data['occupation']="";
			$data['occupation_type']=1;
			$data['companies']="";
			$data['companies_type']=1;
			$data['school']="";
			$data['school_type']=1;			
		}
		
		//music
		$data['genres']=$this->m_member->get_genre();
		$data['fav_genre']=explode(", ", $row->up_favgenre);
		$data['genre_type']=$row->up_favgenrehide;
		$data['fav_artist']=$row->up_favartist;
		$data['fav_artist_type']=$row->up_favartisthide;
		$data['system_mail']=$row->up_sendmailalert;
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
		$data['count_my_artist']=$this->m_member->get_count_artist_member($row->up_uid);
		//render view
		$this->template->write('head_title', 'Profile');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_side_newbie', $data, '');
		$this->template->write_view('middle_content', 'pm_profile_edit', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();
	}
  
  	function profile_edit_process()
	{
   		$this->load->library('form_validation');
		$this->load->helper('form');

		//validation rules
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean|min_length[5]|max_length[20]');
    	$this->form_validation->set_rules('email', 'Email', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|valid_email|callback_new_email|min_length[5]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('old_password', 'Old Password', 'trim|alpha_numeric|prep_for_form|htmlspecialchars|encode_php_tags|min_length[5]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('new_password', 'New Password', 'trim|alpha_numeric|prep_for_form|htmlspecialchars|encode_php_tags|min_length[5]|max_length[20]|xss_clean|matches[conf_password]');
		$this->form_validation->set_rules('conf_password', 'Confirm Password', 'trim|alpha_numeric|prep_for_form|htmlspecialchars|encode_php_tags|min_length[5]|max_length[20]|xss_clean|matches[new_password]');
		$this->form_validation->set_rules('nick_name', 'Nick Name', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('address', 'Address', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('zip_code', 'Zip Code', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean');		
    	
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
    	$this->form_validation->set_message('alpha_numeric', 'Only alphanumeric characters are allowed.');
		$this->form_validation->set_message('valid_email', 'Wrong email format.');
    	$this->form_validation->set_message('min_length[5]', 'Min. character is 5 chars.');    	
    	$this->form_validation->set_message('max_length[20]', 'Max. character is 20 chars.');
    	$this->form_validation->set_message('max_length[50]', 'Max. character is 50 chars.');
		
		$this->form_validation->set_error_delimiters('','<br />');
		
		if ($this->form_validation->run() == true)
		{
			if($this->input->post('new_password'))
			{
				//get password and salt
				$pass=$this->m_member->get_user_password($this->user_id);
				$my_pass=$pass->ul_pwd;
				$input_pass=$this->input->post('old_password');
				$salt_pass=$pass->ul_salt;
				$input_pass_with_salt=$this->ion_auth->hash_password($input_pass, $salt_pass);
				
				if($input_pass_with_salt != $my_pass) //old password doesn't match
				{
					$this->session->set_flashdata('message', "Edit profile failed, old password doesn't match");
					redirect('member/profile_edit');											
				}
				else //change password
				{
					$new_pass=$this->input->post('new_password');
					$new_pass_with_salt=$this->ion_auth->hash_password($new_pass, $salt_pass);				
					$this->m_member->update_user_password($this->user_id, $new_pass_with_salt);					
				}
			}
						
			$birthday=$this->input->post('birth_year')."-".$this->input->post('birth_month')."-".$this->input->post('birth_date');
			$city=$this->m_member->get_city_name($this->input->post('city'));
			$country=$this->m_member->get_contry_name($this->input->post('country'));
			$favgenre=$this->input->post('fav_genre');
			$favegenres="";
			for($i=0; $i < count($favgenre); $i++)
			{
				$favegenres.=$favgenre[$i].", ";
			}
			
			$sendmailalert=$this->input->post('system_mail');
			$sendmailalert=$sendmailalert[0];
			
			$data_update=array(
				'name' => $this->input->post('first_name'),
				'lastname' => $this->input->post('last_name'),
				'nickname' => $this->input->post('nick_name'),
				'nicknamehide' => $this->input->post('nick_name_type'),
				'mobile' => $this->input->post('mobile'),
				'mobilehide' => $this->input->post('mobile_type'),
				'gender' => $this->input->post('gender'),
				'genderhide' => $this->input->post('gender_type'),
				'birthday' => $birthday,
				'status' => $this->input->post('status'),
				'statushide' => $this->input->post('status_type'),
				'email' => $this->input->post('email'),
				'emailhide' => $this->input->post('email_type'),
				'birthyearhide' => $this->input->post('birthday_type'),
				'sendmailalert' => $sendmailalert,
				'city' => $city,
				'state' => $this->input->post('state'),
				'country' => $country,
				'cityid' => $this->input->post('city'),
				'countrycode'=> $this->input->post('country'),
				'address' => $this->input->post('address'),
				'zipcode' => $this->input->post('zip_code'),
				'zipcodehide' => $this->input->post('zip_code_type'),
				'totalpointprivacy' => $this->input->post('total_point_privacy_type'),
				'favgenre' => $favegenres,
				'favgenrehide' => $this->input->post('fav_genre_type'),
				'favartist' => $this->input->post('fav_artist'),
				'favartisthide' => $this->input->post('fav_artist_type'),				
				'occupation' => $this->input->post('occupation'),
				'occupationhide' => $this->input->post('occupation_type'),
				'companies' => $this->input->post('companies'),
				'companieshide' => $this->input->post('companies_type'),
				'school' => $this->input->post('school'),
				'schoolhide' => $this->input->post('school_type')
				);			
			$this->m_member->update_user_profile($data_update, $this->user_id);

			$this->session->set_flashdata('message', "Edit profile success");
			redirect('member/profile_edit');						
		}
		else
		{
			$this->session->set_flashdata('message', validation_errors());
			redirect('member/profile_edit');			
		}
	}
	
	/*
	Nama: Nisa
	Tanggal: 6 Des 2011
	Deskripsi: Upload photo
	*/	
	function photo_save()
	{
		$row=$this->m_member->get_profile_path($this->user_id);
		$arr=explode('/', $row);
		$year=$arr[0];
		$month=$arr[1];
		$day=$arr[2];
		
		//create directory	
		$dir="./assets/images/profile/".$year."/";
		if(!is_dir($dir)) {mkdir($dir, 0755);}
		$dir="./assets/images/profile/".$year."/".$month."/";
		if(!is_dir($dir)) {mkdir($dir, 0755);}
		$dir="./assets/images/profile/".$year."/".$month."/".$day;
		if(!is_dir($dir)) {mkdir($dir, 0755);}
		
		$config['upload_path']="./assets/images/profile/".$year."/".$month."/".$day;
		$config['file_name']=$this->user_id;
		$config['allowed_types']='jpg';
		$config['max_size']='300';
		$config['max_width']='950';
		$config['max_height']='950';
		$config['overwrite']=TRUE;
		
		//upload
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload())
		{
			$this->session->set_flashdata('message', $this->upload->display_errors());
			redirect('member/profile_edit');						
		}	
		else
		{
			$data=array('upload_data'=>$this->upload->data());

			//thumbnailining
	        $thumb_config['width']=184;
	        $thumb_config['height']=184;
	        $thumb_config['master_dim']='width';
	        $thumb_config['create_thumb']=TRUE;
	        $thumb_config['maintain_ratio']=TRUE;
	       	$thumb_config['source_image']="./assets/images/profile/".$year."/".$month."/".$day."/".$this->user_id.".jpg";

	        $this->load->library('image_lib', $thumb_config);	

	        if (!$this->image_lib->resize())
	        {
				$this->session->set_flashdata('message', "Can not create photo thumbnail");
				redirect('member/profile_edit');						
	        }
			else
			{
				$this->session->set_flashdata('message', "Upload profile picture success");
				redirect('member/profile_edit');						
			}
			//end thumbniling
		}
		//end upload
	}
	
	function inbox(){ /* zy */
	  $data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js');
	    $data['foot_script']='
			var sics = $(".share_icons"); sics.hide();
		    $(\'#slider\').nivoSlider({speed:5000});

		    $("#tab ul li.middle a").addClass("active").show(); 
 	        $("#tab2").show();
		    ';	      
			
			$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');

			$row=$this->m_member->get_user_profile($this->user_id);
			$data['user_id']=$this->user_id;
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
			$data['count_notification']=$this->m_member->get_count_notification($this->user_id);

			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->user_id, $limit_sidebar_friend_list);
			$data['ispagephoto'] = false;
			//render view
			$this->template->write('head_title', 'Message Inbox • Popmaya', true);
			$this->template->write('keywords', '');
	    $this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		  $this->template->write_view('sidebar_left', 'track/pm_track_side', $data, '');
			$this->template->write_view('middle_content', 'pm_message_idx', $data, '');
			$this->template->write_view('sidebar_right', 'track/pm_track_side_right', $data, '');
			$this->template->render();
	}

	function profile_info($id=''){
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}
		else
		{
			//setting up viewer
			$page_req="pm_profile_idx";
			$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').  js_asset('jwysiwyg/jquery.wysiwyg.js').js_asset('jRecorder/jRecorder.js').js_asset('fileuploader.js').js_asset('jPlayer/jquery.jplayer.min.js');
			$data['foot_script']='
				var sics = $(".share_icons"); sics.hide();
				$(\'#slider\').nivoSlider({speed:5000});

				$("#tab ul li.middle a").addClass("active").show(); 
				$("#tab2").show();
				/* ^zy */
				var the_player = $("#song_player");
				
				the_player.jPlayer({
					ready: function(){
						console.log("player ready."); 	           			
					},
					play: function(){
						console.log("play file.");	
					},
					volume: "0.4",
					swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});

				var prev_val=0;
				$("#wrapper .btn_play").click(function(e){
					the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
				});
				/* ^zy.end */
			  ';	      

			if($id=='') //my profile
			{
				$row=$this->m_member->get_user_profile($this->user_id);
				//$row2=$this->m_member->get_user_work($this->user_id);
			}
			else //other member profile
			{
				$row=$this->m_member->get_user_profile($id);
				$row2=$this->m_member->get_user_work($id);
				//$row3=$this->m_member->get_fav_genre($id)
			}
			
			//setting data
			$data['ispagephoto'] = false;
			$data['user_id']=$this->user_id;
			$data['image_path']=$this->m_member->get_profile_path($this->user_id);
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['phone']=$row->up_mobile;
			$data['birthday']=$row->up_birthday;
			$data['status_code']=$row->up_status;
			$data['status']=($row->up_status==1) ? 'Married' : 'Single';
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
			$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
			
			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['state']=$row->up_state;
			$data['address']=$row->up_address;
			$data['zip_code']=$row->up_zipcode;
			if($row2!='kosong')
			{
				$data['occupation']=$row2->uw_occupation;
				$data['companies']=$row2->uw_companies;
				$data['school']=$row2->uw_school;
			}
			else
			{
				$data['occupation']="";
				$data['companies']="";
				$data['school']="";				
			}
			//$data['genres']=$this->m_member->get_fav_genre($id);
			$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
			
			//music sidebar, my song list
		$limit=$this->config->item('limit_sidebar_fave_song_list');
		$data['song_list']=$this->m_member->get_fav_song($this->user_id, $limit);		

		//music sidebar, suggest song drop down menu		
		$fav_genre=$this->m_member->get_fav_genre($this->user_id);
		$fav_genre=explode(", ", $fav_genre);
		$fav_artist=$this->m_member->get_fav_artist($this->user_id);
		$fav_artist=explode(", ", $fav_artist);
		
		$genres=$this->m_member->get_genre();
		$artists=$this->m_member->get_artist();
		$i=0;
		$fav_genre_display='';
		$fav_genre_string='';
		$fav_artist_display='';
		$fav_artist_string='';

		if($fav_genre[0]) //have favourite genre
		{
			foreach($genres as $g)
			{
				if($g->g_id==$fav_genre[$i])
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$fav_genre_string .= $g->g_name.',';
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($genres as $g)
			{
				$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
				$fav_genre_string .= $g->g_name.',';
				$i++;
			}				
		}	
		  $data['suggest_song_option']=$fav_genre_display;
			$data['fav_genre_string']=$fav_genre_string;
			
		$i=0;	
		if($fav_artist[0]) //have favourite artist
		{
			foreach($artists as $a)
			{
				if($a->ID==$fav_artist[$i])
				{
					$fav_artist_display[$i]=array('id'=>$a->ID, 'name'=>$a->artist_name);
					$fav_artist_string .= $a->artist_name.',';
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($artists as $a)
			{
				$fav_artist_display[$i]=array('id'=>$a->ID, 'name'=>$a->artist_name);
				$fav_artist_string .= $a->artist_name.',';
				$i++;
			}				
		}	
		
			$data['fav_artist_string']=$fav_artist_string;

		  $genre_id=$fav_genre_display[0]['id'];
		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);
		
		$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			
   			$data['artist_list']=$this->m_member->get_artist_member($row->up_uid, 3);
			$data['count_my_artist']=$this->m_member->get_count_artist_member($row->up_uid);
			
			$data['recommend_friends']=$this->m_member->get_recommendation($row->up_uid, 5);

			//render view
			$this->template->write('head_title', 'Profile');
			$this->template->write('keywords', '');
		$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		  $this->template->write_view('sidebar_left', 'pm_side_newbie', $data, '');
			$this->template->write_view('middle_content', 'pm_profile_info', $data, '');
			$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
			$this->template->render();	      			
		}
	}
	
	function save_artist($data){
		$data_insert = array('ID' => null,
				'artist_name' => $data['name'],
				'title' => $data['title'],
				'memberID' => $data['memberID'],
				'pageID' => 0,
				'type' => $data['category'],
				'status' => 0,
				'post' => 0,
				'ref' => ''
		);
		//echo $data_insert['artist_name'];
		$this->db->insert('tr_member_artist',$data_insert);
		

	}
	
	//(nova) 21-12-2011
	function add_friend($id) {
		$data_insert = array(
				'f_userid' => $this->user_id,
				'f_friendid' => $id,
				'f_isactive' => 0
		);
		$query = $this->db->get_where('tr_friend',$data_insert);
		if(! $query->row()->f_id) {
			$this->db->insert('tr_friend',$data_insert);
			$data_not = array(
				'n_type' => 8,
				'n_fromuserid' => $this->user_id,
				'n_foruserid' => $id
			);
			$this->db->insert('tr_notification',$data_not);			
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	function cancel_friend($id) {
		$this->db->delete('tr_friend', array('f_id' => $id));
		
		$data_not = array(
			'n_type' => 8,
			'n_fromuserid' => $this->user_id,
			'n_foruserid' => $id
		);

		$this->db->delete('tr_notification', array('n_id' => $id));
		
		//echo "gagal";
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	/* Nisa. 25 Des 2011. Friend request */
	function friend_request()
	{
		//top content
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
		$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
		$data['user_id']=$this->user_id;
		$data['name']=$this->m_member->get_user_name($this->user_id);
		
		//left content
		$row=$this->m_member->get_user_profile($this->user_id);
		$data['user_id']=$this->user_id;
		$data['image_path']=$this->m_member->get_profile_path($this->user_id);
		$data['name']=$row->up_name;
		$data['alias']=$row->up_alias;	
		$data['gender_code']=$row->up_gender;
		$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
		$data['city']=$row->up_city;
		$data['country']=$row->up_country;
		$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
		$data['ispagephoto'] = false;
		
		//music sidebar, my song list
		$limit=$this->config->item('limit_sidebar_fave_song_list');
		$data['song_list']=$this->m_member->get_fav_song($this->user_id, $limit);		
		
		//music sidebar, suggest song drop down menu		
		$fav_genre=$this->m_member->get_fav_genre($this->user_id);
		$fav_genre=explode(", ", $fav_genre);
		$genres=$this->m_member->get_genre();
		$i=0;
		
		if($fav_genre[0]) //have favourite genre
		{
			foreach($genres as $g)
			{
				if($g->g_id==$fav_genre[$i])
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($genres as $g)
			{
				$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
				$i++;
			}				
		}	
		$data['suggest_song_option']=$fav_genre_display;

		//music sidebar, suggest song
		$genre_id=$fav_genre_display[0]['id'];
		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);		
		
		//middle content
		$data['friend_request']=$this->m_member->get_friend_request($this->user_id);
		$data['count_all_friend_request']=$this->m_member->get_count_all_friend_request($this->user_id);
		$data['count_friends']=$this->m_member->get_count_friends($this->user_id);
		$data['friends_all']=$this->m_member->get_friends($this->user_id, 0);		
		$this->m_member->read_friend_notification($this->user_id); 
		//end middle content
		
		//right content
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
		$data['artist_list']=$this->m_member->get_artist_member($row->up_uid, 3);
		$data['count_my_artist']=$this->m_member->get_count_artist_member($row->up_uid);
		
		//render view
		$this->template->write('head_title', 'Friend Request');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_side_newbie', $data, '');
		$this->template->write_view('middle_content', 'pm_friend_request', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();
	}
	
	/* Nisa. 25 Des 2011. Friend approve process */
	function friend_approve_process($friend_id)
	{
		$this->m_member->friend_approve_process($this->user_id, $friend_id);
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	/* Nisa. 3 Jan 2012. Friend list */
	function friend_list($member_id)
	{
		//top content
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
		$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
		$data['my_user_id']=$this->user_id;
		$data['my_name']=$this->m_member->get_user_profile($this->user_id)->up_name;
		
		//left content
		$row=$this->m_member->get_user_profile($member_id);
		$data['user_id']=$member_id;
		$data['image_path']=$this->m_member->get_profile_path($member_id);
		$data['name']=$row->up_name;
		$data['alias']=$row->up_alias;	
		$data['gender_code']=$row->up_gender;
		$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
		$data['city']=$row->up_city;
		$data['country']=$row->up_country;
		$data['friends']=$this->m_member->get_friends($member_id, $this->limit_sidebar_friend_list);
		$data['ispagephoto'] = false;
		
		//music sidebar, my song list
		$limit=$this->config->item('limit_sidebar_fave_song_list');
		$data['song_list']=$this->m_member->get_fav_song($member_id, $limit);		
		
		//music sidebar, suggest song drop down menu		
		$fav_genre=$this->m_member->get_fav_genre($member_id);
		$fav_genre=explode(", ", $fav_genre);
		$genres=$this->m_member->get_genre();
		$i=0;
		
		if($fav_genre[0]) //have favourite genre
		{
			foreach($genres as $g)
			{
				if($g->g_id==$fav_genre[$i])
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($genres as $g)
			{
				$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
				$i++;
			}				
		}	
		$data['suggest_song_option']=$fav_genre_display;

		//music sidebar, suggest song
		$genre_id=$fav_genre_display[0]['id'];
		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);		
		
		//middle content
		$data['count_friend']=$this->m_member->get_count_friends($member_id, 0);		
		$data['list_friend']=$this->m_member->get_friends($member_id, 0);		
		//end middle content
		
		//right content
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
		$data['artist_list']=$this->m_member->get_artist_member($row->up_uid, 3);
		$data['count_my_artist']=$this->m_member->get_count_artist_member($row->up_uid);
		
		//render view
		$this->template->write('head_title', 'Friend Request');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_side_newbie', $data, '');
		$this->template->write_view('middle_content', 'pm_friend_list', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();
	}
	//------------------------------------------------------------------------------------------------------------------------end friend
	
	//------------------------------------------------------------------------------------------------------------------------notification
	/* Nisa. 25 Des 2011. Notification */
	function notification()
	{
		//top content
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
		$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
		$data['user_id']=$this->user_id;
		$data['name']=$this->m_member->get_user_name($this->user_id);
		
		//left content
		$row=$this->m_member->get_user_profile($this->user_id);
		$data['user_id']=$this->user_id;
		$data['image_path']=$this->m_member->get_profile_path($this->user_id);
		$data['name']=$row->up_name;
		$data['alias']=$row->up_alias;	
		$data['gender_code']=$row->up_gender;
		$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
		$data['city']=$row->up_city;
		$data['country']=$row->up_country;
		$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
		$data['ispagephoto'] = false;
		
		//music sidebar, my song list
		$limit=$this->config->item('limit_sidebar_fave_song_list');
		$data['song_list']=$this->m_member->get_fav_song($this->user_id, $limit);		
		
		//music sidebar, suggest song drop down menu		
		$fav_genre=$this->m_member->get_fav_genre($this->user_id);
		$fav_genre=explode(", ", $fav_genre);
		$genres=$this->m_member->get_genre();
		$i=0;
		
		if($fav_genre[0]) //have favourite genre
		{
			foreach($genres as $g)
			{
				if($g->g_id==$fav_genre[$i])
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($genres as $g)
			{
				$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
				$i++;
			}				
		}	
		$data['suggest_song_option']=$fav_genre_display;

		//music sidebar, suggest song
		$genre_id=$fav_genre_display[0]['id'];
		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);		
		
		//middle content
		$data['notification']=$this->m_member->get_notification($this->user_id);
		$data['all_notification']=$this->m_member->get_all_notification($this->user_id);
		$this->m_member->read_notification($this->user_id); 
		//end middle content
		
		//right content
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
		$data['artist_list']=$this->m_member->get_artist_member($row->up_uid, 3);
		$data['count_my_artist']=$this->m_member->get_count_artist_member($row->up_uid);
				
		//render view
		$this->template->write('head_title', 'Notification');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_side_newbie', $data, '');
		$this->template->write_view('middle_content', 'pm_member_notification', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();
	}
	
	/* Nisa. 6 Jan 2012. notification detail */
	function notification_detail($type, $wall_id)
	{
		//top content
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
		$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
		$data['user_id']=$this->user_id;
		$data['name']=$this->m_member->get_user_name($this->user_id);
		
		//left content
		$row=$this->m_member->get_user_profile($this->user_id);
		$data['user_id']=$this->user_id;
		$data['image_path']=$this->m_member->get_profile_path($this->user_id);
		$data['name']=$row->up_name;
		$data['alias']=$row->up_alias;	
		$data['gender_code']=$row->up_gender;
		$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
		$data['city']=$row->up_city;
		$data['country']=$row->up_country;
		$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
		$data['ispagephoto'] = false;
		
		//music sidebar, my song list
		$limit=$this->config->item('limit_sidebar_fave_song_list');
		$data['song_list']=$this->m_member->get_fav_song($this->user_id, $limit);		
		
		//music sidebar, suggest song drop down menu		
		$fav_genre=$this->m_member->get_fav_genre($this->user_id);
		$fav_genre=explode(", ", $fav_genre);
		$genres=$this->m_member->get_genre();
		$i=0;
		
		if($fav_genre[0]) //have favourite genre
		{
			foreach($genres as $g)
			{
				if($g->g_id==$fav_genre[$i])
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($genres as $g)
			{
				$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
				$i++;
			}				
		}	
		$data['suggest_song_option']=$fav_genre_display;

		//music sidebar, suggest song
		$genre_id=$fav_genre_display[0]['id'];
		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);		
		
		//middle content
		if($type=='wall')
		{
			$data['wall_detail']=$this->m_member->get_wall_item($wall_id);			
			$data['wall_comments']=$this->m_member->get_wall_item_comments($wall_id);
		}		
		//end middle content
		
		//right content
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
		$data['artist_list']=$this->m_member->get_artist_member($row->up_uid, 3);
		$data['count_my_artist']=$this->m_member->get_count_artist_member($row->up_uid);
				
		//render view
		$this->template->write('head_title', 'Notification');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_side_newbie', $data, '');
		$this->template->write_view('middle_content', 'pm_member_notification_detail', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();
	}
	//------------------------------------------------------------------------------------------------------------------------end notification
	
	//------------------------------------------------------------------------------------------------------------------------search	
	/* Nisa. 22 Des 2011. Search */
	function search()
	{		
		//top content
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
		$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
		$data['user_id']=$this->user_id;
		$data['name']=$this->m_member->get_user_name($this->user_id);
		
		//left content
		$row=$this->m_member->get_user_profile($this->user_id);
		$data['user_id']=$this->user_id;
		$data['image_path']=$this->m_member->get_profile_path($this->user_id);
		$data['name']=$row->up_name;
		$data['alias']=$row->up_alias;	
		$data['gender_code']=$row->up_gender;
		$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
		$data['city']=$row->up_city;
		$data['country']=$row->up_country;
		$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
		$data['ispagephoto'] = false;
		
		//music sidebar, my song list
		$limit=$this->config->item('limit_sidebar_fave_song_list');
		$data['song_list']=$this->m_member->get_fav_song($this->user_id, $limit);		
		
		//music sidebar, suggest song drop down menu		
		$fav_genre=$this->m_member->get_fav_genre($this->user_id);
		$fav_genre=explode(", ", $fav_genre);
		$genres=$this->m_member->get_genre();
		$i=0;
		
		if($fav_genre[0]) //have favourite genre
		{
			foreach($genres as $g)
			{
				if($g->g_id==$fav_genre[$i])
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($genres as $g)
			{
				$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
				$i++;
			}				
		}	
		$data['suggest_song_option']=$fav_genre_display;

		//music sidebar, suggest song
		$genre_id=$fav_genre_display[0]['id'];
		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);		
		
		//middle content
		$key=$this->input->post('searchbox');		
		$data['key']=$key;
		$data['search_music']=$this->m_member->search_music($key);
		$data['search_music_album']=$this->m_member->search_music_album($key);
		$data['search_music_category']=$this->m_member->search_music_category($key);
		$data['search_artist']=$this->m_member->search_artist($key);
		$data['search_fans']=$this->m_member->search_fans($key);
		//end middle content
		
		//right content
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
		$data['artist_list']=$this->m_member->get_artist_member($row->up_uid, 3);
		$data['count_my_artist']=$this->m_member->get_count_artist_member($row->up_uid);
				
		//render view
		$this->template->write('head_title', 'Search');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_side_newbie', $data, '');
		$this->template->write_view('middle_content', 'pm_search_result', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();
	}
	//------------------------------------------------------------------------------------------------------------------------end search
	
	//------------------------------------------------------------------------------------------------------------------------use as page
	/* Nisa. 2 Jan 2012. use as page viewer */
	function use_as_page()
	{
		//top content
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
		$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
		$data['user_id']=$this->user_id;
		$data['name']=$this->m_member->get_user_name($this->user_id);
		
		//left content
		$row=$this->m_member->get_user_profile($this->user_id);
		$data['user_id']=$this->user_id;
		$data['image_path']=$this->m_member->get_profile_path($this->user_id);
		$data['name']=$row->up_name;
		$data['alias']=$row->up_alias;	
		$data['gender_code']=$row->up_gender;
		$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
		$data['city']=$row->up_city;
		$data['country']=$row->up_country;
		$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
		$data['ispagephoto'] = false;
		
		//music sidebar, my song list
		$limit=$this->config->item('limit_sidebar_fave_song_list');
		$data['song_list']=$this->m_member->get_fav_song($this->user_id, $limit);		
		
		//music sidebar, suggest song drop down menu		
		$fav_genre=$this->m_member->get_fav_genre($this->user_id);
		$fav_genre=explode(", ", $fav_genre);
		$genres=$this->m_member->get_genre();
		$i=0;
		
		if($fav_genre[0]) //have favourite genre
		{
			foreach($genres as $g)
			{
				if($g->g_id==$fav_genre[$i])
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($genres as $g)
			{
				$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
				$i++;
			}				
		}	
		$data['suggest_song_option']=$fav_genre_display;

		//music sidebar, suggest song
		$genre_id=$fav_genre_display[0]['id'];
		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);		
		
		//middle content
		$data['my_artist']=$this->m_member->get_artist_member($this->user_id);
		//end middle content
		
		//right content
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
		$data['artist_list']=$this->m_member->get_artist_member($row->up_uid, 3);
		$data['count_my_artist']=$this->m_member->get_count_artist_member($row->up_uid);
				
		//render view
		$this->template->write('head_title', 'Use As Page');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_side_newbie', $data, '');
		$this->template->write_view('middle_content', 'pm_use_as_page', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();
	}

	/* Nisa. 2 Jan 2012. use as page process */
	function use_as_page_process($id)
	{
		if($this->m_member->is_my_artist($this->user_id, $id))
		{
			$newdata = array('artist_id'  => $id);
			$this->session->set_userdata($newdata);
			
			$this->session->set_flashdata('message', "Successfully use popmaya as artist");			
			redirect($_SERVER['HTTP_REFERER'], 'refresh');						
		}
		else
		{
			$this->session->set_flashdata('message', "Can not use popmaya as this artist");			
			redirect($_SERVER['HTTP_REFERER'], 'refresh');						
		}
	}

	/* Nisa. 2 Jan 2012. use as page as member */	
	function use_as_myself_process($id)
	{
		if($this->session->userdata['user_id']==$id)
		{
			if(isset($this->session->userdata['artist_id']))
			{
				$this->session->unset_userdata('artist_id');
			}
			$this->session->set_flashdata('message', "Successfully use popmaya as myself");			
			redirect($_SERVER['HTTP_REFERER'], 'refresh');			
		}
		else
		{
			$this->session->set_flashdata('message', "Can not use popmaya as myself");			
			redirect($_SERVER['HTTP_REFERER'], 'refresh');			
		}
	}	
	//------------------------------------------------------------------------------------------------------------------------end use as page
	
	//Adi 
	//27 Dec 2011
	//Download music
	function download_music($filename){
		$data = file_get_contents(site_url().'/assets/media/'.$filename);
		force_download($filename,$data);
	}
	
	/*	Remove Friend
		Julian
		3 Jan 2012
	*/
	function remove_friend(){	
		$this->m_member->delete_friend($this->uri->segment(3));
		redirect('member/friend_request','refresh');
	}
	
	/*	Post Comment Playlist
		Julian
		3 Jan 2012
	*/
	function playlist_comment(){	
		$this->m_member->add_playlist_comment($this->input->post('p_id_playlist'),  $this->user_id, $this->input->post('p_comment'), $this->session->userdata('ip_address'));
		redirect('member/profile/playlist_detail/'.$this->input->post('p_id_playlist'),'refresh');
		
	
		/*$this->m_member->delete_friend($this->uri->segment(3));
		redirect('member/?','refresh');*/
	}
}

?>
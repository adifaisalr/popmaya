<?php
class M_Member extends CI_Model 
{
	//------------------------------------------------------------------------------------------------------------------------profile
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Get user profile, join dengan user login
	*/
	function get_user_profile($id)
	{
		$query=$this->db->select('tr_user_profile.*, tr_user_login.ul_email')
				->from('tr_user_profile')
				->join('tr_user_login', 'ul_id=up_uid')
				->where('up_uid', $id)
				->get();
		return $query->row();
	}
	
	/*
	Nama: Adi, edit Nisa, tambahin yg no limit
	Tanggal: 2 Jan 2012
	Deskripsi: Get all artist by member
	*/
	function get_artist_member($id, $limit='')
	{
		if($limit)
		{
			$query=$this->db->select('tr_member_artist.*')
				->from('tr_member_artist')
				->where('memberID', $id)
				->order_by('ID','desc')
				->limit($limit)
				->get();
		}
		else
		{
			$query=$this->db->select('tr_member_artist.*')
				->from('tr_member_artist')
				->where('memberID', $id)
				->order_by('ID','desc')
				->get();			
		}
		$row=$query->result();
		return $row;
	}
	
	/* Nisa. 3 Jan 2012. Ambil jumlah artis saya */
	function get_count_artist_member($user_id)
	{
		$count=$this->db->select('*')
			->from('tr_member_artist')
			->where('memberID', $user_id)
			->count_all_results();		
		return $count;		
	}
	
	/* Nisa. 2 Jan 2012. cek apakah ini artisnya atau bukan. kembalian boolean */
	function is_my_artist($user_id, $artist_id)
	{
		$query=$this->db->get_where('tr_member_artist', array('ID'=>$artist_id, 'memberID'=>$user_id));
		if($query->row())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*
	Nama: Nisa
	Tanggal: 21 Des 2011
	Deskripsi: Get user name
	*/
	function get_user_name($id)
	{
		$query=$this->db->select('tr_user_profile.*')
				->from('tr_user_profile')
				->where('up_uid', $id)
				->get();
		return $query->row()->up_name;
	}
	
	/*
	Nama: Nisa
	Tanggal: 9 Des 2011
	Deskripsi: Get more wall on profile-info-update
	*/
	function get_more_wall($limit, $id)
	{
		$query=$this->db->select('tr_wall_item.*, up_name')
			->from('tr_wall_item')
			->join('tr_user_profile', 'up_uid=memberID')
			->where('memberID', $id)
			->order_by('created', 'desc')
			->limit($limit)
			->get();
		return $query->result();
	}
	
	
	/*
	Nama: Nisa
	Tanggal: 22 Nov 2011
	Deskripsi: Update user profile
	*/
	function update_user_profile($data, $id)
	{
		//update profile
		$data_profile = array(
			'up_name' => $data['name'],
			'up_lastname' => $data['lastname'],
			'up_nickname' => $data['nickname'],
			'up_nicknamehide' => $data['nicknamehide'],
			'up_mobile' => $data['mobile'],
			'up_mobilehide' => $data['mobilehide'],
			'up_gender' => $data['gender'],
			'up_genderhide' => $data['genderhide'],
			'up_birthday' => $data['birthday'],
			'up_status' => $data['status'],
			'up_statushide' => $data['statushide'],
			'up_emailhide' => $data['emailhide'],
			'up_birthyearhide' => $data['birthyearhide'],
			'up_sendmailalert' => $data['sendmailalert'],
			'up_city' => $data['city'],
			'up_state' => $data['state'],
			'up_country' => $data['country'],
			'up_cityid' => $data['cityid'],
			'up_countrycode'=> $data['countrycode'],
			'up_address' => $data['address'],
			'up_zipcode' => $data['zipcode'],
			'up_zipcodehide' => $data['zipcodehide'],
			'up_totalpointprivacy' => $data['totalpointprivacy'],
			'up_favgenre' => $data['favgenre'],
			'up_favgenrehide' => $data['favgenrehide'],
			'up_favartist' => $data['favartist'],
			'up_favartisthide' => $data['favartisthide']
			);
		$this->db->where('up_uid', $id);
		$this->db->update('tr_user_profile', $data_profile);
		
		//update email
		$data_login = array(
	        'ul_email' => $data['email']
		);
		$this->db->where('ul_id', $id);
		$this->db->update('tr_user_login', $data_login);

		//work
		//insert or update
		if($this->get_user_work($id)!='kosong') //update
		{
			$data_work = array(
				'uw_occupation' => $data['occupation'],
				'uw_occupationhide' => $data['occupationhide'],
				'uw_companies' => $data['companies'],
				'uw_companieshide' => $data['companieshide'],
				'uw_school' => $data['school'],
				'uw_schoolhide' => $data['schoolhide']	
			);
			$this->db->where('uw_uid', $id);
			$this->db->update('tr_user_work', $data_work);			
		}
		else //insert
		{
			$data_work = array(
				'uw_uid' => $id,
				'uw_occupation' => $data['occupation'],
				'uw_occupationhide' => $data['occupationhide'],
				'uw_companies' => $data['companies'],
				'uw_companieshide' => $data['companieshide'],
				'uw_school' => $data['school'],
				'uw_schoolhide' => $data['schoolhide']	
			);
			$this->db->insert('tr_user_work', $data_work); 			
		}	
	}
	
	/*
	Nama: Nisa
	Tanggal: 22 Nov 2011
	Deskripsi: Get user password and salt
	*/
	function get_user_password($id)
	{
		$query=$this->db->select('ul_pwd, ul_salt')
			->from('tr_user_login')
			->where('ul_id', $id)
			->get();
		return $query->row();
	}

	/*
	Nama: Nisa
	Tanggal: 22 Nov 2011
	Deskripsi: Update user password
	*/
	function update_user_password($id, $pass)
	{
		$data = array(
        	'ul_pwd' => $pass
     		);
		$this->db->where('ul_id', $id);
		$this->db->update('tr_user_login', $data);
	}
	
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Get user work
	*/
	function get_user_work($id)
	{
		/*$query=$this->db->get_where('tr_user_work', array('uw_uid'=>$id));
		
		if($query->row()) return $query->row();
		else return false;*/
		
		$query=$this->db->select('tr_user_work.*')
				->from('tr_user_work')
				->where('uw_uid', $id)
				->get();
		
		$row=$query->row();
		
		if($row) return $row;
		else return "kosong";
	}
	
	/*
	Nama: Nisa
	Tanggal: 22 Nov 2011
	Deskripsi: Get user status list
	*/
	function get_status()
	{
		$query=$this->db->order_by('name', 'asc')->get('ms_status');
		return $query->result();
	}	

	/*
	Nama: Nisa
	Tanggal: 22 Nov 2011
	Deskripsi: Get country
	*/
	function get_country()
	{
		$query=$this->db->order_by('Name', 'asc')->get('ms_country');
		return $query->result();		
	}
	
	/*
	Nama: Nisa
	Tanggal: 24 Nov 2011
	Deskripsi: Get country name
	*/
	function get_contry_name($id)
	{
		$query=$this->db->get_where('ms_country', array('Code'=>$id));
		$row=$query->row();
		if($row) return $row->Name;		
		else return "";
	}

	/*
	Nama: Nisa
	Tanggal: 22 Nov 2011
	Deskripsi: Get city
	*/
	function get_city()
	{
		$query=$this->db->order_by('Name', 'asc')->get('ms_city');
		return $query->result();		
	}
	
	/*
	Nama: Nisa
	Tanggal: 24 Nov 2011
	Deskripsi: Get city name
	*/
	function get_city_name($id)
	{
		$query=$this->db->get_where('ms_city', array('ID'=>$id));
		$row=$query->row();
		if($row) return $row->Name;
		else return "";
	}
	
	/* nisa 29 Des 2011, get state */
	function get_state()
	{
		$query=$this->db->order_by('Name', 'asc')->get('ms_state');
		return $query->result();		
	}
	
	/*
	Nama: Nisa
	Tanggal: 7 Des 2011
	Deskripsi: Get string profile folder
	*/
	function get_profile_path($id)
	{
		$query=$this->db->get_where('tr_user_login', array('ul_id'=>$id));
		$row=$query->row();
		$row=substr($row->ul_createdon, 0, 10);
		$path=str_replace('-', '/', $row);
		return $path;
	}	
	//------------------------------------------------------------------------------------------------------------------------end profile
	
	//------------------------------------------------------------------------------------------------------------------------message
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Get jumlah message yang belum di read
	Update, ambil dari tr_notification
	*/
	function get_count_unread_messages($id)
	{
		/*
		$count=$this->db->select('*')
			->from('tr_message')
			->where('toID', $id)
			->where('isread', "0")
			->count_all_results();		
		return $count;
		*/
		$count=$this->db->select('*')
			->from('tr_notification')
			->where('n_foruserid', $id)
			->where('n_type', "9")
			->where('n_isread', "0")
			->count_all_results();		
		return $count;
	}
	//------------------------------------------------------------------------------------------------------------------------end message
	
	//------------------------------------------------------------------------------------------------------------------------music
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Get all genre
	*/
	function get_genre()
	{
		$query=$this->db->get('tr_genre');
		return $query->result();
	}
	
	/*
	Nama: Adi
	Tanggal: 16 Des 2011
	Deskripsi: Get all artist
	*/
	function get_artist()
	{
		$query=$this->db->get('tr_member_artist');
		return $query->result();
	}
	
	/*
	Nama: Nisa
	Tanggal: 24 Nov 2011
	Deskripsi: Get fav genre
	*/	
	function get_fav_genre($id)
	{
		$query=$this->db->select('up_favgenre')
			->from('tr_user_profile')
			->where('up_uid', $id)
			->get();
		$row=$query->row();
		return $row->up_favgenre;
	}
	
	/*
	Nama: Adi
	Tanggal: 16 Desember 2011
	Deskripsi: Get fav artist
	*/	
	function get_fav_artist($id)
	{
		$query=$this->db->select('up_favartist')
			->from('tr_user_profile')
			->where('up_uid', $id)
			->get();
		$row=$query->row();
		return $row->up_favartist;
	}
	
	/*
	Nama: Nisa
	Tanggal: 24 Nov 2011
	Deskripsi: Get my fav song list
	*/	
	function get_fav_song($id, $limit)
	{
		$query=$this->db->select('tr_fave_song.fs_id, tr_member_artist.artist_name as artist, tr_song.title')
			->from('tr_fave_song')
			->join('tr_song', 'tr_song.s_id=tr_fave_song.songID')
			->join('tr_member_artist', 'tr_member_artist.memberID=tr_song.memberID')
			->where('tr_fave_song.memberID', $id)
			->limit($limit)
			->get();
		return $query->result();
	}
	
	/* zy. temporary model */
	function get_fave($id, $limit)
	{
		//$this->db->where(array('memberID'=>$id));
		//$query=$this->db->get('tr_fave_song');
		$query=$this->db->get_where('tr_fave_song', array('memberID'=>$id));
		return $query->result();
	}
	
	/*
	Nama: Nisa
	Tanggal: 24 Nov 2011
	Deskripsi: Get song by genre
	*/	
	//??
	function get_song_by_genre($id, $limit)
	{
		$query=$this->db->select('tr_member_artist.title as artist, tr_song.title')
			->from('tr_song')
			->join('tr_member_artist', 'tr_member_artist.memberID=tr_song.memberID')
			->where('tr_song.genreID', $id)
			->limit($limit)
			->get();
		return $query->result();		
	}
	//------------------------------------------------------------------------------------------------------------------------end music
	
	//------------------------------------------------------------------------------------------------------------------------friends
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Get sejumlah tertentu teman
	*/
	function get_friends($id, $limit)
	{
		if($limit>0)
		{
			$query=$this->db->select('up_uid, up_name, up_city, up_alias, ul_createdon, f_id') //edited by julian 3 jan 2012,menambah parameter select 'f_id'
				->from('tr_friend')
				->join('tr_user_profile', 'f_friendid=up_uid')
				->join('tr_user_login', 'f_friendid=ul_id')
				->where('f_userid', $id)
				->or_where('f_friendid', $id)
				->where('ul_active', '1')
				->where('f_isactive', '1')
				->limit($limit)
				->get();
		}
		else
		{
			$query=$this->db->select('up_uid, up_name, up_city, up_alias, ul_createdon, f_id') //edited by julian 3 jan 2012,menambah parameter select 'f_id'
				->from('tr_friend')
				->join('tr_user_profile', 'f_friendid=up_uid')
				->join('tr_user_login', 'f_friendid=ul_id')
				->where('f_userid', $id)
				->or_where('f_friendid', $id)
				->where('ul_active', '1')
				->where('f_isactive', '1')
				->get();			
		}
		return $query->result();
	}

	/* Nisa. 27 Des 2011. get jumlah teman */
	function get_count_friends($id)
	{
		$count=$this->db->select('*')
			->from('tr_friend')
			->join('tr_user_login', 'f_friendid=ul_id')
			->where('f_userid', $id)
			->or_where('f_friendid', $id)
			->where('f_isactive', '1')
			->where('ul_active', '1')
			->count_all_results();		
		return $count;
	}
	
	/* Nisa. 25 Des 2011. ambil jumlah request pertemanan untuk notification */
	function get_count_friend_request($user_id)
	{
		$count=$this->db->select('*')
			->from('tr_notification')
			->where('n_foruserid', $user_id)
			->where('n_type', '8')
			->where('n_isread', "0")
			->count_all_results();		
		return $count;
	}

	/* Nisa. 25 Des 2011. ambil jumlah request pertemanan */
	function get_count_all_friend_request($user_id)
	{
		$count=$this->db->select('*')
			->from('tr_friend')
			->where('f_friendid', $user_id)
			->where('f_isactive', "0")
			->count_all_results();		
		return $count;
	}
	
	/* Nisa. 2 Jan 2012. hapus notif friend request */
	function read_friend_notification($user_id)
	{
		$data = array(
	        'n_isread' => '1'
			);
		$this->db->where('n_foruserid', $user_id);
		$this->db->where('n_isread', '0');
		$this->db->where('n_type', '8'); //tipe friend request
		$this->db->update('tr_notification', $data);		
	}
	
	/* Nisa. 25 Des 2011. ambil semua request pertemanan */
	function get_friend_request($user_id)
	{
		$query=$this->db->select("tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_login.ul_createdon, (select count(a.f_id) from tr_friend a
		where (a.f_userid=tr_friend.f_userid
		or a.f_friendid=tr_friend.f_userid)
		and a.f_isactive='1'
		and (a.f_userid in (select b.f_userid from tr_friend b where b.f_friendid='$user_id' and b.f_isactive='1')
			or a.f_userid in (select b.f_friendid from tr_friend b where b.f_userid='$user_id' and b.f_isactive='1')
			or a.f_friendid in (select b.f_userid from tr_friend b where b.f_friendid='$user_id' and b.f_isactive='1')
				or a.f_friendid in (select b.f_friendid from tr_friend b where b.f_userid='$user_id' and b.f_isactive='1')
			)) as mutual ")
			->from('tr_friend')
			->join('tr_user_profile', 'tr_user_profile.up_uid=tr_friend.f_userid')
			->join('tr_user_login', 'f_friendid=ul_id')
			->where('f_friendid', $user_id)
			->where('f_isactive', '0')
			->order_by('f_datetime', 'desc')
			->get();
		return $query->result();
	}
	
	/* Nisa. 25 Des 2011. approve request pertemanan */
	function friend_approve_process($user_id, $friend_id)
	{
		//update friend status
		$data=array('f_isactive'=>'1');
		$this->db->where('f_userid', $friend_id);
		$this->db->where('f_friendid', $user_id);
		$this->db->update('tr_friend', $data);
		
		//insert notification
		$data = array(
		   'n_type' => '7', //accepted your friend request
		   'n_fromuserid' => $user_id,
		   'n_foruserid' => $friend_id,
		   'n_isread' => '0'
		);
		$this->db->insert('tr_notification', $data);		
	}
	//------------------------------------------------------------------------------------------------------------------------end friends
	
	/* Nisa. 25 Des 2011. Ambil jumlah notifikasi aktifitas yang berkaitan dengan dirinya */
	function get_count_notification($user_id)
	{
		$arr=array('1', '2', '3', '4', '5', '6', '7');
		$count=$this->db->select('*')
			->from('tr_notification')
			->where('n_foruserid', $user_id)
			->where('n_isread', "0")
			->where_in('n_type', $arr)
			->count_all_results();		
		return $count;		
	}
	
	/* Nisa. 25 Des 2011. Ambil list notifikasi aktifitas yang berkaitan dengan dirinya */
	function get_notification($user_id)
	{
		$arr=array('1', '2', '3', '4', '5', '6', '7');
		$query=$this->db->select('tr_notification.*, dr_notification_type.nt_name, tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_login.ul_createdon')
			->from('tr_notification')
			->join('tr_user_profile', 'tr_user_profile.up_uid=tr_notification.n_fromuserid')
			->join('tr_user_login', 'tr_user_login.ul_id=tr_notification.n_fromuserid')
			->join('dr_notification_type', 'tr_notification.n_type=dr_notification_type.nt_id')
			->where('n_foruserid', $user_id)
			->where('n_isread', '0')
			->where_in('n_type', $arr)
			->order_by('n_datetime', 'desc')
			->get();
		return $query->result();
	}
	
	/* Nisa. 28 Des 2011. Ambil semua list notifikasi aktifitas yang berkaitan dengan dirinya */
	function get_all_notification($user_id)
	{
		$arr=array('1', '2', '3', '4', '5', '6', '7');
		$query=$this->db->select('tr_notification.*, dr_notification_type.nt_name, tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_login.ul_createdon')
			->from('tr_notification')
			->join('tr_user_profile', 'tr_user_profile.up_uid=tr_notification.n_fromuserid')
			->join('tr_user_login', 'tr_user_login.ul_id=tr_notification.n_fromuserid')
			->join('dr_notification_type', 'tr_notification.n_type=dr_notification_type.nt_id')
			->where('n_foruserid', $user_id)
			->where('n_isread', '1')
			->where_in('n_type', $arr)
			->order_by('n_datetime', 'desc')
			->get();
		return $query->result();		
	}
	
	/* Nisa. 28 Des 2011. Ubah semua notifikasi menjadi sudah terbaca */
	function read_notification($user_id)
	{
		$arr=array('1', '2', '3', '4', '5', '6', '7');
		$data = array(
	        'n_isread' => '1'
			);
		$this->db->where('n_foruserid', $user_id);
		$this->db->where('n_isread', '0');
		$this->db->where_in('n_type', $arr);	
		$this->db->update('tr_notification', $data);
	}
	
	function get_recommendation($id, $limit)
	{
		$this->db->select('a.*', 'b.f_isactive');
	    $this->db->from('tr_user_profile a');
	    $this->db->join('tr_friend b', 'a.up_uid = b.f_friendid', 'left');
		$this->db->limit(5,0);
		$query = $this->db->get();
	    return $query->result();		
	}
	
	//------------------------------------------------------------------------------------------------------------------------facebook
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Cek apakah facebook dengan id tertentu sudah ada atau belum
	*/
	function fb_id_exists($fbid)
	{
		$query = $this->db->select('ul_id')
			->from('tr_user_login')
			->where('ul_fbid',$fbid)
			->get();

		if ($query->num_rows() > 0) 
		{
			$row = $query->row();
			$uid = $row->ul_id;
			return $uid;
		} 
		else 
		{
			return FALSE;
		}
	}
	
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Menyimpan profile picture dari facebook
	*/
	function save_pic_from_fb($uid,$pic) 
	{
	   $data = array(
		   'uph_uid' => $uid,
		   'uph_orig' => $pic,
		   'uph_public' => $pic,
		   'uph_thumb' => $pic
	    	);
    	$this->db->insert('tr_user_photo', $data);
  	}	
	//------------------------------------------------------------------------------------------------------------------------end facebook
	
	//------------------------------------------------------------------------------------------------------------------------location
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Get geolocation
	*/
	function get_geolocation()
	{
		$this->load->library('ip2locationlite');
		
		$ip2locationKey=$this->config->item('ip2location_key');
		
		//Load the class
		$ipLite = new ip2locationlite();
		$ipLite->setKey($ip2locationKey);
		
		//Get errors and locations
		$locations = $ipLite->getCity($_SERVER['REMOTE_ADDR']);
		$errors = $ipLite->getError();
		
		return $locations;
	}
	//------------------------------------------------------------------------------------------------------------------------end location
	
	//------------------------------------------------------------------------------------------------------------------------registration
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Menyimpan member baru
	*/
	function save_new_member($name,$email,$pwd,$gender,$sendupdate,$location,$vericode,$fbid,$twid) 
	{
		$data = array(
			'ul_email' => $email,
			'ul_pwd' => md5($pwd),
			'ul_vericode' => $vericode,
			'ul_sendupdate' => $sendupdate,
			'ul_twid' => $twid,
			'ul_fbid' => $fbid
		);
		$this->db->insert('tr_user_login', $data);

		$id= $this->db->insert_id();

		$city = $location['City'];

		$city = $location['City'];
		if($city != '') 
		{
			$country = $location['CountryName'];
			$timezone= $location['TimezoneName'];

			$query = $this->db->select('ID,CountryCode')
			->from('ms_city')
			->where('Name',$city)
			->get();

			if ($query->num_rows() > 0) 
			{
				$row = $query->row();
				$cityid = $row->ID;
				$countrycode = $row->CountryCode;
			} 
			else 
			{
				$cityid = '';
				$countrycode = '';
			}
		} 
		else 
		{
			$country = '';
			$timezone = '';
			$cityid = '';
			$countrycode = '';
		}

		$data = array(
			'up_uid' => $id,
			'up_name' => $name,
			'up_gender' => $gender,
			'up_city' => $city,
			'up_country' => $country,
			'up_cityid' => $cityid,
			'up_countrycode' => $countrycode,
			'up_timezonename' => $timezone
		);
		$this->db->insert('tr_user_profile', $data);
		return $id;
	}    
	//------------------------------------------------------------------------------------------------------------------------end registration
	
	//------------------------------------------------------------------------------------------------------------------------login
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Menambahkan ke tabel session dan user login ketika user melakukan login
	*/
	public function add_session($uid,$ip,$browser,$location,$session)
	{
		$unixtime = time();
		$date = date("Y-m-d H:i:s");

		$session = $uid.'|'.$unixtime;

                $lat = '';//$location['Latitude'];
                $long = '';//$location['Longitude'];
                $country = '';//$location['CountryName'];
                $city = '';//$location['City'];
		$ins = array('us_userid'     => $uid,
                               'us_session' => $session,
				'us_ip'  => $ip,
        			'us_browser' => $browser,
				'us_latitude' => $lat,
				'us_longitude' => $long,
				'us_city' => $city,
				'us_country' => $country,
					 );
        	$this->db->insert('tr_user_session', $ins);

		$data = array('ul_session' => $session);
		$this->db->where('ul_id', $uid);
		$this->db->update('tr_user_login', $data);
	}
	//------------------------------------------------------------------------------------------------------------------------end login	
	/*
	Name	: Firman
	Date	: 17 Des 2011
	Desc	: get list image banner profile
	*/
	function get_image_banner_profile()
	{
		$query = $this->db->get_where('tr_ads', array('position' => 'profi'));
		return $query->result();		
	}
	
	/*
	Name	: Firman
	Date	: 19 Des 2011
	Desc	: insert new play list
	*/
	function save_playlist($playlist){
		$this->db->insert('tr_playlist', $playlist);
		return $this->db->insert_id();
	}
	
	function delete_playlist($id){
		$this->db->where('playlist_id', $id);
		$this->db->delete('tr_song_playlist');
		$this->db->where('playlist_id', $id);
		$this->db->delete('tr_playlist'); 
	}
	
	function get_last_playlist($user_id){
	}
	
	/*
	Name	: Firman
	Date	: 19 Des 2011
	Desc	: insert new song to play list
	*/
	function add_song_playlist($playlist_song){
		$this->db->insert('tr_song_playlist', $playlist_song);
	}
	
	function remove_song_playlist($playlist_id,$song_id){
	
	}
	
	/*
	Name	: Firman
	Date	: 19 Des 2011
	Desc	: get all song
	*/
	function get_all_song(){
		$query=$this->db->get('tr_song');
		return $query->result();		
	}
	
	/*
	Name	: Firman
	Date	: 19 Des 2011
	Desc	: get user playlist
	*/
	function get_user_playlist($user_id){	
		//echo $user_id;
		$query = $this->db->select("*")
			->from("tr_playlist")
			->join("tr_song_playlist","tr_playlist.playlist_id = tr_song_playlist.playlist_id")
			->where("tr_playlist.user_id",$user_id)
			->get();
		return $query->result();
	}
	
	/*
	Name	: Firman
	Date	: 19 Des 2011
	Desc	: get song detail
	*/
	function get_specified_song($song_id){
		$query=$this->db->select('*')
				->from('tr_song')
				->where('s_id', $song_id)
				->get();
		return $query->row();
	}
	
	/*
	Name	: Firman
	Date	: 19 Des 2011
	Desc	: get artist detail
	*/
	function get_specified_artist($artist_id){
		$query=$this->db->select('*')
				->from('tr_member_artist')
				->where('ID', $artist_id)
				->get();
		return $query->row();
	}
	
	/*
	Name	: Firman
	Date	: 19 Des 2011
	Desc	: get song one playlist
	*/
	function get_one_playlist($playlist_id){
		$query = $this->db->select("*")
			->from("tr_playlist")
			->join("tr_song_playlist","tr_playlist.playlist_id = tr_song_playlist.playlist_id")
			->where("tr_song_playlist.playlist_id",$playlist_id)
			->get();
		return $query->result();
	}
	
	/*
	Name	: Firman
	Date	: 19 Des 2011
	Desc	: delete song from playlist
	*/
	function delete_song_playlist($song_id,$playlist_id){
      $query = $this->db->where('playlist_id', $playlist_id);
	  $query = $this->db->where('song_id', $song_id);
	  $query = $this->db->limit(1,0);
      $query = $this->db->delete('tr_song_playlist');
	}
	
	function save_album($album){
		$this->db->insert('tr_album', $album);
	}
	
	function get_album($user_id){
		$query = $this->db->select('*')
			->from("tr_album")
			->where("memberID",$user_id)
			->get();
		return $query->result();
	}
	
	function save_photo($photo){
		$this->db->insert('tr_photo', $photo);
		
		//get jumlah foto dalam album
		$count_post=$this->db->select('*')
			->from('tr_photo')
			->where('albumID', $photo['albumID'])
			->count_all_results();		

		//update jumlah foto
		$data = array(
	        'post' => $count_post
		);
		$this->db->where('ID', $photo['albumID']);
		$this->db->update('tr_album', $data);		
	}
	
	function get_photo_album($album_id){
		$query = $this->db->select('*')
			->from("tr_photo")
			->where("albumID",$album_id)
			->get();
		return $query->result();
	}
	
	function get_single_photo($id){
		$query = $this->db->select('*')
			->from("tr_photo")
			->where("ID",$id)
			->get();
		return $query->result();
	}
	
	
	//------------------------------------------------------------------------------------------------------------------------search
	/*
	User bisa search global / dari seluruh kategori (kategori musik)
	User bisa filter pencarian berdasarkan kategori (kategori musik)
	User bisa menampilkan / mencari list artist
	User bisa menampilkan /  mencari list fan
	*/
	
	/* Nisa. 24 Des 2011. Search musik */
	function search_music($key)
	{
		$query=$this->db->select('s_id, title')
			->from('tr_song')
			->like('title', $key)
			->get();
		return $query->result();		
	}

	/* Nisa. 24 Des 2011. Search musik album */
	function search_music_album($key)
	{
		$query=$this->db->select('as_id, title')
			->from('tr_album_song')
			->like('title', $key)
			->get();
		return $query->result();		
	}

	/* Nisa. 24 Des 2011. Search kategori musik */
	function search_music_category($key)
	{
		$query=$this->db->select('g_id, g_name')
			->from('tr_genre')
			->like('g_name', $key)
			->get();
		return $query->result();		
	}
	
	/* Nisa. 24 Des 2011. Search artist */
	function search_artist($key)
	{
		$query=$this->db->select('ID, artist_name')
			->from('tr_member_artist')
			->like('artist_name', $key)
			->get();
		return $query->result();		
	}
	
	/* Nisa. 24 Des 2011. Search fans */
	function search_fans($key)
	{
		$query=$this->db->select('up_uid, up_name')
			->from('tr_user_profile')
			->like('up_name', $key)
			->or_like('up_lastname', $key)
			->get();
		return $query->result();
	}
	//------------------------------------------------------------------------------------------------------------------------end search
	
	//------------------------------------------------------------------------------------------------------------------------wall
	/* Nisa. 26 Des 2011. share status */
	function share_status($user_id, $to_id, $textStatus, $ip_addr)
	{
		//insert wall item
		$data = array(
		   'memberID' => $user_id,
		   'toMemberID' => $to_id,
		   'info' => $textStatus,
		   'type' => '1',
		   'host' => $ip_addr
		);
		$this->db->insert('tr_wall_item', $data);
		
		//insert notification
		if($to_id!='0')
		{
			$wall_id=$this->db->insert_id();
			
			$data = array(
			   'n_type' => '4', //lihat dr_notification_type
			   'n_itemid' => $wall_id,
			   'n_fromuserid' => $user_id,
			   'n_foruserid' => $to_id,
			   'n_isread' => '0'
			);
			$this->db->insert('tr_notification', $data);			
		}
	}
	
	/* Nisa. 26 Des 2011. get status update */
	function get_status_update($user_id, $type='')
	{
		if($type=='')
		{
			$query=$this->db->select('tr_wall_item.*')
				->from('tr_wall_item')
				->where('memberID', $user_id)
				->or_where('toMemberID', $user_id)
				->order_by('created', 'desc')
				->get();
		}
		else
		{
			$query=$this->db->select('tr_wall_item.*')
				->from('tr_wall_item')
				->where('memberID', $user_id)
				->where('type', $type)
				->where('toMemberID', '0')
				->order_by('created', 'desc')
				->get();			
		}
		return $query->result();
	}
	
	/* Nisa. 26 Des 2011. komentari status */
	function status_comment($comment, $wall_itemID, $user_id, $ip_addr)
	{
		//insert wall comment
		$data = array(
		   'wall_itemID' => $wall_itemID,
		   'memberID' => $user_id,
		   'comment' => $comment,
		   'post' => now(),
		   'host' => $ip_addr
		);
		$this->db->insert('tr_wall_item_comment', $data);
		
		//cari tau siapa yg punya wall/status
		$query=$this->db->get_where('tr_wall_item', array('ID'=>$wall_itemID));
		$row=$query->row();		
		$user_have_wall=$row->memberID;
		
		//kalau dia nge wall, jadinya ada notification responds on your wall
		if($row->toMemberID!='0') 
		{
			$is_wall='1';
		}
		else
		{
			$is_wall='0';
		}
		
		//insert notification kalau yg nulis komen itu yg bikin wall
		//kalau ngomenin wall sendiri ga ada notificationnya
		if($user_have_wall!=$user_id)
		{
			//insert notification
			$data = array(
			   'n_type' => '1',
			   'n_itemid' => $wall_itemID,
			   'n_fromuserid' => $user_id,
			   'n_foruserid' => $user_have_wall,
			   'n_isread' => '0'
			);
			$this->db->insert('tr_notification', $data);
			
			//cek commented, jadinya ada also commented
			$query=$this->db->get_where('tr_notification', array('n_type'=>'1', 'n_itemid'=>$wall_itemID));
			$result=$query->result();
			
			//untuk semua yg pernah ngomenin, kecuali yg punya wall dikasih notification
			foreach($result as $res)
			{
				if($res->n_fromuserid!=$user_have_wall)
				{
					$data = array(
					   'n_type' => '2', //also commented
					   'n_itemid' => $wall_itemID,
					   'n_fromuserid' => $user_id,
					   'n_foruserid' => $res->n_fromuserid,
					   'n_isread' => '0'
					);
					$this->db->insert('tr_notification', $data);
				}
				else
				{
					if($is_wall=='1')
					{						
						//insert notification
						$data = array(
						   'n_type' => '5', //responds on your wall
						   'n_itemid' => $wall_itemID,
						   'n_fromuserid' => $user_id,
						   'n_foruserid' => $user_have_wall,
						   'n_isread' => '0'
						);
						$this->db->insert('tr_notification', $data);
					}
				}
			}		
		}
	}
	
	/* Nisa. 26 Des 2011. ambil komentar2 terhadap suatu status */
	function get_status_comment($user_id)
	{		
		$query=$this->db->select('tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_membersince, tr_wall_item_comment.*')
			->from('tr_wall_item_comment')
			->join('tr_user_profile', 'tr_user_profile.up_uid=tr_wall_item_comment.memberID')			
			->join('tr_wall_item', 'tr_wall_item.ID=tr_wall_item_comment.wall_itemID')
			->where('tr_wall_item.memberID', $user_id)
			->order_by('post', 'asc')
			->get();
		return $query->result();
	}
	
	/* Nisa. 26 Des 2011. aksi klik like suatu status */
	function status_like($wall_id, $user_id)
	{
		//insert tr_wall_item_like
		$data = array(
		   'l_member_id' => $user_id,
		   'l_wall_id' => $wall_id,
		);
		$this->db->insert('tr_wall_item_like', $data);				
		
		//get jumlah like
		$query=$this->db->get_where('tr_wall_item', array('ID'=>$wall_id));
		$result=$query->row();
		$like=$result->like;
		
		//update jumlah like
		$data = array(
	        'like' => $like+1
		);
		$this->db->where('ID', $wall_id);
		$this->db->update('tr_wall_item', $data);

		//cari tau siapa yg punya wall
		$query=$this->db->get_where('tr_wall_item', array('ID'=>$wall_id));
		$result=$query->row();		
		$user_have_wall=$result->memberID;
		
		//insert notification kalau yg nge like komen itu yg bikin wall
		//kalau ngomenin wall sendiri ga ada notificationnya
		if($user_have_wall!=$user_id)
		{
			//insert notification
			$data = array(
			   'n_type' => '3',
			   'n_itemid' => $wall_id,
			   'n_fromuserid' => $user_id,
			   'n_foruserid' => $user_have_wall,
			   'n_isread' => '0'
			);
			$this->db->insert('tr_notification', $data);	
		}	
	}
	
	/* Nisa. 2 Jan 2012. Cek udah pernah nge like suatu wall atau belum. Kembalian boolean */
	function is_like_wall($user_id, $wall_id)
	{
		$query=$this->db->get_where('tr_wall_item_like', array('l_member_id'=>$user_id, 'l_wall_id'=>$wall_id));
		if($query->row())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/* Nisa. 26 Des 2011. hapus status */
	function status_delete($wall_id)
	{
		//delete wall
		$this->db->delete('tr_wall_item', array('ID' => $wall_id));
		
		//delete comment
		$this->db->delete('tr_wall_item_comment', array('wall_itemID' => $wall_id));
		
		//delete notification
		$this->db->delete('tr_notification', array('n_itemid' => $wall_id));		
	}
	
	/* Nisa. 26 Des 2011. like photo */
	function photo_like($photo_id, $user_id)
	{
		//insert ke tr_photo_like
		$data = array(
			'l_member_id' => $user_id,
			'l_photo_id' => $photo_id,
		);
		$this->db->insert('tr_photo_like', $data); 					
		
		//get jumlah like
		$query=$this->db->get_where('tr_photo', array('ID'=>$photo_id));
		$result=$query->row();
		$like=$result->like;
		
		//update jumlah like
		$data = array(
	        'like' => $like+1
		);
		$this->db->where('ID', $photo_id);
		$this->db->update('tr_photo', $data);		
	}
	
	/* Nisa. 3 Jan 2012. Cek udah pernah nge like suatu wall atau belum. Kembalian boolean */
	function is_like_photo($user_id, $photo_id)
	{
		$query=$this->db->get_where('tr_photo_like', array('l_member_id'=>$user_id, 'l_photo_id'=>$photo_id));
		if($query->row())
		{
			return true;
		}
		else
		{
			return false;
		}
	}	
	
	/* Nisa. 26 Des 2011. komentari foto */
	function photo_comment($comment, $photo_id, $user_id, $ip_addr)
	{
		//insert photo comment
		$data = array(
		   'photoID' => $photo_id,
		   'memberID' => $user_id,
		   'comment' => $comment,
		   'post' => now(),
		   'host' => $ip_addr
		);
		$this->db->insert('tr_photo_comment', $data);		
	}
	
	/* Nisa. 26 Des 2011. ambil komentar2 terhadap suatu foto */
	function get_photo_comment($user_id)
	{
		$query=$this->db->select('tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_membersince, tr_photo_comment.*')
			->from('tr_photo_comment')
			->join('tr_user_profile', 'tr_user_profile.up_uid=tr_photo_comment.memberID')			
			->join('tr_photo', 'tr_photo.ID=tr_photo_comment.photoID')
			->where('tr_photo.memberID', $user_id)
			->order_by('post', 'desc')
			->get();
		return $query->result();		
	}
	
	/* Nisa. 26 Des 2011. hapus foto */
	function photo_delete($photo_id)
	{
		//get album id
		$query=$this->db->get_where('tr_photo', array('ID'=>$photo_id));
		$row=$query->row();
		$album_id=$row->albumID;
		
		//delete photo
		$this->db->delete('tr_photo', array('ID' => $photo_id));
		
		//delete comment
		$this->db->delete('tr_photo_comment', array('photoID' => $photo_id));		
		
		//get jumlah foto dalam album
		$count_post=$this->db->select('*')
			->from('tr_photo')
			->where('albumID', $album_id)
			->count_all_results();		

		//update jumlah foto
		$data = array(
	        'post' => $count_post
		);
		$this->db->where('ID', $album_id);
		$this->db->update('tr_album', $data);		
	}
	
	/* Nisa. 26 Des 2011. hapus album */
	function album_delete($album_id)
	{
		//delete album
		$this->db->delete('tr_album', array('ID' => $album_id));
		
		//delete comment
		$query=$this->db->get_where('tr_photo', array('albumID'=>$album_id));
		foreach($query->result() as $result)
		{
			$this->db->delete('tr_photo_comment', array('photoID' => $result->ID));
		}
		
		//delete photo
		$this->db->delete('tr_photo', array('albumID' => $album_id));				
	}
	
	/* Nisa. 26 Des 2011. share note */
	function share_note($user_id, $title, $note, $ip_addr)
	{
		$data = array(
		   'memberID' => $user_id,
		   'title' => $title,
		   'info' => $note,
		   'created' => now(),
		   'type' => '4',
		   'host' => $ip_addr
		);

		$this->db->insert('tr_wall_item', $data);
	}	
	
	/* Nisa. 6 Jan 2012. ambil detail suatu wall */
	function get_wall_item($wall_id)
	{
		$query=$this->db->select('tr_wall_item.*, tr_user_profile.up_name')
			->from('tr_wall_item')
			->join('tr_user_profile', 'tr_user_profile.up_uid=tr_wall_item.memberID')			
			->where('ID', $wall_id)
			->get();		
		return $query->row();
	}

	/* Nisa. 6 Jan 2012. ambil suatu wall comment */	
	function get_wall_item_comments($wall_id)
	{
		$query=$this->db->select('tr_wall_item_comment.*, tr_user_profile.up_name')
			->from('tr_wall_item_comment')
			->join('tr_user_profile', 'tr_user_profile.up_uid=tr_wall_item_comment.memberID')			
			->where('wall_itemID', $wall_id)
			->order_by('post', 'asc')
			->get();		
		return $query->result();		
	}
	//------------------------------------------------------------------------------------------------------------------------end wall
	
	function get_friend_list($user_id){
		
	}
	
	function like_playlist($like_data){
		$this->db->insert('tr_playlist_like', $like_data);
	}
	
	function get_num_like_playlist($id){
		$query = $this->db->select('*')
			->from('tr_playlist_like')
			->where('playlist_id',$id)
			->get();

		return $query->num_rows();
	}
	
	/*
	Delete Friend, Add Playlist Comment, Get Playlist Comment
	Julian
	3 Jan 2012
	*/
	function delete_friend($p_f_id){
      $query = $this->db->where('f_id', $p_f_id);
	  $query = $this->db->limit(1,0);
      $query = $this->db->delete('tr_friend');
	}
	
	function add_playlist_comment($p_playlist_id, $user_id, $comment, $ip_addr){
		$data = array(
		   'playlist_itemID' => $p_playlist_id,
		   'memberID' => $user_id,
		   'comment' => $comment,
		   'post' => now(),
		   'host' => $ip_addr
		);
		$this->db->where('playlist_itemID',$p_playlist_id);
		$this->db->insert('tr_playlist_item_comment',$data);
	}
	
	function get_playlist_comment($p_playlist_id){
		$this->db->where('playlist_itemID',$p_playlist_id);
		$query = $this->db->get('tr_playlist_item_comment');
		return $query->result();
	}
}
?>
<?php /* ^zy */
class M_Message extends CI_Model 
{
	function list_message($whr, $start, $limit){	
    $this->db->select('a.ID, a.isread, a.toID, a.message_contentID, a.isTrash, a.timestamp, b.up_name, b.up_uid, c.subject');
    $this->db->from('tr_message a');
    $this->db->join('tr_user_profile b', 'a.toID=b.up_uid');
    $this->db->join('tr_message_content c', 'c.ID=a.message_contentID');  
    $this->db->where($whr);
    $this->db->limit($limit, $start);
    $query=$this->db->get();
//    echo $this->db->last_query();
		return $query->result();
	}
  function count_all($whr){
    $this->db->select('a.isread, a.toID, a.message_contentID, a.isTrash, a.timestamp, b.up_name, b.up_uid, c.subject');
    $this->db->from('tr_message a');
    $this->db->join('tr_user_profile b', 'a.toID=b.up_uid');
    $this->db->join('tr_message_content c', 'c.ID=a.message_contentID');  
    $this->db->where($whr);
    $this->db->limit('1000000',0);
    $qca = $this->db->get();
    return $qca->num_rows();
  }
  function list_friends($limit){
    $listfr=$this->db->select('a.f_userid, a.f_isactive, a.f_friendid, b.up_name')
        ->from('tr_friend a')
        ->join('tr_user_profile b', 'a.f_friendid=b.up_uid')
        ->where(array('a.f_userid'=>$this->session->userdata('user_id')))
        ->order_by('b.up_name', 'asc')
        ->limit($limit, '0')
        ->get();
    return $listfr->result();
  }
  function send_mssg(){

    $mssg = array('to_member'=>$this->session->userdata('user_id'), 'subject' => $this->input->post('mssg_subject'), 'message'=>$this->input->post('mssg_text'));

    $this->db->insert('tr_message_content', $mssg);
    //echo $this->db->last_query();
    if($this->db->insert_id()){
      
      $mssg1=array('fromID'=>$this->session->userdata('user_id'),
        'toID'=>$this->input->post('friend_id'),
        'timestamp'=>date('Y-m-d H:i:s'),
        'type'=>'pm',
        'message_contentID'=>$this->db->insert_id()
      );
      $this->db->where(array('message_contentID'));
      $this->db->insert('tr_message', $mssg1);
      return $this->db->insert_id();
    }
  }
  function read_mssg($mid){
    $qrm = $this->db->get_where('tr_message_content', array('ID'=>$mid)); 
    return $qrm->row();
  }
  function set_trash($mid){

    $this->db->where(array('ID'=>$mid));
    return $this->db->update('tr_message', array('isTrash'=>1));
  }

	/* Nisa. 2 Jan 2012. hapus notif inbox */
	function read_inbox_notification($user_id)
	{
		$data = array(
	        'n_isread' => '1'
			);
		$this->db->where('n_foruserid', $user_id);
		$this->db->where('n_isread', '0');
		$this->db->where('n_type', '9');
		$this->db->update('tr_notification', $data);		
	}
	
}
?>
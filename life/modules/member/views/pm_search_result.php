Search result for <?= $key; ?>

<?php if($search_music) { ?>
	<br />
	<br />
	Music
	<ul>
	<?php foreach($search_music as $s) { ?>
		<li><?= anchor('', $s->title); ?></li>
	<?php } ?>
	</ul>
<?php } ?>

<?php if($search_music_album) { ?>
	<br />
	<br />
	Music Album
	<ul>
	<?php foreach($search_music_album as $s) { ?>
		<li><?= anchor('', $s->title); ?></li>
	<?php } ?>
	</ul>
<?php } ?>

<?php if($search_music_category) { ?>
	<br />
	<br />
	Music Category
	<ul>
	<?php foreach($search_music_category as $s) { ?>
		<li><?= anchor('artist/page/'.$s->g_id, $s->g_name); ?></li>
	<?php } ?>
	</ul>
<?php } ?>

<?php if($search_artist) { ?>
	<br />
	<br />
	Artist
	<ul>
	<?php foreach($search_artist as $s) { ?>
		<li><?= anchor('', $s->artist_name); ?></li>
	<?php } ?>
	</ul>
<?php } ?>

<?php if($search_fans) { ?>
	<br />
	<br />
	Fans
	<ul>
	<?php foreach($search_fans as $s) { ?>
		<li><?= anchor('member/profile/'.$s->up_uid, $s->up_name); ?></li>
	<?php } ?>
	</ul>
<?php } ?>
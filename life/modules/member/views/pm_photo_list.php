<div class="share temp_share" style="display: block;">
	
	<img class="upload_photo" onclick="js:showUploadPhoto()" src="<?= image_asset_url('general/upload_photo.png'); ?>" /> &nbsp;
	<img class="create_album" onclick="js:showCreateAlbum()" src="<?= image_asset_url('general/create_album.png'); ?>" />
	<div class="share_photo_box">
	</div>
	
    <div class="clear"></div>
</div>

<?= anchor("#", 'Share', 'class="photo_share"'); ?><br />
<?= anchor(site_url()."member/photo", 'Back', 'class="back_green"'); ?>

<h3 class="ttl_photo_detail">Judul Album Foto (click to set as primary)</h3>
<ul id="photo-list">
<?php foreach($user_photo_album as $photo){ 
?>
	<li class="thumb">
		<a href="<?= site_url()."member/photo/change_avatar/".$photo->ID ?>"><?= image_asset("album/".$photo->image, '', array('alt'=>'')); ?></a>
		
		<!-- nisa 26 des -->

		<!-- di sini bagian pop up -->
		<div id="blanket" style="display:none;"></div>
		<div id="popUpDiv" style="display:none;">
			<a href="#" onclick="popup('popUpDiv')">Click Me To Close</a>
			<span class="ijoText"><h6>MY PHOTO</h6></span>
			<?= image_asset("album/".$photo->image); ?>
			
			<?= form_open('member/photo/photo_comment_process'); ?>
				<?= form_input('comment'); ?>
				<?= form_hidden('photo_id', $photo->ID); ?>
				<?= form_submit('submit', 'Comment'); ?>
			<?= form_close(); ?>
			
			<?php if($photo_comments) { ?>
				<?php foreach($photo_comments as $c) { ?>
					<?php if($c->photoID == $photo->ID) { ?>
						<?= $c->up_name." - ".$c->comment; ?> <br />
					<?php } ?>
				<?php } ?>
				<br />
			<?php } ?>			
			
			<?php if($photo->like >= 1) { ?>
				<?= $photo->like; ?> member like this - 			
			<?php } ?>	
			
			<?php if(!$this->m_member->is_like_photo($this->session->userdata('user_id'), $photo->ID)) { ?>
				<?= anchor('member/photo/photo_like_process/'.$photo->ID, 'Like'); ?>
			<?php } else { ?>
				You like this
			<?php } ?>
			
		</div>
		
		<a href="#" type="file" onclick="popup('popUpDiv')">Preview</a>
		<!-- endPopup -->
		
		<?php if($photo->memberID == $this->session->userdata['user_id']) { ?>
			<?= anchor('member/photo/photo_delete_process/'.$photo->ID, 'Delete'); ?>
		<?php } ?>
		
		<!-- end nisa 26 des -->
		
		<div style="clear:both;"></div>
	</li>
<?php
} ?>
</ul>
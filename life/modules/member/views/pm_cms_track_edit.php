<?php
	$return = 'track/manage';
	echo anchor($return, 'kembali ke indeks'); ?>
<div class="form_add">
	<table>
	<?php 
		$attr=array();
		$hidden=array('item_id'=>$this->uri->segment(4) ,'prev_page'=>current_url());
		echo form_open_multipart('track/manage/upload', $attr, $hidden); 
		echo '<tr><td>Judul</td><td>'.form_input(array('name'=>'item_title', 'value'=>$item->title)).'</td></tr>';
		echo '<tr><td>Genre</td><td>'.form_dropdown('item_genre', array('1'=>'General', '2'=>'Alternative'), $item->genreID).'</td></tr>';
		echo '<tr><td>Lama</td><td>'.form_input(array('name'=>'item_length', 'value'=>$item->time_length)).'</td></tr>';
		echo '<tr><td>File size</td><td>'.form_input(array('name'=>'', 'value'=>$item->file_size.' Kb', 'disabled'=>'disabled')).'</td></tr>';
		echo '<tr><td>Info</td><td>'.form_textarea(array('name'=>'item_info', 'value'=>$item->post)).'</td></tr>';
		echo '<tr><td>File</td><td>'.form_upload('userfile', '').'</td></tr>';
		echo '<tr><td></td><td>'.form_submit(array('name'=>'submit', 'value'=>'Submit')).'</td></tr>';
		echo form_close();
	?>
	</table>
</div>
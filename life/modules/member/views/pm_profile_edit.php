<!-- all style edited by gilangaramadan 25Des11 -->
<?php if($this->session->flashdata('message')) {?>
	<?= $this->session->flashdata('message'); ?>
<?php } ?>

<div id="editForm" class="grid_8">
<?= form_open(base_url()."member/profile_edit_process", array('id'=>'register_form', 'class'=>'niceform'));?>
    <div class="head-title">
			<h2>Personal Information</h2>
	</div>
	<fieldset style="margin-top: 10px">
		
		<div>
			<label for="first_name">First Name:</label>
			<input type="text" name="first_name" id="first_name" size="32" maxlength="128" value="<?= $name; ?>"/>
		</div>
		<div>
			<label for="last_name">Last Name:</label> 
			<input type="text" name="last_name" id="last_name" size="32" maxlength="128" value="<?= $last_name; ?>"/>
		</div>
		<div>
			<label for="email">Username (email):</label>
			<input type="text" name="email" id="email" size="32" maxlength="128" value="<?= $email; ?>"/>
			<div class="clear"></div>
		</div>
		<div class="seleksi prefix_2">
			<select name="email_type">
				<option value="1" <?php if($email_type=="1") echo"selected";?> >Private</option>
				<option value="0" <?php if($email_type=="0") echo"selected";?> >Public</option>
			</select>
		</div>
		<div>
			<label for="old_password">Old Password:</label>
			<input type="password" name="old_password" id="old_password" size="32" maxlength="32"/>
		</div>
		<div>
			<label for="new_password">New Password:</label>
			<input type="password" name="new_password" id="new_password" size="32" maxlength="32"/>
		</div>
		<div>
			<label for="conf_password">Confirm Password:</label>
			<input type="password" name="conf_password" id="conf_password" size="32" maxlength="32"/>
		</div>
		<div>
			<label for="nick_name">Nick Name:</label>
			<input type="text" name="nick_name" id="nick_name" size="32" maxlength="128" value="<?= $nick_name; ?>"/>
		</div>
		<div class="seleksi prefix_2">
			<select name="nick_name_type" class="seleksi">
				<option value="1" <?php if($nick_name_type=="1") echo"selected";?> >Private</option>
				<option value="0" <?php if($nick_name_type=="0") echo"selected";?> >Public</option>
			</select>
		</div>
		<div>
			<label for="mobile">Mobile Number:</label>
				<input type="text" name="mobile" id="mobile" size="32" maxlength="20" value="<?= $mobile;?>"/>
		</div>
		<div class="seleksi prefix_2">
			<select name="mobile_type" class="seleksi">
				<option value="1" <?php if($mobile_type=="1") echo"selected";?> >Private</option>
				<option value="0" <?php if($mobile_type=="0") echo"selected";?> >Public</option>
			</select>
		</div>
		<div class="clear"></div>
		<div><label for="point">Total Point Privacy:</label></div>
		<div class="seleksi">
			<select name="total_point_privacy_type" class="seleksi">
				<option value="1" <?php if($total_point_privacy_type=="1") echo"selected";?> >Private</option>
				<option value="0" <?php if($total_point_privacy_type=="0") echo"selected";?> >Public</option>
			</select>
		</div>
		<div class="radio" style="margin-bottom: -25px;">
			<fieldset>
			<legend for="gender"><span>Gender:</span></legend>
				<div>
					<input style="width:13px; margin-top:-5px;" type="radio" name="gender" value="M" <?php if($gender_code=="M") echo"checked";?>/><label for="male" >Male</label>
				</div>
				<div>				
					<input style="width:13px; margin-top:-5px;" type="radio" name="gender" value="F" <?php if($gender_code=="F") echo"checked";?>/><label for="female" >Female</label>
				</div>
			</fieldset>
		</div>
		<div class="clear"></div>
		<div class="seleksi prefix_2">
			<select name="gender_type" class="seleksi">
				<option value="1" <?php if($gender_type=="1") echo"selected";?> >Private</option>
				<option value="0" <?php if($gender_type=="0") echo"selected";?> >Public</option>
			</select>
		</div>
		
		<div>
			<label for="birth_date">Date of Birth:</label>
				<div class="seleksi grid_1">
				<select size="1" name="birth_month" id="birth_month">
					<option value="1" <?php if($birth_month=="1") echo"selected";?>>January</option>
					<option value="2" <?php if($birth_month=="2") echo"selected";?>>February</option>
					<option value="3" <?php if($birth_month=="3") echo"selected";?>>March</option>
					<option value="4" <?php if($birth_month=="4") echo"selected";?>>April</option>
					<option value="5" <?php if($birth_month=="5") echo"selected";?>>May</option>
					<option value="6" <?php if($birth_month=="6") echo"selected";?>>June</option>
					<option value="7" <?php if($birth_month=="7") echo"selected";?>>July</option>
					<option value="8" <?php if($birth_month=="8") echo"selected";?>>August</option>
					<option value="9" <?php if($birth_month=="9") echo"selected";?>>September</option>
					<option value="10" <?php if($birth_month=="10") echo"selected";?>>October</option>
					<option value="11" <?php if($birth_month=="11") echo"selected";?>>November</option>
					<option value="12" <?php if($birth_month=="12") echo"selected";?>>December</option>
				</select>
				</div>
				<div class="seleksi grid_1" style="width: 50px;">
				<select size="1" name="birth_date" style="width: 70px;">
					<?php for($i=1 ; $i<=31 ; $i++) {
						echo "<option value='".$i."'";
							if($birth_date==$i) echo"selected";
						echo ">".$i."</option>";
					} ?>
				</select>
				</div>
				<div class="seleksi grid_1">
				<select name="birth_year">
					<?php 
						$year_now=date("Y");			  
							for($i=1980 ; $i<=$year_now ; $i++) {
							echo "<option value='".$i."'";
								if($birth_year==$i) echo"selected";
							echo ">".$i."</option>";
					} ?>
				</select>
				</div>
		</div>
		<div class="clear"></div>
		<div class="prefix_2 seleksi">
			<select name="birthday_type" class="seleksi">
				<option value="1" <?php if($birthday_type=="1") echo"selected";?> >Private</option>
				<option value="0" <?php if($birthday_type=="0") echo"selected";?> >Public</option>
			</select>
		</div>
		<div>
			<label for="status">Status:</label>
				<div class="seleksi" style="width: 150px;">
					<select name="status" id="status" style="width: 170px;">
						<option value="">--Select Status--</option>
						<?php foreach($statuses as $s) {?>
						<option value="<?= $s->id?>" <?php if($status==$s->id) echo"selected";?>><?= $s->name;?></option>
						<?php } ?>
					</select>
				</div>
				<div class="prefix_2 seleksi">
				<select name="status_type" class="seleksi">
					<option value="1" <?php if($status_type=="1") echo"selected";?> >Private</option>
					<option value="0" <?php if($status_type=="0") echo"selected";?> >Public</option>
				</select>
				</div>
			
		</div>
		<div>
			
			<label for="country">Location:</label>
				<div class="seleksi" style="width: 200px;">
					<select name="country" id="country" style="margin-bottom:5px; width: 220px;">
					  <option value="">--Select Country--</option>
						<?php foreach($countries as $s) {?>
							<option value="<?= $s->Code?>" <?php if($country_code==$s->Code) echo"selected";?>><?= $s->Name;?></option>
						<?php } ?>			  
					</select>
				</div>
				<div class="seleksi prefix_2" style="width: 130px;">
					<select name="state" style="margin-bottom:5px; width: 150px;">
						<option value="">--Select State--</option>
						<?php foreach($states as $s) {?>
							<option value="<?= $s->Code?>" <?php if($state_code==$s->Code) echo"selected";?>><?= $s->Name;?></option>
						<?php } ?>			  
					</select>
				</div>
				<div class="seleksi prefix_2" style="width: 130px;">
					<select name="city" style="width: 150px;">
						<option value="">--Select City--</option>
						<?php foreach($cities as $s) {?>
							<option value="<?= $s->ID?>" <?php if($city_code==$s->ID) echo"selected";?>><?= $s->Name;?></option>
						<?php } ?>			  
					</select>
				</div>
			
		</div>
		<div>
			<label for="address">Address:</label>
			<textarea name="address" id="address" cols="38" rows="5"><?= $address?></textarea>
		</div>
		<div>
			<label for="zip_code">Zip Code:</label>
				<input type="text" name="zip_code" style="width: 100px;" size="32" maxlength="32" value="<?= $zip_code;?>"/>
				<div class="prefix_2 seleksi">
				<select name="zip_code_type">
					<option value="1" <?php if($zip_code_type=="1") echo"selected";?> >Private</option>
					<option value="0" <?php if($zip_code_type=="0") echo"selected";?> >Public</option>
				</select>
				</div>
		</div>
		</fieldset>
		
		<div class="head-title">
			<h2>Work and Education</h2>
		</div>
		<fieldset style="margin-top:10px;">
		<div>
			<label for="occupation">Occupation:</label>
			
				<input type="text" name="occupation" id="occupation" size="32" maxlength="128" value="<?= $occupation?>"/>
				<div class="prefix_2 seleksi">
				<select name="occupation_type">
					<option value="1" <?php if($occupation_type=="1") echo"selected";?> >Private</option>
					<option value="0" <?php if($occupation_type=="0") echo"selected";?> >Public</option>
				</select>
				</div>
			
		</div>
		<div>
			<label for="companies">Companies:</label>
			
				<input type="text" name="companies" id="companies" size="32" maxlength="128" value="<?= $companies?>"/>
				<div class="prefix_2 seleksi">
					<select name="companies_type">
						<option value="1" <?php if($companies_type=="1") echo"selected";?> >Private</option>
						<option value="0" <?php if($companies_type=="0") echo"selected";?> >Public</option>
					</select>
				</div>
			
		</div>
		<div>
			<label for="school">School:</label>
			
				<input type="text" name="school" id="school" size="32" maxlength="128" value="<?= $school?>"/>
				<div class="prefix_2 seleksi">
					<select name="school_type" class="seleksi">
						<option value="1" <?php if($school_type=="1") echo"selected";?> >Private</option>
						<option value="0" <?php if($school_type=="0") echo"selected";?> >Public</option>
					</select>
				</div>
		</div>
		</fieldset>
		
		<div class="head-title">
			<h2>Favorite Information</h2>
		</div>
		<fieldset style="margin-top:10px;">
		<div>
				<div class="radio" style="margin-bottom: -25px;">
					<fieldset>
					<legend for="gender"><span>Favourite Genre:</span></legend>
					<div class="seleksi">
						<select name="genre_type">
						  <option value="1" <?php if($genre_type=="1") echo"selected";?> >Private</option>
						  <option value="0" <?php if($genre_type=="0") echo"selected";?> >Public</option>
						</select>
					</div>
					<div class="clear"></div>
					<div class="grid_5 checkbox" style="margin-bottom: 5px;">
						<?php $i=0;
						foreach($genres as $g) {?>
						<div class="grid_2">
							<input type="checkbox" name="fav_genre[]" value="<?= $g->g_id?>" <?php if($fav_genre[$i]==$g->g_id) { echo "checked"; if($i<count($fav_genre)-1) {$i++;} }?>/>
							<label style="margin-top: 7px;" for="<?= $g->g_id?>"><span><?= $g->g_name;?></span></label>
						</div>
						<?php }?>
					</div>
					</fieldset>
				</div>
		</div>
		<div class="clear"></div>
		<div>
			<label for="fav_artist">Favorite Artist:</label>
				<input type="text" name="fav_artist" id="fav_artist" size="32" maxlength="128" value="<?= $fav_artist?>"/>
				<div class="prefix_2 seleksi">
					<select name="fav_artist_type">
						<option value="1" <?php if($fav_artist_type=="1") echo"selected";?> >Private</option>
						<option value="0" <?php if($fav_artist_type=="0") echo"selected";?> >Public</option>
					</select>
				</div>
		</div>
	</fieldset>
	
    
    
    
	<!-- //??
    <div class="row-input">
      <label for="band_name"><span>Band Name</span>:</label>
      <div class="input">
          <input type="text" name="band_name" id="band_name" class="text-input"/>
          <select name="type">
              <option value="Member">Member</option>
          </select>
      </div>
      <div class="clear"></div>
    </div>
	-->
	
	<!-- //??
    <div class="row-input">
      <label for="description"><span>Description</span>:</label>
      <div class="input">
          <textarea name="description" id="description" cols="38" rows="5"></textarea>
          <select name="type">
              <option value="Member">Member</option>
          </select>                
      </div>
      <div class="clear"></div>
    </div>
	-->
	<div class="clear"></div>
	<fieldset>
		<div class="prefix_2 grid_4">
			<div class="radio checkbox">
				<input type="checkbox" name="system_mail" id="system_mail[]" value="1" <?php if($system_mail=="1") echo "checked"?>/>
				<label style="margin-top: 7px;" for="agree">Receive system email</label>
			</div>
		</div>
		<div class="clear"></div>
		<div class="prefix_2">
			<div class="grid_2">
				<button class="save_change" type="submit" name="submit" value="Save Changes" />
			</div>
			<div class="grid_1"> <!-- update gilangaramadan 26Des11 -->
				<button class="cancel_change" type="submit" name="cancel" value="Cancel" />
			</div>
		</div>
	</fieldset>
    <?= form_close();?>
	
	<div class="head-title">
		<h2>My Photo</h2>
	</div>
	<fieldset style="margin-top:10px;">
    <div class="grid_8" style="margin-left: 10px; margin-bottom: 10px;">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec lorem bibendum neque dignissim tincidunt. Pellentesque a tortor vel augue fringilla luctus
	</div>	
	<div class="clear"></div>
	<div class="prefix_2 grid_2" style="margin:0 0 10px 60px;">
	<?= form_open_multipart('member/photo_save'); ?>    
		<div id="file_browse_wrapper">
			<input type="file" name="userfile" id="file_browse" value="upload" />
		</div>
		<button class="save_change" type="submit" name="submit" value="Save Photo" />
	<?= form_close(); ?>
	</div>
	<div class="clear"></div>
	<div class="prefix_2 grid_3" style="margin:0 0 10px 40px;">
	<span class="ijoText" style="margin-left:35px;">Thumbnail save</span>
	<?= form_open('member/thumbnail_save'); ?>    
	<div class="image-selector">
        <div class="frame">
			<?= image_asset('profile/0000/00/00/'.$this->session->userdata('user_id').'.jpg', '', array('alt'=>'profile', 'id'=>"photo")); ?>
        </div>

          <div class="frame-preview">
              <span class="ijoText" style="margin-left:55px;">Preview</span>
              <div id="preview">
					<?= image_asset('profile/0000/00/00/'.$this->session->userdata('user_id').'.jpg', '', array('alt'=>'preview')); ?>
              </div>
          </div>
	
	</div>
	<div class="grid_2" style="margin-left:18px;"> <!-- update gilangaramadan 26Des11 -->
		<button class="save_change" type="submit" name="submit" value="Save Thumbnail" />
	</div>
	<?= form_close();?>
	</fieldset>
<div class="clear"></div>
</div>

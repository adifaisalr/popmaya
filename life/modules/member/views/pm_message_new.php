<div class="head-title">
	<span><strong>Message</strong></span>
</div>
      
<div id="track_list">
    <div class="mssg_control">
    	<?= anchor(site_url().'member/message/box/inbox', 'Back to Inbox')?><br />
    	<?php
    		$path_to=site_url().'member/message/send';
    		$attributes='';
    		$hidden='';
    		echo form_open($path_to, $attributes, $hidden);?>
        <label for="to member">To</label>
        <select name="friend_id">
        <?php 
        	
        	foreach ($list_to as $item) { ?>
        	<option value="<?= $item->f_friendid; ?>"><?= $item->up_name; ?></option>
        <?php } ?>
        </select>

        <br />
        <label>Subject</label><input type="text" name="mssg_subject" placeholder="Subject" />
        <br />
        <label>Message</label><textarea name="mssg_text"></textarea><br />
        <?= form_submit(array('name'=>'submit', 'value'=>'Send Message')); ?>
        <?= form_close(); ?>
    </div>      

    <div class="clear"></div>
</div>      
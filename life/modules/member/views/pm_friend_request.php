<!-- all style edited by gilangaramadan 26Des11 -->
<div class="grid_8">
	<div class="head-title">
		<h2>Friend Request <?= $count_all_friend_request; ?></h2>
	</div>
	<div class="grid_8" style="margin-left: 10px;">
		<?php foreach($friend_request as $f) { ?>
			<div class="clear"></div>
			<div style="margin-top: 10px;"></div>
			<div class="grid_7" style="width: 460px;">
				<div class="grid_4">
					<p>
						<?php
						$path=substr($f->ul_createdon, 0, 10);
						$path=str_replace('-', '/', $path);
						$path=$path."/".$f->up_uid."_thumb.jpg";
						?>
						<?php if(is_file("./assets/images/profile/$path")) { ?>
							<?= image_asset("profile/$path",'',array('class'=>'left', 'style'=>'margin-top:0px;', 'width'=>'60')); ?>
						<?php } else { ?>
							<?= image_asset("profile/thumbnail/what.jpg",'',array('class'=>'left', 'style'=>'margin-top:0px;', 'width'=>'60')); ?>
						<?php } ?>						
						<span class="ijoText"><b><?= anchor('member/profile/'.$f->up_uid, $f->up_name); ?></b></span><br />
						<span class="small biru"><?= $f->mutual; ?> Mutual Friends</span>
					</p>
				</div>
				<div class="grid_3 top right" style="width: 150px;">
					<div class="grid_1">
						<?= anchor('member/friend_approve_process/'.$f->up_uid, 'Confirm', array("class"=>"confirm_friend", "type"=>"submit", "name"=>"submit")); ?>
					</div>
					<div class="grid_1" style="margin-left: 15px;">
						<button class="not_friend" type="submit" name="submit" value="Not Now"/>
					</div>
				</div>
			<hr />
			</div>		
		<?php } ?>
	
		<div class="btn-more" style="width: 450px;"> See More... </div>
		
		<div class="clear"></div>
		<div class="grid_7" style="width: 460px; margin-top: 30px;">
			<div id="editForm">
				<form action="">
				<fieldset>
				<div>
					<label for="username"><span class="abuMonyet large"><b>Friend List <?= $count_friends; ?></b></span></label>
					<input type="text" name="search" size="32" maxlength="128" value="Search"/>
				</div>
				</fieldset>
				</form>
			</div>
			<hr />
		</div>
		
		<?php foreach($friends_all as $fa) { ?>
		<div class="grid_7" style="width: 460px;">		
			<div class="grid_4">
				<p>
					<?php
					$path=substr($fa->ul_createdon, 0, 10);
					$path=str_replace('-', '/', $path);
					$path=$path."/".$fa->up_uid."_thumb.jpg";
					?>
					<?php if(is_file("./assets/images/profile/$path")) { ?>
						<?= image_asset("profile/$path",'',array('class'=>'left', 'style'=>'margin-top:0px;', 'width'=>'60')); ?>
					<?php } else { ?>
						<?= image_asset("profile/thumbnail/what.jpg",'',array('class'=>'left', 'style'=>'margin-top:0px;', 'width'=>'60')); ?>
					<?php } ?>
					<span class="ijoText"><b><?= anchor('member/profile/'.$fa->up_uid, $fa->up_name); ?></b></span><br />
					<span class="small biru">10 Mutual Friends</span>
				</p>
			</div>
			
			<div class="grid_3 top right" style="width: 130px;">
				<?=form_open('member/remove_friend/'.$fa->f_id);?>
					<?//=form_hidden('f_id',$this->uri->segment(3));?>
						<!--<pre><?print_r($fa);?></pre>-->
					<button class="remove_friend" type="submit" name="submit" value="Confirm"/>
				</form>
			</div>
		<hr />
		</div>
		<?php } ?>		
	</div>
</div>
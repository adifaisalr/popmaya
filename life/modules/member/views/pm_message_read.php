<div class="head-title">
	<span><strong>Message Read</strong></span>
</div>
      
<div id="track_list">
    <div class="mssg_control">
    	<?= anchor(site_url().'member/message/box/inbox', 'Back to Inbox')?><br />
        Subject : <?= $mssg_read->subject; ?><br />
        Message :<br />
        <?= nl2br($mssg_read->message); ?>
        <br />
        <br />
    	<?php
    		$path_to=site_url().'member/message/send';
    		$attributes='';
    		$hidden=array('friend_id'=>$reply_to_id); // harus get member id
    		echo form_open($path_to, $attributes, $hidden); ?>
        <!--<label for="to member">To</label><?= $reply_to ?>-->

        <br />
        <label>Subject</label><input type="text" name="mssg_subject" placeholder="Subject" value="RE: <?= $mssg_read->subject ?>"/>
        <br />
        <label>Message</label><textarea name="mssg_text"></textarea><br />
        <?= form_submit(array('name'=>'submit', 'value'=>'Send Message')); ?>
        <?= form_close(); ?>
    </div>      

    <div class="clear"></div>
</div>      
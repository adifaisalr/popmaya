<div class="head-title">
          <span><strong>Message <?= $msg_title; ?></strong><?= $total_rows; ?></span>
      </div>
      
      <div id="track_list">
          <div class="mssg_control">
          <?= anchor(site_url().'member/message/box/new', image_asset('general/btn_new_mssg.png', '', array('alt'=>'New Message'))); ?> <span class="pad_ctr">&nbsp;</span>
          <?= anchor(site_url().'member/message/box/inbox', image_asset('general/btn_inbox.png', '', array('alt'=>'Inbox'))); ?>
          <?= anchor(site_url().'member/message/box/sent', image_asset('general/btn_sent_item.png', '', array('alt'=>'Sent Item'))); ?>
          <?= anchor(site_url().'member/message/box/trash', image_asset('general/btn_trash.png', '', array('alt'=>'Trash'))); ?>
          </div>
          <div class="sort-form">
              <label for="category-music">Inbox</label>
              <select id="category-music">
                  <option>All</option>
                  <option>Deleted</option>
              </select>
          </div>
          <table class="mssg_list">
            <tr class="first_line">
              <th><input type="checkbox" name="select_all" value="" id="select_all"></th>
              <th class="sel_all">Select All</th>
              <th><span class="mssg_main_action"><?= anchor('#', 'Mark as Unread')?>
              <?= anchor('#', 'Mark as Read')?>
              <?= anchor('#', 'Mark as Trash')?></span></th>
            </tr>
            <?php
              if(count($list_messages)==0){
                echo '<tr><td colspan="4"><h2>No message</h2></td></tr>';
              }
              foreach($list_messages as $item) {  // tinggal ganti jadi foreach
            ?>
                 <tr>
                    <td><input type="checkbox" name="cb_item_id" value="" class="cb_item_id"></td>
                    <td class="sender_img"><?= image_asset('general/profile_th.png', '', array('alt'=>$item->up_name)); ?></td>
                    <td>
                      <div class="time_sent"><?= $item->timestamp?><!-- 2 jam yang lalu -->
                      <br />
                      <?= anchor(site_url().'member/message/delete_item/'.$item->ID, 'Delete', 'class="conf_del" title="Hapus item ini?"'); ?>
                      </div>
                      <?= anchor(site_url().'member/profile/'.$item->up_uid, $item->up_name)?><br />
                      <?= anchor('member/message/box/read/'.$item->message_contentID, $item->subject, 'class="mssg_more"'); ?>
                    </td>
                  </tr>
            <?php
              }
            ?>
          </table>
          
          <div class="clear"></div>
          <?= $pagination ?>
          <!--
          Showing 1 of 4 Page.
          <ul class="pagination_r">
            <li><?= anchor('#', '1'); ?></li>
            <li><?= anchor('#', '2'); ?></li>
            <li><?= anchor('#', '3'); ?></li>
          </ul>
          -->
          <div class="clear"></div>
      </div>
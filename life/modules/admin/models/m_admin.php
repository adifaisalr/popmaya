﻿<?php
//Author 	: Firman
//Date		: 17 Desember 2011
//Desc		: 
class M_Admin extends CI_Model 
{
	function get_benefit(){
		$query=$this->db->get('tr_benefit');
		return $query->result();	
	}
	
	function update_benefit($artist,$fan,$brand){
		$data=array('benefit_description'=>$artist);
		$this->db->where('benefit_title','artist');
		$this->db->update('tr_benefit',$data); 
		$data=array('benefit_description'=>$fan);
		$this->db->where('benefit_title','fan');
		$this->db->update('tr_benefit',$data);
		$data=array('benefit_description'=>$brand);
		$this->db->where('benefit_title','brand');
		$this->db->update('tr_benefit',$data);
	}
	
	function add_banner($banner_data){
		$this->db->insert('tr_ads',$banner_data);
	}
	
	function get_banner($banner_type){
		$query=$this->db->select('*')
				->from("tr_ads")
				->where("position",$banner_type)
				->get();
		return $query->result();	
	}
	
	function get_specified_banner($id){
		$query=$this->db->select('*')
				->from("tr_ads")
				->where("ID",$id)
				->get();
		return $query->result();
	}
	
	function update_banner($id,$updated_data){
		$this->db->where('ID',$id);
		$this->db->update('tr_ads',$updated_data);
	}
	
	function delete_banner($id){
		$this->db->where('ID',$id);
		$this->db->delete('tr_ads');
	}
}
?>
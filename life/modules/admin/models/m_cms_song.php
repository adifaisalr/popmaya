<?php // ^zy
class M_Cms_Song extends CI_Model 
{
	function get_paged_list($limit, $offset = 0, $whr, $likey){
		/*
SELECT b.artist_name,  c.title AS album_title, a.*
FROM `tr_song` a 
INNER JOIN 
`tr_member_artist` b
ON 
a.artist_id=b.ID
LEFT JOIN
`tr_album_song` c
ON
a.albumID = c.as_id
		*/

	  	$this->db->select('b.artist_name,  c.title AS album_title, a.*');
      	$this->db->from('tr_song a');
      	$this->db->join('tr_member_artist b', 'a.artist_id=b.ID');
      	$this->db->join('tr_album_song c', 'a.albumID = c.as_id', 'LEFT');

      	if($whr){ 
			$this->db->where($whr);
		}
		if($likey){ 
			$this->db->like($likey);
		}
     	$this->db->limit($limit, $offset);
   		$query = $this->db->get();
   		return $query->result();
	}

	function count_searches($whr, $likey){
		$this->db->select('b.artist_name,  c.title AS album_title, a.*');
      	$this->db->from('tr_song a');
      	$this->db->join('tr_member_artist b', 'a.artist_id=b.ID');
      	$this->db->join('tr_album_song c', 'a.albumID = c.as_id', 'LEFT');
      	if($whr){ 
			$this->db->where($whr);
		}
		if($likey){ 
			$this->db->like($likey);
		}
   		$query = $this->db->get();
   		return $query->num_rows();
	}

}
?>
<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Chat extends MX_Controller 
{ // ^zy
  private $limit = 20;
	function __construct(){
   		parent::__construct();

   		$this->load->library('form_validation');

		  $this->user_id=$this->session->userdata('user_id');
   		
      $this->load->model('m_cms_chat', '', TRUE);
      $this->load->model('chat/m_chat', '', '');

      $this->template->set_master_template('admin_cms');
      $this->template->add_region('ttl');
      $this->template->add_region('content');

      $this->output->enable_profiler(TRUE);
   	}
    
    function room(){
        
        $this->template->write('head_title', 'Lihat Chat Room');
        $data['foot_js']=$data['warn_js']='';

        $whr=$likey='';
        $data['list_items']=$this->m_cms_chat->get_room_paged_list($this->limit,0, array('a.c_room_id'=>$this->uri->segment(4)), $likey);

        /* pagination */ 
        $uri_segment = 5;
        if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($uri_segment); }
        $data['start_no']= $offset+1;

        $this->load->library('pagination');
        $config['base_url'] = site_url().'admin/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4);
        $config['total_rows'] = $this->m_cms_chat->count_all_mssg(array('c_room_id'=>$this->uri->segment(4)));
        $config['per_page'] = $this->limit;
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        /* pagination.end */
        
        $data['foot_js']='
          $(".conf_item").click(function(){
            var url = $(this).attr("href");
            var ttl = $(this).attr("title");
            if(confirm(ttl)) {
              location.href=url;
            } else {
              return false;
            }   
          });
        ';
        $data['item']='';
        if($this->uri->segment(6)){
          $rowc=$this->db->get_where('tr_chat', array('c_id'=>$this->uri->segment(5)));
          $data['item']=$rowc->row();
        }
        $this->template->write('ttl', 'Lihat Chat Room');
        $this->template->write_view('content', 'pm_cms_chat_room_idx', $data, '');
        $this->template->render();       
    }

    function new_room(){
      $arr = array('cr_title'=>$this->input->post('item_title'), 'cr_max_num_ppl'=>$this->input->post('max_ppl'));

      if($this->input->post('mode')=='add'){
        $this->db->insert('tr_chat_room', $arr);  
        if($this->db->insert_id()){
          set_flash('warn','success','Chat room berhasil ditambahakan');
        }else{
          set_flash('warn','notice','Chat room gagal ditambahakan');
        }
      }else{
        $this->db->where(array('cr_id'=>$this->input->post('item_id')));
        if($this->db->update('tr_chat_room', $arr)){
          set_flash('warn','success','Data update berhasil');
        }else{
          set_flash('warn','notice','Data update gagal');
        }
      }      
      redirect($_SERVER['HTTP_REFERER']);
    }

    function index(){
        

        $this->template->write('head_title', 'Chat Management');
        $data['foot_js']=$data['warn_js']='';

        $whr=$likey='';
        $data['chat_rooms']=$this->m_cms_chat->get_paged_list($this->limit,0, $whr, $likey);

        /* pagination */ 
        $uri_segment = 5;
        if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($uri_segment); }
        $data['start_no']= $offset+1;

        $this->load->library('pagination');
        $config['base_url'] = site_url().'admin/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4);
        $config['total_rows'] = $this->m_cms_chat->count_all('');
        $config['per_page'] = $this->limit;
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        /* pagination.end */
        
        $data['foot_js']='
          $(".conf_item").click(function(){
            var url = $(this).attr("href");
            var ttl = $(this).attr("title");
            if(confirm(ttl)) {
              location.href=url;
            } else {
              return false;
            }   
          });
        ';
        $data['item']='';
        if($this->uri->segment(4)){
          $rowc=$this->db->get_where('tr_chat_room', array('cr_id'=>$this->uri->segment(4)));
          $data['item']=$rowc->row();
        }
        $this->template->write('ttl', 'Chat Management');
        $this->template->write_view('content', 'pm_cms_chat_room', $data, '');
        $this->template->render();       
    }

    function set_status($sid){
      $querec=$this->db->get_where('tr_chat_room', array('cr_id'=>$sid));
      if($querec->row()->cr_status==1){
        $rec_val=0;
      }else{
        $rec_val=1;
      }
      $this->db->where(array('cr_id'=>$sid));
      if($this->db->update('tr_chat_room', array('cr_status'=>$rec_val))){
        set_flash('warn', 'success', 'Data Updated');
      }else{
        set_flash('warn', 'notice', 'Data Not Updated');
      }
      redirect($_SERVER['HTTP_REFERER']);
    }

    function search(){
        $tipes=$this->input->post('tipeSearch');
        $item_title = $this->input->post('item_title');
        redirect(site_url().'admin/chat/list_search/'.$tipes.'/'.$item_title);
    }

    function list_search(){
      
        $this->template->write('head_title', 'Song Recommendation');
        $data['foot_js']=$data['warn_js']='';
        $data['foot_script']='
          $(".conf_item").click(function(){
            var url = $(this).attr("href");
            var ttl = $(this).attr("title");
            if(confirm(ttl)) {
              location.href=url;
            } else {
              return false;
            }   
          });
        ';
        if($this->uri->segment(4)=="artist"){
          $sTerm=array('b.artist_name'=>urldecode($this->uri->segment(5)));  
        } else{
          $sTerm=array('c.title'=>urldecode($this->uri->segment(5)));  
        }       

        /* pagination */ 
        $uri_segment = 6;
        if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($uri_segment); }
        $data['start_no']= $offset+1;

        $this->load->library('pagination');
        if($this->uri->segment('6')==''){ $urlVal='a'; }else{
          $urlVal=$this->uri->segment('5');
        }
        
        $config['base_url'] = site_url().'/admin/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$urlVal;

        $config['total_rows'] = $this->m_cms_song->count_searches('', $sTerm);
        $config['per_page'] = $this->limit;
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        /* pagination.end */

        $data['list_search']=$this->m_cms_song->get_paged_list($this->limit, $offset, '', $sTerm);

        $this->template->write('ttl', 'Song Recommendation');
        $this->template->write_view('content', 'pm_cms_recommend_song', $data, '');
        $this->template->render();       
    }
	 
    function del_mssg(){
      $this->db->where('c_id', $this->uri->segment(4));
      if($this->db->delete('tr_chat')){
        set_flash('warn','success','Data deleted');
      }else{
        set_flash('warn','notice','Data not deleted');
      }
      redirect($_SERVER['HTTP_REFERER']);
    }
    
    function set_mssg_status($sid){
      $querec=$this->db->get_where('tr_chat', array('c_id'=>$sid));
      if($querec->row()->c_status==1){
        $rec_val=0;
      }else{
        $rec_val=1;
      }
      $this->db->where(array('c_id'=>$sid));
      if($this->db->update('tr_chat', array('c_status'=>$rec_val))){
        set_flash('warn', 'success', 'Data Updated');
      }else{
        set_flash('warn', 'notice', 'Data Not Updated');
      }
      redirect($_SERVER['HTTP_REFERER']);
    }
    		
}
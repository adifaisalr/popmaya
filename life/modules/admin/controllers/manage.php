﻿<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

//Author	: Firman
//Date		: 16 Descember 2011
//Desc		: class for CMS
class Manage extends MX_Controller 
{
	 	function __construct(){
   		parent::__construct();

   		$this->load->library('form_validation');
   		$this->load->model('m_admin', '', TRUE);

		$this->user_id=$this->session->userdata('user_id');

   		$this->output->enable_profiler(TRUE);

        $this->template->set_master_template('admin_cms');
        $this->template->add_region('ttl');
        $this->template->add_region('content');

   	}
	
	function index()
	{	
		if (!$this->ion_auth->logged_in()) //not logged in
		{
			//redirect to the login page
			redirect('home', 'refresh');
		}
		else //logged in
		{
			redirect("admin/manage/edit_logo", 'refresh');
		}
   	}

		
		
		function edit_logo(){
			$data['foot_js']=js_asset('jquery.imgareaselect.pack.js');
      	
			$this->template->write('head_title', 'Change Logo');
			$this->template->write('ttl', 'Change Logo');
			$this->template->write_view('content', 'logo');
			$this->template->render();      
		}
		
		function save_logo(){
			
			//create directory	
			$dir="./assets/images/";
			
			$config['upload_path']="./assets/images/";
			$config['file_name']="pop_maya_logo";
			$config['allowed_types']='png';
			$config['max_width']='270';
			$config['max_height']='30';
			$config['overwrite']=TRUE;
			
			//upload
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload("site_logo")){
				$error=array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('message', $error);
				$data['error']=$error;
				//print_r($data['error']);
				//redirect('admin/manage/edit_logo',$data);					
			}else{
				$data = array('upload_data' => $this->upload->data());
				$this->template->write('head_title', 'Save Logo');
				$this->template->write('ttl', 'Saved Logo');
				$data['image_property'] = $data;
				$this->template->write_view('content', 'save_logo');
				$this->template->render();
			}
		}

		
		function edit_benefit(){
			$this->config->load('my_config');
			
			if($this->input->post('item_artist')){
				$this->m_admin->update_benefit($this->input->post('item_artist'),$this->input->post('item_fan'),$this->input->post('item_brand'));
			}

			$data['benefit']=$this->m_admin->get_benefit();
			$this->template->write('head_title', 'Edit Benefit');
			$this->template->write('ttl', 'Edit Benefit');
			$this->template->write_view('content', 'benefit', $data);
			$this->template->render();
			
		}
		

		function add_banner($param){	
			$banner_type = $this->uri->segment(4);
			if($this->input->post('submit')){
				//create directory	
				$dir="./assets/images/banner/".$banner_type."/";
				
				switch($banner_type){
					case "ads":{
						$width = 220;
						$height = 168;
					}break;
					case "profile":{
						$width = 479;
						$height = 184;
					}break;
					case "home":{
						$width = 410;
						$height = 300;
					}break;
				}
				$this->load->library('image_lib');
				//$config['image_library'] = 'gd2';
				$config['upload_path']="./assets/images/banner/".$banner_type."/";
				$config['file_name']="banner_".$banner_type;
				$config['allowed_types']='png|jpg|gif|bmp';
				$config['width']=$width;
				$config['height']=$height;
				$config['maintain_ratio'] = FALSE;
				$config['overwrite']=FALSE;
				
				
				//upload
				$this->load->library('upload', $config);
		
				if (!$this->upload->do_upload("banner_".$banner_type."_image"))
				{
					
					$error=array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('message', $error);
					$data['error']=$error;
					print_r($data['error']);
					//redirect('admin/manage/edit_logo',$data);
				}else{
					$data = array('upload_data' => $this->upload->data());
					
					
					
					$data['image_property']=$data;
					//construct array banner to insert
					foreach($data as $dummy){
					foreach($dummy as $image_property){
						$config['source_image']    = $image_property['full_path'];
						$this->image_lib->initialize($config); 
						if ( ! $this->image_lib->resize()){
							//echo $this->image_lib->display_errors();
						}
						$insert_data = array(
							'title' => $this->input->post($banner_type.'_title') ,
							'url' => $this->input->post($banner_type.'_url') ,
							'script' => $this->input->post($banner_type.'_script') ,
							'image' => $image_property['file_name'],
							'size' => $width."x".$height,
							'online' => $this->input->post('on_stat_'.$banner_type) ,
							'offline' => (1-$this->input->post('on_stat_'.$banner_type)) , 
							'impression' => 0 ,
							'moduleID' => 'Dummy' ,
							'position' => $banner_type,
							'type' => ($banner_type=="ads")? 'static':'dynamic',
							'priority' => 0,
							'ordering' => 0
						);
						
						$data['banner_title'] = $this->input->post($banner_type.'_title');
						$data['banner_url'] = $this->input->post($banner_type.'_url');
						$data['banner_type']= $banner_type;
					}
					}				

					//insert ads data to database
					$this->m_admin->add_banner($insert_data);
					
					//display view
					//print_r($data['image_property']);
					$this->template->write('head_title', 'New '.$banner_type.' banner saved');
					$this->template->write('ttl', 'New '.$banner_type.' banner saved');
					$this->template->write_view('content', 'banner_saved_notif',$data);
					$this->template->render();
				}	
			}else{
				if(strcmp($banner_type,"profile")!=0){
					$data['banner_data'] = $this->m_admin->get_banner($banner_type);
				}else{
					$data['banner_data'] = $this->m_admin->get_banner('profi');
				}
				$data['banner_type']= $banner_type;
				$this->template->write('head_title', 'Add '.$banner_type.' banner');
				$this->template->write('ttl', 'Add '.$banner_type.' banner');
				$this->template->write_view('content', 'add_banner',$data);
				$this->template->render();
			}
		}
		
		function edit_banner($id="",$banner_type){
			if (!$this->ion_auth->logged_in()) //not logged in
			{
				redirect('home', 'refresh');
			}
			else
			{
				$data['banner_type'] = $banner_type;
				$data['banner'] = $this->m_admin->get_specified_banner($id);
				$this->template->write('head_title', 'Edit '.$banner_type.' banner');
				$this->template->write('ttl', 'Edit '.$banner_type.' banner');
				$this->template->write_view('content', 'edit_banner',$data);
				$this->template->render();
			}
		}
		
		function save_edited_banner($id="",$banner_type=""){
			if($this->input->post('submit')){
				//create directory	
				$dir="./assets/images/banner/".$banner_type."/";
				
				switch($banner_type){
					case "ads":{
						$width = 220;
						$height = 168;
					}break;
					case "profile":{
						$width = 479;
						$height = 184;
					}break;
					case "home":{
						$width = 410;
						$height = 300;
					}break;
				}
				$this->load->library('image_lib');
				//$config['image_library'] = 'gd2';
				$config['upload_path']="./assets/images/banner/".$banner_type."/";
				$config['file_name']="banner_".$banner_type;
				$config['allowed_types']='png|jpg|gif|bmp';
				$config['width']=$width;
				$config['height']=$height;
				$config['maintain_ratio'] = FALSE;
				$config['overwrite']=TRUE;
				
				
				//upload
				$this->load->library('upload', $config);
		
				if (!$this->upload->do_upload("banner_".$banner_type."_image"))
				{
					
					$error=array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('message', $error);
					$data['error']=$error;
					print_r($data['error']);
					//redirect('admin/manage/edit_logo',$data);
				}else{
					$data = array('upload_data' => $this->upload->data());
					
					
					
					$data['image_property']=$data;
					//construct array banner to insert
					foreach($data as $dummy){
					foreach($dummy as $image_property){
						$config['source_image']    = $image_property['full_path'];
						$this->image_lib->initialize($config); 
						if ( ! $this->image_lib->resize()){
							//echo $this->image_lib->display_errors();
						}
						$updated_data = array(
							'title' => $this->input->post($banner_type.'_title') ,
							'url' => $this->input->post($banner_type.'_url') ,
							'script' => $this->input->post($banner_type.'_script') ,
							'image' => $image_property['file_name'],
							'size' => $width."x".$height,
							'online' => $this->input->post('on_stat_'.$banner_type) ,
							'offline' => (1-$this->input->post('on_stat_'.$banner_type)) , 
							'impression' => 0 ,
							'moduleID' => 'Dummy' ,
							'position' => $banner_type,
							'type' => ($banner_type=="ads")? 'static':'dynamic',
							'priority' => 0,
							'ordering' => 0
						);
						
						$data['banner_title'] = $this->input->post($banner_type.'_title');
						$data['banner_url'] = $this->input->post($banner_type.'_url');
						$data['banner_type']= $banner_type;
					}
					}				

					//insert ads data to database
					$this->m_admin->update_banner($id,$updated_data);
					
					redirect('admin/manage/add_banner/'.$banner_type,'refresh');
				}	
			}else{
				redirect('admin/manage','refresh');
			}
		}
		
		function delete_banner($id="",$banner_type=""){
			if (!$this->ion_auth->logged_in()) //not logged in
			{
				//redirect to the login page
				redirect('home', 'refresh');
			}
			else{ //logged in
				$this->m_admin->delete_banner($id);
				redirect('admin/manage/add_banner/'.$banner_type,'refresh');
			}
		}
			
		
		
}
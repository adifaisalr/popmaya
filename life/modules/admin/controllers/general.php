<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class General extends MX_Controller 
{	// ^zy 
	function __construct(){
   		parent::__construct();

//		$this->user_id=$this->session->userdata('user_id');
   		$this->output->enable_profiler(TRUE);

    	$this->template->set_master_template('admin_cms');
    	$this->template->add_region('ttl');
    	$this->template->add_region('content');
   	}
	
	function index()
	{	
		redirect(site_url().'admin/general/limit_profile');
   	}

   	function admin_tools_artist(){
   		$data['foot_js']='';
   		$data['admin_tools']=$this->global_model->admin_tools();
   		$this->template->write('head_title', 'Setting Admin Tools');
   		$this->template->write('ttl', 'Admin Tools');
        $this->template->write_view('content', 'pm_cms_admin_tools', $data, '');
        $this->template->render();
   	}

   	function process_admin_tools(){
   		$this->global_model->update_ms_artist_tools('use_as', $this->input->post('use_as'));
   		$this->global_model->update_ms_artist_tools('edit_profile', $this->input->post('edit_profile'));
   		$this->global_model->update_ms_artist_tools('upload_album', $this->input->post('upload_album'));
   		$this->global_model->update_ms_artist_tools('blast_email', $this->input->post('blast_email'));
   		$this->global_model->update_ms_artist_tools('show_app', $this->input->post('show_app'));
   		$this->global_model->update_ms_artist_tools('mob_premium', $this->input->post('mob_premium'));

   		set_flash('warn', 'success', 'Update berhasil.');
		redirect($_SERVER['HTTP_REFERER']);
   	}

   	function tarif_mobile(){
   		$data['foot_js']='';
   		$data['tarif_mobile']=$this->global_model->tarif_mobile();
   		$this->template->write('head_title', 'Setting Tarif Mobile');
   		$this->template->write('ttl', 'Tarif Premium Mobile');
        $this->template->write_view('content', 'pm_cms_tarif_mobile', $data, '');
        $this->template->render();
   	}

   	function process_tarif_mobile(){
   		$this->global_model->update_ms_tarif('lagu', $this->input->post('lagu'));
   		$this->global_model->update_ms_tarif('notes', $this->input->post('notes'));
   		$this->global_model->update_ms_tarif('wallpaper', $this->input->post('wallpaper'));
   		set_flash('warn', 'success', 'Tarif berhasil diperbaharui.');
		redirect($_SERVER['HTTP_REFERER']);
   	}

   	function limit_profile(){
   		$data['foot_js']='';
   		$data['limit_profile']=$this->global_model->limit_profile();
   		$this->template->write('head_title', 'Setting Limit Profile Page');
   		$this->template->write('ttl', 'Limit Profile Page');
        $this->template->write_view('content', 'pm_cms_limit_profile', $data, '');
        $this->template->render();
   	}

   	function process_limit_profile(){

   		$val  =		"<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); \n"; 
		$val .=  " \n";
		$val .= '$config["recommend_song"] = \''.$this->input->post('recommend_song').'\';';
		$val .=  " \n";
		$val .= '$config["new_song"] = \''.$this->input->post('new_song').'\';';
		$val .=  " \n";
		$val .= '$config["fave_song"] = \''.$this->input->post('fave_song').'\';';
		$val .=  " \n";
		$val .= '$config["artist"] = \''.$this->input->post('artist').'\';';
		$val .=  " \n";
		$val .= '$config["friend"] = \''.$this->input->post('friend').'\';';
		$val .=  " \n";
		$val .= '$config["recent_activity"] = \''.$this->input->post('recent_activity').'\';';
		$val .=  " \n";
		$val .= '$config["artist_page_fans"] = \''.$this->input->post('artist_page_fans').'\';';
		$val .=  " \n";
		$val .= '$config["suggest_friend"] = \''.$this->input->post('suggest_friend').'\';';
		$val .=  " \n";
		$val .= '$config["suggest_song"] = \''.$this->input->post('suggest_song').'\';';
		$val .=  " \n";
		$val .= '$config["suggest_artist"] = \''.$this->input->post('suggest_artist').'\';';
		$val .=  " \n";
		$val .= '$config["fan_artist"] = \''.$this->input->post('fan_artist').'\';';
		$val .=  " \n\n";
		$val .= '?>';
		$filename = './life/config/limit_config.php';
		$fp=fopen($filename,"w+"); 
		fwrite($fp,$val); 
		fclose($fp); 	

		// update tabel ms_limit
		$this->global_model->update_ms_limit('recommend_song', $this->input->post('recommend_song'));
		$this->global_model->update_ms_limit('new_song', $this->input->post('new_song'));
		$this->global_model->update_ms_limit('fave_song', $this->input->post('fave_song'));
		$this->global_model->update_ms_limit('artist', $this->input->post('artist'));
		$this->global_model->update_ms_limit('friend', $this->input->post('friend'));
		$this->global_model->update_ms_limit('recent_activity', $this->input->post('recent_activity'));
		$this->global_model->update_ms_limit('artist_page_fans', $this->input->post('artist_page_fans'));
		$this->global_model->update_ms_limit('suggest_friend', $this->input->post('suggest_friend'));
		$this->global_model->update_ms_limit('suggest_song', $this->input->post('suggest_song'));
		$this->global_model->update_ms_limit('suggest_artist', $this->input->post('suggest_artist'));
		$this->global_model->update_ms_limit('fan_artist', $this->input->post('fan_artist'));

		set_flash('warn', 'success', 'Configuration Updated');
		redirect($_SERVER['HTTP_REFERER']);
   	}
}
?>
<?php

/*
Nama: Nisa
Tanggal: 21 Nov 2011
Deskripsi: Home model
*/

class M_Home extends CI_Model 
{
	/*
	Nama: Nisa
	Tanggal: 8 Des 2011
	Deskripsi: Get updated dari artist (harusnya). Tapi ini masih bingung tabelnya jadinya ambil dari update status orang random.
				Itu hasil kesepakatan ketemu mas Lavanda di tuseb Unpar
	*/	
	function get_updated_artist()
	{
		$query=$this->db->select('tr_wall_item.*, up_uid, up_name')
			->from('tr_wall_item')
			->join('tr_user_profile', 'memberID=up_uid')
			->order_by('created_on', 'random')
			->limit('1')
			->get();
		return $query->row();
	}

	/*
	Nama: Nisa
	Tanggal: 8 Des 2011
	Deskripsi: Get recommended track
	*/	
	function get_recommended_track()
	{
		$query=$this->db->select('tr_song.*')
			->from('tr_song')			
			->order_by('created_on', 'desc')
			->where('ispromo', '1')
			->get();
		return $query->result();
		//->join('tr_member_artist', )
		//, tr_member_artist.artist_name
	}
	
	/*
	Nama: Nisa
	Tanggal: 8 Des 2011
	Deskripsi: Get new track sebanyak limit
	*/	
	function get_new_track($limit)
	{
		$query=$this->db->order_by('created_on', 'desc')->limit($limit)->get('tr_song');
		return $query->result();
	}	
}
?>
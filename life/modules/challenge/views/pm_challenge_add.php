Photo Challenge Add
<br />
<br />

<?php if($this->session->flashdata('message')) {?>
	<?= $this->session->flashdata('message'); ?>
<?php } ?>

<?= form_open_multipart('challenge/challenge_add_process'); ?>
	Buat Photo Challenge
	<br />
	<?= form_label('Title'); ?>
	<?= form_input('title', ''); ?>
	<br />
	<?= form_label('Picture'); ?>
	<?= form_upload('userfile')?>	
	<br />
	<?= form_label('Image'); ?>
	<?= form_input('image', ''); ?>
	<br />
	<?= form_label('Description'); ?>
	<?= form_textarea('description'); ?>
	<br />
	<?= form_submit('submit', "Kirim"); ?>
<?= form_close(); ?>
<?php
class M_Fans extends CI_Model 
{
	function getUserProfile($id)
	{
		$query=$this->db->select('tr_user_profile.*, tr_user_login.ul_email')
				->from('tr_user_profile')
				->join('tr_user_login', 'ul_id=up_uid')
				->where('up_uid', $id)
				->get();
		return $query->row();
	}
	
	//Author		: Firman
	//Date			: 2011 Dec 15
	//Desc			: Get list of members
	function get_fans_list($page){
		//add join (nova) 21-12-2011
		$this->db->select('*')->from('tr_user_profile')->join('tr_friend','tr_user_profile.up_uid = tr_friend.f_friendid','left')->limit(10,$page*10);		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	
	function get_fans_list_criteria($page,$gender,$start_age,$end_age){
		//add join (nova) 21-12-2011
		$this->db->select('*')
			->from('tr_user_profile')
			->join('tr_friend','tr_user_profile.up_uid = tr_friend.f_friendid','left')
			->where('up_gender',$gender)
			->limit(10,$page*10);		
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	
	//Author		: Firman
	//Date			: 2011 Dec 18
	//Desc			: Count the page needed for showing members
	function get_fans_page_amount(){
		$query=$this->db->get('tr_user_profile');
		return ((int)($query->num_rows()/10)+1);
	}
	
	//Author		: Firman
	//Date			: 2011 Dec 18
	//Desc			: get number of member
	function get_num_fans(){
		$query=$this->db->get('tr_user_profile');
		return $query->num_rows();
	}
	
	//Author		: Firman
	//Date			: 2011 Dec 18
	//Desc			:get image ads
	function get_image_ads()
	{
		$query = $this->db->get_where('tr_ads', array('position' => 'ads'));
			$this->db->order_by('RAND()','asc');
			$this->db->limit(1);
		return $query->result();		
	}
	
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Get all genre
	*/
	function get_genre()
	{
		$query=$this->db->get('tr_genre');
		return $query->result();
	}
	
	/*
	Nama: Adi
	Tanggal: 16 Des 2011
	Deskripsi: Get all artist
	*/
	function get_artist()
	{
		$query=$this->db->get('tr_member_artist');
		return $query->result();
	}
	
	/*
	Nama: Nisa
	Tanggal: 24 Nov 2011
	Deskripsi: Get fav genre
	*/	
	function get_fav_genre($id)
	{
		$query=$this->db->select('up_favgenre')
			->from('tr_user_profile')
			->where('up_uid', $id)
			->get();
		$row=$query->row();
		return $row->up_favgenre;
	}
	
	/*
	Nama: Adi
	Tanggal: 16 Desember 2011
	Deskripsi: Get fav artist
	*/	
	function get_fav_artist($id)
	{
		$query=$this->db->select('up_favartist')
			->from('tr_user_profile')
			->where('up_uid', $id)
			->get();
		$row=$query->row();
		return $row->up_favartist;
	}
	
	/*
	Nama: Nisa
	Tanggal: 24 Nov 2011
	Deskripsi: Get my fav song list
	*/	
	function get_fav_song($id, $limit)
	{
		$query=$this->db->select('tr_member_artist.title as artist, tr_song.title')
			->from('tr_fave_song')
			->join('tr_song', 'tr_song.s_id=tr_fave_song.songID')
			->join('tr_member_artist', 'tr_member_artist.memberID=tr_song.memberID')
			->where('tr_fave_song.memberID', $id)
			->limit($limit)
			->get();
		return $query->result();
	}
	
	/* zy. temporary model */
	function get_fave($id, $limit)
	{
		$this->db->where(array('memberID'=>$id));
		$query=$this->db->get('tr_fave_song');
		return $query->result();
	}
	
	/*
	Nama: Nisa
	Tanggal: 24 Nov 2011
	Deskripsi: Get song by genre
	*/	
	//??
	function get_song_by_genre($id, $limit)
	{
		$query=$this->db->select('tr_member_artist.title as artist, tr_song.title')
			->from('tr_song')
			->join('tr_member_artist', 'tr_member_artist.memberID=tr_song.memberID')
			->where('tr_song.genreID', $id)
			->limit($limit)
			->get();
		return $query->result();		
	}
	
}
?>
<div id="sidebar">
	<?php if($this->session->userdata('user_id')){ ?>
      <div class="profile-picture">
          <?php
            if(file_exists('./static/profilepic/public/what.png')){
              echo image_profile($user_id.'.jpg', array('alt'=>'profile'));  
            }else{
              echo image_asset('profile-pict.jpg', '', array('alt'=>'profile'));
            }
             
          ?>
      </div>
      <div class="profiler">
          <h2><?= $name; ?></h2>
          <span><?= $gender; ?></span>
          <span><?= $city; ?> - <?= $country; ?></span>
      </div>  
     <div class="list-left">              
        <?= my_song_list($up_uid); ?>
        <div class="divider">&nbsp;</div>
      </div>
	<?php } ?> 
      <div class="list-left">
         <?= sidebar_left_suggest_song($up_uid, $user_id); ?>
      </div>
  </div>

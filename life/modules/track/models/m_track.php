<?php
class M_Track extends CI_Model 
{
 	function save($mode, $whr, $arr){
      if($mode=='add'){
         $this->db->insert('tr_song', $arr);
         return $this->db->insert_id();   
      }else{
         if($whr){
            $this->db->where($whr);   
         }
         return $this->db->update('tr_song', $arr);
      }
   	}

   	/*function get_paged_list($limit, $offset = 0, $whr){
	  	$this->db->select('*');
      	$this->db->from('tr_song');
      	if($whr){ $this->db->where($whr); }
     	$this->db->limit($limit, $offset);
   		$query = $this->db->get();
   		return $query->result();
	}*/
	
	function get_paged_list($limit, $offset = 0, $whr){
	  	$this->db->select('*');
      	$this->db->from('tr_song');
      	if($whr){ 
			$this->db->where($whr); 
			//echo $whr;
		}
     	$this->db->limit($limit, $offset);
   		$query = $this->db->get();
   		return $query->result();
	}

	//edited by Adi 
	//27 dec 2011
	function item_info($item_id){
	   
	   $query = $this->db->select('*')
	   ->from('tr_song')
	   ->where('s_id', $item_id)
	   ->get();
	   if($row = $query->row()){
	      return $row;
	   }else{
	      return false;
	   }	   
	}
	
	//name : Adi
	//27 Dec 2011
	//get artist name by song
	function get_artist_name($id){
		$query = $this->db->select('artist_name')
		->from('tr_member_artist')
		->join('tr_song','tr_member_artist.ID=tr_song.artist_id')
		->where('s_id',$id)
		->get();
		if($row = $query->row()){
			return $row->artist_name;
		}else{
			return 'unknown';
		}
	}
	
	//name : Adi
	//27 Dec 2011
	//get album name by song
	function get_album_name($id){
		$query = $this->db->select('tr_album_song.title as album_title')
		->from('tr_album_song')
		->join('tr_song','tr_album_song.as_id=tr_song.albumID')
		->where('s_id',$id)
		->get();
		if($row = $query->row()){
			return $row->album_title;
		}else{
			return 'unknown';
		}
	}

	function count_all($whr){
	   	if($whr){ $this->db->where($whr); }
	   	$this->db->select('s_id');
	   	$this->db->from('tr_song');
      	$query=$this->db->get();
		return $query->num_rows();
	}

	function remove_item($item_id){	   
	   return $this->db->delete('tr_song', array('s_id' => $item_id)); 
	}
	
	//Name		: Firman
	//Date		: 18 Des 2011
	//Desc		: Get image ads
	function get_image_ads()
	{
		$query = $this->db->get_where('tr_ads', array('position' => 'ads'));
			$this->db->order_by('RAND()','asc');
			$this->db->limit(1);
		return $query->result();		
	}
	
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Get all genre
	*/
	function get_genre()
	{
		$query=$this->db->get('tr_genre');
		return $query->result();
	}
	
	/*
	Nama: Adi
	Tanggal: 16 Des 2011
	Deskripsi: Get all artist
	*/
	function get_artist()
	{
		$query=$this->db->get('tr_member_artist');
		return $query->result();
	}
	
	/*
	Nama: Nisa
	Tanggal: 24 Nov 2011
	Deskripsi: Get fav genre
	*/	
	function get_fav_genre($id)
	{
		$query=$this->db->select('up_favgenre')
			->from('tr_user_profile')
			->where('up_uid', $id)
			->get();
		$row=$query->row();
		return $row->up_favgenre;
	}
	
	/*
	Nama: Adi
	Tanggal: 16 Desember 2011
	Deskripsi: Get fav artist
	*/	
	function get_fav_artist($id)
	{
		$query=$this->db->select('up_favartist')
			->from('tr_user_profile')
			->where('up_uid', $id)
			->get();
		$row=$query->row();
		return $row->up_favartist;
	}
	
	/*
	Nama: Nisa
	Tanggal: 24 Nov 2011
	Deskripsi: Get my fav song list
	*/	
	function get_fav_song($id, $limit)
	{
		$query=$this->db->select('tr_member_artist.title as artist, tr_song.title')
			->from('tr_fave_song')
			->join('tr_song', 'tr_song.s_id=tr_fave_song.songID')
			->join('tr_member_artist', 'tr_member_artist.memberID=tr_song.memberID')
			->where('tr_fave_song.memberID', $id)
			->limit($limit)
			->get();
		return $query->result();
	}
	
	/* zy. temporary model */
	function get_fave($id, $limit)
	{
		$this->db->where(array('memberID'=>$id));
		$query=$this->db->get('tr_fave_song');
		return $query->result();
	}
	
	/*
	Nama: Nisa
	Tanggal: 24 Nov 2011
	Deskripsi: Get song by genre
	*/	
	//??
	function get_song_by_genre($id, $limit)
	{
		$query=$this->db->select('tr_member_artist.title as artist, tr_song.title')
			->from('tr_song')
			->join('tr_member_artist', 'tr_member_artist.memberID=tr_song.memberID')
			->where('tr_song.genreID', $id)
			->limit($limit)
			->get();
		return $query->result();		
	}
	
	/* Adi. 28 Des 2011. komentari song */
	function song_comment($comment, $songID, $user_id, $ip_addr)
	{
		//insert wall comment
		$data = array(
		   'songID' => $songID,
		   'memberID' => $user_id,
		   'comment' => $comment,
		   'post' => now(),
		   'host' => $ip_addr
		);
		$this->db->insert('tr_song_comment', $data);
		
		/*
		//cari tau siapa yg punya wall
		$query=$this->db->get_where('tr_wall_item', array('ID'=>$wall_itemID));
		$result=$query->row();		
		$user_have_wall=$result->memberID;
		
		//insert notification
		$data = array(
		   'n_type' => '1',
		   'n_itemid' => $wall_itemID,
		   'n_fromuserid' => $user_id,
		   'n_foruserid' => $user_have_wall,
		   'n_isread' => '0'
		);
		$this->db->insert('tr_notification', $data);*/				
	}
	
	/* Adi. 28 Des 2011. ambil komentar2 terhadap suatu song */
	function get_song_comment($song_id)
	{		
		$query=$this->db->select('tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_membersince, tr_song_comment.*')
			->from('tr_song_comment')
			->join('tr_user_profile', 'tr_user_profile.up_uid=tr_song_comment.memberID')	
			->where('tr_song_comment.songID', $song_id)
			->order_by('post')
			->get();
		return $query->result();
	}
	
	/* Adi. 28 Des 2011. aksi klik like suatu song */
	function song_like($s_id, $user_id)
	{
		//get jumlah like
		$query=$this->db->get_where('tr_song', array('s_id'=>$s_id));
		$result=$query->row();
		$like=$result->like;
		
		//update jumlah like
		$data = array(
	        'like' => $like+1
		);
		$this->db->where('s_id', $s_id);
		$this->db->update('tr_song', $data);

		/*
		//cari tau siapa yg punya wall
		$query=$this->db->get_where('tr_wall_item', array('ID'=>$wall_id));
		$result=$query->row();		
		$user_have_wall=$result->memberID;
		
		//insert notification
		$data = array(
		   'n_type' => '3',
		   'n_itemid' => $wall_id,
		   'n_fromuserid' => $user_id,
		   'n_foruserid' => $user_have_wall,
		   'n_isread' => '0'
		);
		$this->db->insert('tr_notification', $data);*/		
	}
	
	//Adi. 28 Des 2011. ambil jumlah like
	function get_song_like($s_id){
		$query=$this->db->get_where('tr_song', array('s_id'=>$s_id));
		$result=$query->row();
		return $result->like;
	}
}
?>
Login

<br /><br />

<div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            appId      : '260878907292811',
            status     : true, 
            cookie     : true,
            xfbml      : true
          });
        };
        (function(d){
           var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
           js = d.createElement('script'); js.id = id; js.async = true;
           js.src = "//connect.facebook.net/en_US/all.js";
           d.getElementsByTagName('head')[0].appendChild(js);
         }(document));
      </script>
<div class="fb-login-button">Login with Facebook</div>

<br /><br />

<?= anchor('', 'Twitter Login')?>

<br /><br />

<?= form_open(base_url()."member/login"); ?>

	<?= form_input(array('id'=>"email", 'name'=>"email", 'tabindex'=>"1", 'placeholder'=>"Email", 'tabindex'=>'1')); ?>
	<?= form_password(array('id'=>"password", 'name'=>"password", 'tabindex'=>"2", 'placeholder'=>"Password", 'tabindex'=>'2')); ?>

	<?= form_checkbox(array('name'=>"remember", 'id'=>"remember", 'value'=>true, 'checked'=>false, 'title'=>"Remember Me", 'tabindex'=>'3')); ?> Remember Me
	<?= anchor(base_url()."member/forgot", "Forgot Password"); ?>
	
	<?= form_submit('signin', "Sign In"); ?>
	
<?= form_close(); ?>

<br /><br />

Register

<?= form_open(base_url()."member/register"); ?>

	<?= form_input(array('id'=>"fullname", 'name'=>"fullname", 'placeholder'=>"Fullname", 'tabindex'=>'5')); ?>
	<?= form_input(array('id'=>"email", 'name'=>"email", 'placeholder'=>"Email", 'tabindex'=>'6')); ?>
	<?= form_password(array('id'=>"password", 'name'=>"password", 'placeholder'=>"Password", 'tabindex'=>'7')); ?>
	
	<?= form_label("Gender", 'gender')?>
	<?= form_radio(array('name'=>"gender", 'id'=>"gender", 'checked'=>true, 'title'=>"Male", 'tabindex'=>'8')); ?> Male
	<?= form_radio(array('name'=>"gender", 'id'=>"gender", 'checked'=>false, 'title'=>"Female", 'tabindex'=>'9')); ?> Female

	<?= form_checkbox(array('name'=>"agree", 'id'=>"agree", 'checked'=>false, 'value'=>'agreed', 'title'=>"I agree with the Term & Service", 'tabindex'=>'10')); ?> I agree with the <?= anchor(base_url()."member/toc", "Term &amp; Service");?>
	<?= form_checkbox(array('name'=>"update", 'id'=>"update", 'checked'=>false, 'value'=>'1', 'title'=>"Yes, please send poploud latest update", 'tabindex'=>'11')); ?> Yes, please send poploud latest update
	
	<?= form_submit('register', "Register"); ?>
	
<?= form_close(); ?>
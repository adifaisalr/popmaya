<div id="sidebar">
      <div class="profile-picture">
       		<?php
            if(file_exists('./static/profilepic/public/what.png')){
              echo image_profile($user_id.'.jpg', array('alt'=>'profile'));  
            }else{
              echo image_asset('profile-pict.jpg', '', array('alt'=>'profile'));
            }
             
          ?>
      </div>
      <div class="profiler">
          <h2><?= $name; ?></h2>
          <span><?= $gender; ?></span>
          <span><?= $city; ?> - <?= $country; ?></span>
      </div>  
      <div class="profile-link">
          <ul>
              <li><a href="#" class="info active" title="Info">Info</a></li>
              <li><a href="#" class="status" title="Status">Status</a></li>
              <li><a href="#" class="voice" title="Voice">Voice</a></li>
              <li><a href="#" class="photo" title="Photo">Photo</a></li>
              <li><a href="#" class="video" title="Video">Video</a></li>
              <li><a href="#" class="note" title="Note">Note</a></li>
          </ul>
      </div>
      <div class="mtop50" ></div>
      <div class="list-left">
          <h3>My Song List</h3>
          Your list is empty.<br />
          Find your Fav music here.
          <div class="clear"></div>
      </div>
      
      <hr class="line_spacer" />
      
      <div class="list-left">
         Suggest song by<br />
         <select name="song_genre" id="song_genre">
            <option value="">Genre</option>
            <option value="Alternative">Alternative</option>
         </select>
         <ul>
              <li>
                  <a href="#"><?= image_asset('thumb_song_list1.jpg', '', array('alt'=>'ERK', 'class'=>'thumb-list')) ?></a>
                  <h4><a href="#">ERK</a></h4>
                  <span>Dibuang Sayang</span>
                  <div class="button-share">
                     <a href="#" title="Play"><?= image_asset('play_ico.png', '', array('alt'=>'play')) ?></a>
                     <a href="#" title="Download"><?= image_asset('download_ico.png', '', array('alt'=>'download')) ?></a>
                     <a href="#" title="Share"><?= image_asset('share_ico.png', '', array('alt'=>'share')) ?></a>
                     <a href="#" class="last" title="Add"><?= image_asset('add_ico.png', '', array('alt'=>'add')) ?></a>
                  </div>
              </li>
              <li>
                  <a href="#"><?= image_asset('thumb_song_list2.jpg', '', array('alt'=>'Paramore', 'class'=>'thumb-list')) ?></a>
                  <h4><a href="#">Paramore</a></h4>
                  <span>The Only Exception</span>
                  <div class="button-share">
                      <a href="#" title="Play"><?= image_asset('play_ico.png', '', array('alt'=>'play')) ?></a>
                      <a href="#" title="Download"><?= image_asset('download_ico.png', '', array('alt'=>'download')) ?></a>
                      <a href="#" title="Share"><?= image_asset('share_ico.png', '', array('alt'=>'share')) ?></a>
                      <a href="#" class="last" title="Add"><?= image_asset('add_ico.png', '', array('alt'=>'add')) ?></a>
                   </div>
              </li>
              <li>
                  <a href="#"><?= image_asset('thumb_song_list3.jpg', '', array('alt'=>'Cold Play', 'class'=>'thumb-list')) ?></a>
                  <h4><a href="#">Cold Play</a></h4>
                  <span>Fix You</span>
                  <div class="button-share">
                      <a href="#" title="Play"><?= image_asset('play_ico.png', '', array('alt'=>'play')) ?></a>
                      <a href="#" title="Download"><?= image_asset('download_ico.png', '', array('alt'=>'download')) ?></a>
                      <a href="#" title="Share"><?= image_asset('share_ico.png', '', array('alt'=>'share')) ?></a>
                      <a href="#" class="last" title="Add"><?= image_asset('add_ico.png', '', array('alt'=>'add')) ?></a>
                   </div>
              </li>
              <li>
                  <a href="#"><?= image_asset('thumb_song_list4.jpg', '', array('alt'=>'Utopia', 'class'=>'thumb-list')) ?></a>
                  <h4><a href="#">Uthopia</a></h4>
                  <span>Hujan</span>
                  <div class="button-share">
                       <a href="#" title="Play"><?= image_asset('play_ico.png', '', array('alt'=>'play')) ?></a>
                       <a href="#" title="Download"><?= image_asset('download_ico.png', '', array('alt'=>'download')) ?></a>
                       <a href="#" title="Share"><?= image_asset('share_ico.png', '', array('alt'=>'share')) ?></a>
                       <a href="#" class="last" title="Add"><?= image_asset('add_ico.png', '', array('alt'=>'add')) ?></a>
                    </div>
              </li>
              <li>
                 <a href="#"><?= image_asset('thumb_song_list5.jpg', '', array('alt'=>'SID', 'class'=>'thumb-list')) ?></a>
                 <h4><a href="#">SID</a></h4>
                 <span>Kuat Kita Bersinar</span>
                 <div class="button-share">
                    <a href="#" title="Play"><?= image_asset('play_ico.png', '', array('alt'=>'play')) ?></a>
                    <a href="#" title="Download"><?= image_asset('download_ico.png', '', array('alt'=>'download')) ?></a>
                    <a href="#" title="Share"><?= image_asset('share_ico.png', '', array('alt'=>'share')) ?></a>
                    <a href="#" class="last" title="Add"><?= image_asset('add_ico.png', '', array('alt'=>'add')) ?></a>
                 </div>
              </li>
          </ul>
      </div>
      <div class="list-left">
          <h3>My Friend List</h3>
          <ul>
				<?php foreach($friends as $row) { ?>
	              <li>						
	                 	<?= anchor("member/profile/".$row->up_uid, image_profile_thumb($row->up_uid.'.jpg', array('alt'=>$row->up_name, 'class'=>'thumb-list'))); ?>
	                  	<h4><?= anchor("member/profile/".$row->up_uid, $row->up_name); ?></h4>
	                  	<span><?= $row->up_city;?></span>
	              </li>
				<?php } ?>
          </ul>
          <span><?= anchor('member/friends', 'View All', array('title'=>'View all', 'class'=>'more'))?></span>
          <div class="clear"></div>
      </div>
      <!--
      <div class="list-left">
          <h3>My Brand List</h3>
          <ul>
              <li>
                 <?= anchor('#', image_asset('thumb_b_list1.jpg', '',array('alt'=>'Linkin Park', 'class'=>'thumb-list'))); ?>
                 <h4><a href="#">Linkin Park</a></h4>
              </li>
              <li>
                 <?= anchor('#', image_asset('thumb_b_list2.jpg', '',array('alt'=>'Mariah Carey', 'class'=>'thumb-list'))); ?>
                 <h4><a href="#">Mariah Carey</a></h4>
              </li>
              <li>
                 <?= anchor('#', image_asset('thumb_b_list3.jpg', '',array('alt'=>'Dave Grohl', 'class'=>'thumb-list'))); ?>
                 <h4><a href="#">Dave Grohl</a></h4>
              </li>
          </ul>
          <span><?= anchor('#', 'View All', array('title'=>'View all', 'class'=>'more')); ?></span>
          <div class="clear"></div>
      </div>
      <div class="list-left">
          <h3>My Brand List</h3>
          <p class="bottom">Likes of</p>
          <p class="bottom">Your List is Empty.<br/><?= anchor('#', 'Find Your Fav Brand Here', array('title'=>'Find Brand')); ?></p>
      </div>
      -->
  </div>

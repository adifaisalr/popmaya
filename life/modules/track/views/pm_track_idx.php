<div class="head-title">
          <span><strong>Song List</strong><?= $jml_member ?></span>
      </div>
      
      <div id="track_list">
          <div class="sort-form">
              <label for="category-music">Sort list by Nama Artis</label>
              <select id="category-music">
                  <option>All</option>
                  <option>Rap</option>
              </select>
          </div>

          <?php 
            foreach ($list_songs as $key => $item) {
          ?>
              <div class="list_box_large">
                <?= image_asset('general/layout/efek_thumb.jpg', '', array('alt'=>'efek', 'class'=>'thumb-box')); ?>
                <h3><a href="<?= site_url().'track/detail/'.$item->s_id ?>"><?= $item->title ?></a></h3>
                <span class="title">Nama Artis</span>
                <div class="button-share">
                  <?= anchor(site_url().'assets/media/sample.mp3', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play', 'class'=>'btn_play')) ?>
                  <!-- tambahan adi 27 dec 2011 - link download music -->
				  <a href=<?=site_url().'member/download_music/sample.mp3'?> title="Download"><img src="assets/images/download_ico.png" alt="download"/></a>
                  <a href="#" title="Share"><img src="assets/images/share_ico.png" alt="share"/></a>
                  <a href="<?= site_url().'track/add_to_playlist/'.$item->s_id ?>" class="last add_to_playlist" title="Add"><img src="assets/images/add_ico.png" alt="add"/></a>
                </div>
                <div class="lbl_total_play">Total play : <?= anchor('#', '200', 'class="ttl_play"'); ?> | Total download : <?= anchor('#', '32'); ?></div>
              </div>
          <?php
            }
          ?>
          
          <div class="clear"></div>
          <?= $pagination ?>
          <!--
          <div class="pagination">
              <a href="#" class="first">Previous</a>
              <strong>1</strong>
              <a href="#">2</a>
              <a href="#">3</a>
              <a href="#">4</a>
              <a href="#">5</a>
              <a href="#">6</a>
              <a href="#">7</a>
              <a href="#">8</a>
              <a href="#" class="last">Next</a>
          </div>
          -->
          <div class="clear"></div>
      </div>
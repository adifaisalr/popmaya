<div class="share">
    <input type="text" name="share" class="input-share" placeholder="Share what's new..." title="Share what's new..."/>
    <div class="share_icons">
       Hello
    </div>    
</div>
<div id="tab">
    <ul>
        <li class="first"><?= anchor(current_url().'#tab1', 'Update'); ?></li>
        <li class="middle"><?= anchor(current_url().'#tab2', 'Music Track'); ?></li>
        <li class="last"><?= anchor(current_url().'#tab3', 'Playlist'); ?></li>
    </ul>
</div>
<div id="load_tab">
    <div id="tab1" class="tab_content">
        <div id="slider-wrapper">
            <div id="slider" class="nivoSlider">
               <?= image_asset('banner_11.jpg', '',array('alt'=>'banner1'))?>
               <?= image_asset('radiohead_02.jpg', '',array('alt'=>'banner1'))?>
            </div>
        </div>
    </div>
    <div id="tab2" class="tab_content">
        <p>Music Track Tab</p>
    </div>
    <div id="tab3" class="tab_content">
        <p>Playlists Tab</p>
    </div>
    <div class="clear"></div>
</div>
<div id="track_list">
    <h2>Recommended Track</h2>
    <div class="list_box">
        <div class="square">
           <?= image_asset('efek_thumb.jpg', '',array('alt'=>'efek', 'class'=>'thumb-box'))?>
            <h3><a href="#">Efek Rumah Kaca</a></h3>
            <span class="title">Dibuang sayang...</span>
            <div class="button-share">
               <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
               <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
            </div>
            <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
        </div>
    </div>
    <div class="list_box">
        <div class="square">
           <?= image_asset('param_thumb.jpg', '',array('alt'=>'paramore', 'class'=>'thumb-box'))?>
            <h3><a href="#">Paramore</a></h3>
            <span class="title">The Only Exception</span>
            <div class="button-share">
               <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
               <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
            </div>
            <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
         </div>
    </div>
    <div class="list_box">
        <div class="square">
           <?= image_asset('saras_thumb.jpg', '',array('alt'=>'sarasvati', 'class'=>'thumb-box'))?>
            <h3><a href="#">Sarasvati</a></h3>
            <span class="title">Cut and Paste</span>
            <div class="button-share">
               <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
               <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
            </div>
            <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
         </div>   
    </div>
    <div class="list_box">
        <div class="square">
            <?= image_asset('cd_thumb.jpg', '',array('alt'=>'coldplay', 'class'=>'thumb-box'))?>
            <h3><a href="#">Coldplay</a></h3>
            <span class="title">In My Place</span>
            <div class="button-share">
               <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
               <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
            </div>
            <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
        </div>
    </div>
    <div class="list_box">
        <div class="square">
            <?= image_asset('viera_thumb.jpg', '',array('alt'=>'vierra', 'class'=>'thumb-box'))?>
            <h3><a href="#">Vierra</a></h3>
            <span class="title">Aku Takut</span>
            <div class="button-share">
               <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
               <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
            </div>
            <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total pay 23</span>
        </div>
    </div>
    <div class="list_box">
        <div class="square">
           <?= image_asset('snsd_thumb.jpg', '',array('alt'=>'snsd', 'class'=>'thumb-box'))?>
            <h3><a href="#">SNSD</a></h3>
            <span class="title">Miss U</span>
            <div class="button-share">
               <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
               <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
            </div>
            <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
        </div>
    </div>
    <div class="list_box">
        <div class="square">
           <?= image_asset('vir_thumb.jpg', '',array('alt'=>'virgin', 'class'=>'thumb-box'))?>
            <h3><a href="#">The Virgin</a></h3>
            <span class="title">Cinta Terlarang</span>
            <div class="button-share">
               <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
               <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
            </div>
            <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
        </div>
    </div>
    <div class="list_box">
        <div class="square">
            <?= image_asset('adam_thumb.jpg', '',array('alt'=>'ada', 'class'=>'thumb-box')); ?>
            <h3><a href="#">Ada Band</a></h3>
            <span class="title">Masih</span>
            <div class="button-share">
               <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
               <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
            </div>
            <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
        </div>
    </div>
    <div class="list_box">
        <div class="square">
           <?= image_asset('ut_thumb.jpg', '',array('alt'=>'utopia', 'class'=>'thumb-box'))?>
            <h3><a href="#">Utopia</a></h3>
            <span class="title">Hujan</span>
            <div class="button-share">
               <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
               <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
            </div>
            <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
        </div>
    </div>
    <div class="list_box">
        <div class="square">
           <?= image_asset('sid_thumb.jpg', '',array('alt'=>'sid', 'class'=>'thumb-box'))?>
            <h3><a href="#">SID</a></h3>
            <span class="title">Kuat Kita Bersinar</span>
            <div class="button-share">
               <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
               <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
            </div>
            <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
        </div>
    </div>
    <div class="clear"></div>
</div>
<h2 class="track-head">New Track</h2>
<div class="new_track">          
    <div class="list_box">
        <div class="square">
           <?= image_asset('viera_thumb.jpg', '',array('alt'=>'vierra', 'class'=>'thumb-box'))?>
            <h3><a href="#">Vierra</a></h3>
            <span class="title">Aku Takut</span>
            <div class="button-share">
               <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
               <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
            </div>
            <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
        </div>
    </div>
    
    <div class="list_box">
         <div class="square">
            <?= image_asset('snsd_thumb.jpg', '',array('alt'=>'snsd', 'class'=>'thumb-box'))?>
             <h3><a href="#">SNSD</a></h3>
             <span class="title">Miss U</span>
             <div class="button-share">
                <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
                <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
             </div>
             <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
         </div>
     </div>
     <div class="list_box">
          <div class="square">
             <?= image_asset('vir_thumb.jpg', '',array('alt'=>'virgin', 'class'=>'thumb-box')); ?>
              <h3><a href="#">The Virgin</a></h3>
              <span class="title">Cinta Terlarang</span>
              <div class="button-share">
                 <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
                 <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
              </div>
              <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
          </div>
      </div>            
      <div class="list_box">
           <div class="square">
              <?= image_asset('adam_thumb.jpg', '',array('alt'=>'ada', 'class'=>'thumb-box')); ?>
               <h3><a href="#">Ada Band</a></h3>
               <span class="title">Masih</span>
               <div class="button-share">
                  <?= anchor('#', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play')) ?>
                  <?= anchor('#', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
               </div>
               <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
           </div>
       </div>
    <div class="clear"></div>
</div>

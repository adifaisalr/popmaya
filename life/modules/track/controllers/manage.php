<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends MX_Controller 
{
	var $user_id;
	private $limit = 20;

   	function __construct()
	{
   		parent::__construct();

   		$this->load->library('form_validation');
   		$this->load->model('m_track', '', TRUE);

		$this->user_id=$this->session->userdata('user_id');

   		$this->output->enable_profiler(TRUE);

        $this->template->set_master_template('admin');
        $this->template->add_region('ttl');
        $this->template->add_region('content');

        if($this->session->userdata('group')!='admin'){
            redirect('member/profile');
        }
   	}

   	function index()
	{	
		if (!$this->ion_auth->logged_in()) //not logged in
		{
			//redirect to the login page
			redirect('home', 'refresh');
		}
		else //logged in
		{
			//redirect to the profile (my profile) page
			redirect("track/manage/list_song", 'refresh');
		}
   	}

	function list_song(){
     //render view
        $this->template->write('head_title', 'Track management');
        $data['foot_js']=$data['warn_js']='';

        /* pagination */ 
        $uri_segment = 4;
        if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($uri_segment); }
        $data['start_no']= $offset+1;

        $this->load->library('pagination');
        $config['base_url'] = site_url().'track/manage/list_song';
        $config['total_rows'] = $this->m_track->count_all('');
        $config['per_page'] = $this->limit;
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        /* pagination.end */

        $data['list_songs']=$this->m_track->get_paged_list($this->limit, $offset, '');
        $this->template->write('ttl', 'Track Management');
        $this->template->write_view('content', 'pm_cms_track', $data, '');
        $this->template->render();       
    }

    function edit(){
     //render view
        if($this->uri->segment(4)){
            $aid=$this->uri->segment(4);
        }else{
            redirect('track/manage');
        }
        $this->template->write('head_title', 'Track Edit');
        $data['foot_js']=$data['warn_js']='';
        $data['item']=$this->m_track->item_info($aid);
        $this->template->write('ttl', 'Track Edit');
        $this->template->write_view('content', 'pm_cms_track_edit', $data, '');
        $this->template->render();       
    }

    function delete(){
        if($this->uri->segment(3)){
            $err=1;
            $err_msg='';
            $aid=$this->uri->segment(4);
            $row=$this->m_track->item_info($aid);
            if($row){
                $arr=explode(' ', $row->created_on);
                $arr2=explode('-', $arr[0]);
                $yr=$arr2[0];
                $mon=$arr2[1];
                $day=$arr2[2];
                $dir_path='./assets/media/';
                $dir_path=$dir_path.$yr.'/'.$mon.'/'.$day.'/';
                if(file_exists($dir_path.'sample_'.$aid.'.mp3')){ unlink($dir_path.'sample_'.$aid.'.mp3'); $err=0; }
                if(file_exists($dir_path.$aid.'.mp3')){ unlink($dir_path.$aid.'.mp3'); $err=0; }
                if($err=0){ $err_msg='File deleted.'; }
                if($this->m_track->remove_item($aid)){ set_flash('warn', 'notice', 'Data deleted.'.$err_msg); }else{
                    set_flash('warn', 'notice', 'Data not deleted.'.$err_msg);
                }
            }

            redirect($_SERVER['HTTP_REFERER']);

        }else{
            redirect('track/manage');
        }
    }

    function upload(){
        $err_ev='';
        $err_type='success';
        $arr2 = array('genreID'=>$this->input->post('item_genre'), 'time_length'=>$this->input->post('item_length'), 'title'=>$this->input->post('item_title'), 'post'=>$this->input->post('item_info'), 'memberID'=>$this->session->userdata('user_id'));
        
        if($this->input->post('item_id')){
            $item_id=$this->input->post('item_id');
            $arr1 = array('s_id'=>$item_id);
            $this->m_track->save('edit', $arr1, $arr2);
            $err_ev='Data updated. ';
            $row=$this->m_track->item_info($item_id);
            $arr=explode(' ', $row->created_on);
            $arr2=explode('-', $arr[0]);
            $yr=$arr2[0];
            $mon=$arr2[1];
            $day=$arr2[2];
        }else{
            $item_id=$this->m_track->save('add', '', $arr2);
            $err_ev='Data saved. ';
            $yr=date('Y');
            $mon=date('m');
            $day=date('d');
        }

        $mon=sprintf("%02s",$mon);
        $day=sprintf("%02s",$day);

        $dir_path='./assets/media/';
        
        if(!is_dir($dir_path.'/'.$yr.'/')){ mkdir($dir_path.'/'.$yr); }
        if(!is_dir($dir_path.'/'.$yr.'/'.$mon.'/')){ mkdir($dir_path.$yr.'/'.$mon.'/'); }
        if(!is_dir($dir_path.'/'.$yr.'/'.$mon.'/'.$day.'/')){ mkdir($dir_path.$yr.'/'.$mon.'/'.$day.'/'); }

        $dir_path=$dir_path.$yr.'/'.$mon.'/'.$day.'/';
        $config['upload_path'] = $dir_path;
        $config['overwrite'] = TRUE;
        $config['file_name'] = $item_id;
        $config['allowed_types'] = 'mp3';
        $config['max_size'] = '7168';

        $this->load->library('upload', $config);
        if(!$this->upload->do_upload()){
            $err2=$this->upload->display_errors();
        }else{
            $err2=' File uploaded.';
            $info = $this->upload->data();
            $this->m_track->save('edit', array('s_id'=>$item_id), array('file_size'=> $info['file_size']));
            $input_file = $info['full_path'];
            $output_file = $info['file_path'] .'sample_'. $info['orig_name'];
            exec("ffmpeg -ss 0 -t 30 -i $input_file $output_file");
        }

        set_flash('warn', $err_type, $err_ev.' '.$err2);
        
        redirect($this->input->post('prev_page'));

    }

}
?>
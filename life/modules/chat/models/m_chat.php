<?php
class M_Chat extends CI_Model 
{
	//------------------------------------------------------------------------------------------------------------------------rooms
	/* Nisa. 21 Des 2011. Mengambi list chat room */		
	function get_rooms()
	{
		$query=$this->db->select('tr_chat_room.*, tr_member_artist.artist_name')
			->from('tr_chat_room')
			->join('tr_member_artist', 'tr_member_artist.ID=tr_chat_room.cr_created_by')
			->where('cr_status', '1')
			->order_by('cr_max_num_ppl', 'desc')
			->get();
		return $query->result();
	}
	
	/* Nisa. 21 Des 2011. Mengambi info dari suatu chat room */
	function get_room($id)
	{
		$query=$this->db->select('tr_chat_room.*, tr_member_artist.artist_name')
			->from('tr_chat_room')
			->join('tr_member_artist', 'tr_member_artist.ID=tr_chat_room.cr_created_by')
			->where('cr_id', $id)
			->get();
		return $query->row();
	}
	
	/* Nisa. 21 Des 2011. Buat room */
	function create_room($artist_id, $room_name)
	{
		//insert tr_chat
		$data = array(
		   	'cr_title' => $room_name,
		   	'cr_created_by' => $artist_id,
		   	'cr_max_num_ppl' => '0',
			'cr_status' => '1'
			);
		$this->db->insert('tr_chat_room', $data); 		
	}
	
	//------------------------------------------------------------------------------------------------------------------------chats
	/* Nisa. 21 Des 2011. Mengambi list chat dari suatu room */
	function get_room_chats($id)
	{
		$query=$this->db->select('tr_chat.*, tr_user_profile.up_name')
			->from('tr_chat')
			->join('tr_user_profile', 'tr_chat.c_sender_id=tr_user_profile.up_uid')
			->where('c_room_id', $id)
			->where('c_status', '1')
			->order_by('c_created_on', 'desc')
			->get();
		return $query->result();	
	}	
	
	/* Nisa. 21 Des 2011. Posing chat */
	function insert_chat($room_id, $user_id, $chat)
	{
		//insert tr_chat
		$data = array(
		   	'c_room_id' => $room_id,
		   	'c_sender_id' => $user_id,
		   	'c_mssg' => $chat,
			'c_status' => '1'
			);
		$this->db->insert('tr_chat', $data); 		
		
		//count people
		$query=$this->db->select('count(distinct c_sender_id) as people')
			->from('tr_chat')
			->where('c_room_id', $room_id)
			->get();
		$count_people=$query->row()->people;
		
		//update tr_chat_room
		$data = array(
	        'cr_max_num_ppl' => $count_people
		     );
		$this->db->where('cr_id', $room_id);
		$this->db->update('tr_chat_room', $data);
	}
	
	function get_last_updated_room(){ // ^zy
		$this->db->select('cr_updated_on');
		$this->db->order_by('cr_updated_on', 'desc');
		$que_glur = $this->db->get('tr_chat_room');
		return $que_glur->row()->cr_updated_on;
	}

	function get_all_rooms(){ // ^zy
		$this->db->select('*');
		$que_gar = $this->db->get('tr_chat_room');
		return $que_gar->result();
	}
	function get_messages($id) // ^zy
	{
		$query=$this->db->select('tr_chat.*, tr_user_profile.up_name')
		->from('tr_chat')
		->join('tr_user_profile', 'tr_chat.c_sender_id=tr_user_profile.up_uid')
		->where('c_room_id', $id)
		->where('c_status', '1')
		->order_by('c_created_on', 'desc')
		->limit(35, 0)
		->get();
		return $query->result();	
	}
	function get_username($uid){ // ^zy
		$this->db->select('up_name');
		$quegu=$this->db->get_where('tr_user_profile', array('up_uid'=>$uid));
		echo $quegu->row()->up_name;
	}
	function update_room($rid, $people){ // ^zy
		$this->db->where(array('cr_id'=>$rid));
		$arr=array('cr_ppls'=>$people);
		return $this->db->update('tr_chat_room', $arr);
	}
	function get_chatters($rid){ // ^zy
		$qur = $this->db->get_where('tr_chat_room', array('cr_id'=>$rid))->row();
		return $qur->cr_ppls;
	}	
}
?>
Chat Room List
<br />
<br />

<?php if($this->session->flashdata('message')) {?>
	<?= $this->session->flashdata('message'); ?>
<?php } ?>

<?php if($have_artist) { ?>
	<?= form_open('chat/room_add_process'); ?>
		Buat Chat Room<br />
		<?= form_label('Nama Artis', 'artist'); ?>
		<?= form_dropdown('artist', $my_artist); ?>
		<br />
		<?= form_label('Nama Chat Room', 'room'); ?>
		<?= form_input('room', ''); ?><br />
		<?= form_submit('submit', "Kirim"); ?>
	<?= form_close(); ?>

	<br />
	<br />
<?php } ?>

<ul>
	<?php foreach($rooms as $r) { ?>
		<li>
			<?= anchor('chat/room/'.$r->cr_id, $r->cr_title); ?>
			<br />
			Created by: <?= $r->artist_name; ?>
			<br />
			People in this room: <?= $r->cr_max_num_ppl; ?>
			<br />
			Created on: <?= $r->cr_created_on; ?>
		</li>
	<?php } ?>
</ul>
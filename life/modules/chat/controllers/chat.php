<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Chat extends MX_Controller 
{
	/* Nisa. 20 Des 2011. Atribut user id, diisi di konstruktor dengan session */	
	var $user_id;
	
	//------------------------------------------------------------------------------------------------------------------------constructor
	/* Nisa. 20 Des 2011. Konstruktor kelas Chat controller */	
   	function __construct()
	{
   		parent::__construct();

		$this->user_id=$this->session->userdata('user_id');

   		$this->load->model('m_chat', '', TRUE);
		$this->load->model('member/m_member', '', TRUE);
		
		//if not logged in, redirect them to the login page
		if (!$this->ion_auth->logged_in())
		{
			redirect('home', 'refresh');
		}		
		
   		//$this->output->enable_profiler(TRUE);
   	}

	//------------------------------------------------------------------------------------------------------------------------index
	/* Nisa. 20 Des 2011. Method index, menampilkan list dari chat room */		
	function index()
	{
		//top content
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
		$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
		$data['user_id']=$this->user_id;
		$data['name']=$this->m_member->get_user_name($this->user_id);
		
		//middle content
		$data['rooms']=$this->m_chat->get_rooms();
		
		$this->load->model('artist/m_artist', '', TRUE);		
		$my_artist=$this->m_artist->get_user_artist($this->user_id);
		if(!$my_artist)
		{
			$data['have_artist']=false;
		}
		else
		{
			$data['have_artist']=true;
			foreach($my_artist as $my)
			{
				$data['my_artist'][$my->ID]=$my->artist_name;
			}
		}
		//end middle content
		
		//right content
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
				
		//render view
		$this->template->write('head_title', 'Chat');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_side_chat', $data, '');
		$this->template->write_view('middle_content', 'pm_chat_idx', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();	      					
	}
	
	//------------------------------------------------------------------------------------------------------------------------room
	/* Nisa. 20 Des 2011. Method untuk menampilkan isi chat dari suatu chat room */		
	function room($id)
	{
		//top content
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
		$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
		$data['user_id']=$this->user_id;
		$data['name']=$this->m_member->get_user_name($this->user_id);
		
		//middle content
		$data['room']=$this->m_chat->get_room($id);
		$data['chats']=$this->m_chat->get_room_chats($id);
		
		//right content
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);

		//render view
		$this->template->write('head_title', 'Chat');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_side_chat', $data, '');
		$this->template->write_view('middle_content', 'pm_chat_room', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();	      					
	}
	
	/* Nisa. 20 Des 2011. Method untuk proses menambahkan suatu chat room */			
	function room_add_process()
	{
		$this->load->library('form_validation');
		$this->load->helper('form');
   		
		//validation rules
		$this->form_validation->set_rules('room', 'Nama Chat Room', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('artist', 'Nama Artis', 'required');
		
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
		
		if ($this->form_validation->run() == true)
		{
			$this->m_chat->create_room($this->input->post('artist')
				, $this->input->post('room')
				);
			
			redirect('chat');
		}
		else
		{
			$this->session->set_flashdata('message', "Buat chat room gagal");
			redirect('chat');														
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------------chat
	/* Nisa. 21 Des 2011. Method untuk proses menambahkan chat */
	function chat_add_process($id)
	{
		$this->load->library('form_validation');
		$this->load->helper('form');
   		
		//validation rules
		$this->form_validation->set_rules('chat', 'Chat', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean');
		
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
		
		if ($this->form_validation->run() == true)
		{
			$this->m_chat->insert_chat($id
				, $this->user_id
				, $this->input->post('chat')
				);
			
			redirect('chat/room/'.$id);
		}
		else
		{
			$this->session->set_flashdata('message', "Kirim chat gagal");
			redirect('chat/room/'.$id);														
		}
	}
}
?>
<?php
class M_Artist extends CI_Model 
{
	function getUserProfile($id)
	{
		$query=$this->db->select('tr_user_profile.*, tr_user_login.ul_email')
				->from('tr_user_profile')
				->join('tr_user_login', 'ul_id=up_uid')
				->where('up_uid', $id)
				->get();
		return $query->row();
	}
	
	/* Nisa. 21 Des 2011. Mengambil list artist yang dikelola oleh seorang member */
	function get_user_artist($user_id)
	{
		$query=$this->db->select('tr_member_artist.ID, tr_member_artist.artist_name')
			->from('tr_member_artist')
			->where('tr_member_artist.memberID', $user_id)
			->get();
		return $query->result();
	}
	
	
	//Author		: Firman
	//Date			: 2011 Dec 18
	//Desc			:get image ads
	function get_image_ads()
	{
		$query = $this->db->get_where('tr_ads', array('position' => 'ads'));
			$this->db->order_by('RAND()','asc');
			$this->db->limit(1);
		return $query->result();
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 19
	//Desc			: get artist data
	function getArtistProfile($id)
	{
		$query=$this->db->select('tr_member_artist.*')
				->from('tr_member_artist')
				->where('ID', $id)
				->get();
		return $query->row();
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 19
	//Desc			: get genre name
	function get_genre_name($id)
	{
		$query=$this->db->get_where('tr_genre', array('g_id'=>$id));
		$row=$query->row();
		if($row) return $row->g_name;
		else return "";
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 20
	//Desc			: save data artist
	function save_artist($data){
		$data_insert = array(
				'artist_name' => $data['a_name'],
				'title' => '',
				'memberID' => $data['user_id'],
				'pageID' => 0,
				'type' => '',
				'genre' => $data['a_category'],
				'status' => 0,
				'post' => 0,
				'ref' => ''
		);
		$this->db->insert('tr_member_artist',$data_insert);
		return $this->db->insert_id();
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 21
	//Desc			: save album song data
	function save_album_song($album){
		$this->db->insert('tr_album_song', $album);
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 22
	//Desc			: get album song
	function get_album_song($id){
		$query = $this->db->select('*')
			->from("tr_album_song")
			->where("memberID",$id)
			->get();
		return $query->result();
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 22
	//Desc			: get artist id from album song
	function get_artist_id($album_id){
		$query=$this->db->get_where('tr_album_song', array('as_id'=>$album_id));
		$row = $query->row();
		if($row) return $row->memberID;
		else return '';
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 22
	//Desc			: get album data
	function get_count_album_song($album_id)
	{
		$count=$this->db->select('*')
			->from('tr_song')
			->where('albumID', $album_id)
			->count_all_results();
		
		return $count;
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 22
	//Desc			: get album data
	function getAlbum($album_id)
	{
		$query=$this->db->select('tr_album_song.*')
				->from('tr_album_song')
				->where('as_id', $album_id)
				->get();
		return $query->row();
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 23
	//Desc			: get album data
	function updateAlbum($album_id, $data)
	{
		$data_insert = array(
			'title' => $data['album_name'],
			'image' => $data['cover_album']
		);
		//$this->db->where('as_id', $album_id);
		$this->db->update('tr_album_song', $data_insert, array('as_id'=>$album_id)); 
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 23
	//Desc			: get song by album
	function get_song_by_album($album_id)
	{
		$query = $this->db->select('*')
			->from("tr_album_song")
			->where("albumID",$album_id)
			->get();
		return $query->result();
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 23
	//Desc			: get song by album
	function update_song($s_id,$data)
	{
		$data_insert = array(
			'title' => $data['title'],
			'isprevonly' => $data['isprevonly'],
			'isdownload' => $data['isdownload'],
			'price' => $data['price']
		);
		$this->db->where('s_id', $s_id);
		$this->db->update('tr_song', $data_insert); 
	}
	//Author		: Adi 
	//Date			: 2011 Dec 26
	//Desc			: get song by artist
	function get_song_by_artist($artist_id,$limit)
	{
		$query = $this->db->select('*')
			->from("tr_song")
			->where("artist_id",$artist_id)
			->limit($limit)
			->get();
		return $query->result();
	}
	
	//Author		: Firman 
	//Date			: 2011 Dec 27
	//Desc			: get artist list from database
	function get_artist_list($page){
			$query = $this->db->select('*')
			->from("tr_member_artist")
			->join('tr_member_fans','tr_member_artist.ID = tr_member_fans.artist_id','left')
			->limit(10,$page*10)
			->get();
			
			
		return $query->result();
	}	
	
	//Author		: Firman
	//Date			: 2011 Dec 18
	//Desc			: get number of artist
	function get_num_artist(){
		$query=$this->db->get('tr_member_artist');
		return $query->num_rows();
	}
	
	function become_fans($data){
		$this->db->insert('tr_member_fans', $data);
	}
	
	function unfans($artist_id,$member_id){
		$this->db->where('artist_id', $artist_id);
		$this->db->where('member_id', $member_id);
		
		$this->db->delete('tr_member_fans'); 
	}
	
	//Author 		: Adi
	//Date			: 2011 Dec 29
	//Desc			: cek artist buatan member
	function artist_member($uid,$a_id){
		$query=$this->db->get_where('tr_member_artist', array('ID'=>$a_id, 'memberID'=>$uid));
		$row = $query->row();
		if($row) return true;
		else return false;
	}
}
?>
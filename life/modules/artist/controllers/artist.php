<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Artist extends MX_Controller 
{
	var $uid;
	
   	function __construct()
	{
   		parent::__construct();
		
		//tambahan Adi - 3 Jan 2012
		$this->load->helper(array('form', 'url'));
        $this->load->library('image_moo') ;

   		$this->load->library('form_validation');
   		$this->load->model('m_artist', '', TRUE);
   		$this->load->model('member/m_member', '', TRUE);
		$this->load->model('home/m_home', '', TRUE);

		$this->uid=$this->session->userdata('user_id');

   		//$this->output->enable_profiler(TRUE);
   	}

	//------------------------------------------------------------------------------------------------------------------------frontpage
   	function index($page="")
	{	
		
		$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js');
	    $data['foot_script']='
			var sics = $(".share_icons"); sics.hide();
		    $(\'#slider\').nivoSlider({speed:5000});

		    $("#tab ul li.middle a").addClass("active").show(); 
 	        $("#tab2").show();
		    ';	      
			
			$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			
			if($this->uid){
				$row=$this->m_member->get_user_profile($this->uid);
				
				$row2=$this->m_artist->getArtistProfile(1);
				$data['up_uid']=$row->up_uid; // ^zy 4 jan
				$data['user_id']=$this->uid;
				$data['name']=$row->up_name;
				$data['alias']=$row->up_alias;	
				$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
				
				$data['gender_code']=$row->up_gender;
				$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
				$data['city']=$row->up_city;
				$data['country']=$row->up_country;
				$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
				//load ads data
				$data['ads_data']=$this->m_artist->get_image_ads();
				
				$data2['artist_name']=$row2->artist_name;
			  $data2['title']=$row2->title;
			  $data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			  $data2['artist_id']=1;
			  
			  
			  /****================================ tambahan ===================================****/ 
				//Adi 26 Dec 2011
/*				$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
				$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
				$data['count_notification']=$this->m_member->get_count_notification($this->uid);
*/				
				//music sidebar, suggest song drop down menu		
				$fav_genre=$this->m_member->get_fav_genre($this->uid);
				$fav_genre=explode(", ", $fav_genre);
				$genres=$this->m_member->get_genre();
				$i=0;
				$fav_genre_display='';

				if($fav_genre[0]) //have favourite genre
				{
						foreach($genres as $g)
						{
							if($g->g_id==$fav_genre[$i])
							{
								$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
								$i++;
							}
						}	
					}
					else //do not have favourite genre
					{
						foreach($genres as $g)
						{
							$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
							$i++;
						}				
					}	
					$data['suggest_song_option']=$fav_genre_display;

					$genre_id=$fav_genre_display[0]['id'];
					$limit=$this->config->item('limit_sidebar_suggest_song_list');		
					$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);
					
					$data['recommended_track']=$this->m_home->get_recommended_track();
					$new_track_limit=$this->config->item('limit_new_track');
					$data['new_track']=$this->m_home->get_new_track($new_track_limit);
				}
				
			//Firman
				
				//Autthor	: Firman
			//Date		: 2011 Dec 15
			//Desc		: Load fans list and fans page amount	
			$data['artist_list'] = $this->m_artist->get_artist_list($page);
			/* pagination */ 
			$uri_segment = 3;
			if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($page/10); }
			$data['start_no']= $offset+1;

			$this->load->library('pagination');
			$config['base_url'] = site_url().'artist/index/';
			$data['jml_member']=$config['total_rows'] = $this->m_artist->get_num_artist();
			$config['per_page'] = 10;
			$config['uri_segment'] = $uri_segment;
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			/* pagination.end */
			
				//load ads data
				$data['ads_data']=$this->m_artist->get_image_ads();
				
				/*================================end tambahan ========================================*/
						

			//render view
			$this->template->write('head_title', 'Artist');
			$this->template->write('keywords', '');
	      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		  	$this->template->write_view('sidebar_left', 'pm_side_artist', $data2, '');
			$this->template->write_view('middle_content', 'pm_artist_idx', $data, '');
			$this->template->write_view('sidebar_right', 'pm_artist_side_right', $data, '');
			$this->template->render();			      			

   	}

	function frontpage()
	{
		$data="";
		$data['foot_js']='';
		//render view
		$this->template->write('keyword', ' ?? ');
		$this->template->write('head_title', 'Home');
		$this->template->write_view('middle_content', 'pm_member_frontpage', $data, TRUE);
		$this->template->render();
	}
	//------------------------------------------------------------------------------------------------------------------------end frontpage	
	
	//------------------------------------------------------------------------------------------------------------------------login
   	function login()
	{
		if ($this->ion_auth->logged_in()) //already logged in so no need to access this page
		{			
			redirect("member/profile", 'refresh');
		}
		
		//validation rules
        $this->form_validation->set_rules('email', 'Email', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|required|valid_email|min_length[5]|max_length[50]|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|alpha_numeric|prep_for_form|htmlspecialchars|encode_php_tags|required|min_length[5]|max_length[20]|xss_clean');
		
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
    	$this->form_validation->set_message('alpha_numeric', 'Only alphanumeric characters are allowed.');
		$this->form_validation->set_message('valid_email', 'Wrong email format.');
    	$this->form_validation->set_message('min_length[5]', 'Min. character is 5 chars.');    	
    	$this->form_validation->set_message('max_length[20]', 'Max. character is 20 chars.');
    	$this->form_validation->set_message('max_length[50]', 'Max. character is 50 chars.');
        
		//error delimiter
		$this->form_validation->set_error_delimiters('', '');
		
		if($this->form_validation->run()==TRUE) //validation success
		{
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $remember))
			{ 	
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("member/profile", 'refresh');
			}
			else
			{ 
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('member/frontpage', 'refresh'); 
			}			
		}
		else //validation failed
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one			
			$this->session->set_flashdata('message', validation_errors());
			redirect('member/frontpage');
		}
	}
	
	function login_by_fb()
	{
		$fb_data = $this->session->userdata('fb_data');
		
		if((!$fb_data['uid']) or (!$fb_data['me']))
		{
			redirect('member/frontpage');
		}
		else
		{
			$data['fb_data']=$fb_data;
			
			$this->load->view('pm_member_login_facebook', $data);
		}
	}
	//------------------------------------------------------------------------------------------------------------------------end login

	//------------------------------------------------------------------------------------------------------------------------logout
	function logout()
	{
		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them back to the page they came from
		$this->session->set_flashdata('message', "Logout success");
		redirect('member/frontpage', 'refresh');
	}
	//------------------------------------------------------------------------------------------------------------------------end logout
	
	//------------------------------------------------------------------------------------------------------------------------forgot password
	function forgot()
	{
		$data="";
		$data['foot_js']='';
		//render view
		$this->template->write('keyword', ' ?? ');
		$this->template->write('head_title', 'Forgot Password');
		$this->template->write_view('middle_content', 'pm_member_forgot', $data, TRUE);
		$this->template->render();		
	}
	
	function forgot_process()
	{
		$this->form_validation->set_rules('email', 'Email', 'xss_clean|required|valid_email|min_length[5]|max_length[50]');
		
		if ($this->form_validation->run() == true)
		{
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));					
			
			if ($forgotten)  //if there were no errors
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('member/forgot_confirm', 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('member/forgot', 'refresh');
			}			
		}
		else
		{
			//no email entered
			$this->session->set_flashdata('message', validation_errors());
			redirect('member/forgot');			
		}
	}
	
	function forgot_confirm()
	{
		echo "forgot confirm";
		
		$data="";
						
		//render view
		$this->template->write('keyword', ' ?? ');
		$this->template->write('head_title', 'Forgot Password Confirm');
		$this->template->write_view('middle_content', 'pm_member_forgot_confirm', $data, TRUE);
		$this->template->render();				
	}
	
	function reset_password($code='')
	{
		if($code)
		{
			$reset = $this->ion_auth->forgotten_password_complete($code);

			if ($reset)
			{  
				//if the reset worked then send them to the login page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('member/frontpage', 'refresh');
			}
			else
			{ 
				//if the reset didnt work then send them back to the forgot password page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('member/forgot', 'refresh');
			}
		}
		else
		{
			redirect('member/frontpage', 'refresh');
		}		
	}
	//------------------------------------------------------------------------------------------------------------------------end forgot password
   	
	//------------------------------------------------------------------------------------------------------------------------register
	function register()
	{
		//validation rules
		$this->form_validation->set_rules('fullname', 'Fullname', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean|min_length[5]|max_length[20]');
    	$this->form_validation->set_rules('email', 'Email', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|valid_email|callback_new_email|min_length[5]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|alpha_numeric|required|prep_for_form|htmlspecialchars|encode_php_tags|min_length[5]|max_length[20]|xss_clean');
    	
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
    	$this->form_validation->set_message('alpha_numeric', 'Only alphanumeric characters are allowed.');
		$this->form_validation->set_message('valid_email', 'Wrong email format.');
    	$this->form_validation->set_message('min_length[5]', 'Min. character is 5 chars.');    	
    	$this->form_validation->set_message('max_length[20]', 'Max. character is 20 chars.');
    	$this->form_validation->set_message('max_length[50]', 'Max. character is 50 chars.');
		
		$this->form_validation->set_error_delimiters('','');

		
		if ($this->form_validation->run() == true)
		{
			if($this->input->post('agree')=="agreed")
			{			
				$fullname = $this->input->post('fullname');
				$email = $this->input->post('email');
				$password = $this->input->post('password');

				$additional_data = array('gender' => $this->input->post('gender'),
					'update' => $this->input->post('update')
				);
			
			
				if($this->ion_auth->register($fullname, $password, $email, $additional_data))
				{
					//check to see if we are creating the user
					$this->session->set_flashdata('message', "User Created");
					redirect('member/frontpage', 'refresh');
				}
				else
				{ 
					//display the register user form
					$this->session->set_flashdata('message', $this->ion_auth->errors());
					redirect('member/frontpage', 'refresh'); 
				}
			}
			else
			{
				$this->session->set_flashdata('message', "You must agree with the Term & Service");
				redirect('member/frontpage', 'refresh'); 				
			}
		}
		else
		{
			$this->session->set_flashdata('message', validation_errors());
			redirect('member/frontpage');			
		}
	}
	//------------------------------------------------------------------------------------------------------------------------end register

	//nama : Adi
	//tanggal : 20 Des 2011
	//deskripsi : profile artist page
	//------------------------------------------------------------------------------------------------------------------------profile
	function profile($id='')
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('member/frontpage', 'refresh');
		}
		else
		{
		$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			
			
			$data['a_name'] = $this->input->post('name');
			$data['a_category'] = $this->input->post('category');
			$data['user_id']=$this->uid;
			if(!$this->input->post('name')){
				if($id==''){
					//echo 'a';
					$row2=$this->m_artist->getArtistProfile(1);
					$data['artist_id']=1;
					$data2['artist_id']=1;
				}else{
					$row2=$this->m_artist->getArtistProfile($id);
					//echo 'b';
					//echo '--'.$id;
					$data['artist_id']=$id;
					$data2['artist_id']=$id;
				}
			}else{
				$row2=$this->m_artist->getArtistProfile($this->m_artist->save_artist($data));
			}
			
			if($this->input->post('email')){
				$msg = '<html><body><h1><a href="ideafieldproject.com/popmaya">Popmaya</a></h1></body></html>';
				$subject = "join popmaya";
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: popmaya@example.com' . "\r\n";
				mail($this->input->post('email'), $subject,$msg,$headers);
			}else{
			}
			
			//setting up viewer
			$page_req="pm_artist";
			$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js');
	      	$data['foot_script']='
		        var sics = $(".share_icons"); sics.hide();
		        $(\'#slider\').nivoSlider({speed:5000});
		        
            $("#tab ul li.middle a").addClass("active").show(); 
            $("#tab2").show();  

		      ';	      

			//if($id=='') //my profile
			//{
				$row=$this->m_artist->getUserProfile($this->uid);
			//}
			//else //other member profile
			//{
				//$row=$this->m_artist->getUserProfile($id);
			//}
			//if($id==''){
			//	$row2=$this->m_artist->getArtistProfile(1);
			//}
			//else{
			//	$row2=$this->m_artist->getArtistProfile($this->m_artist->save_artist($data));
			//}
			
						
			//setting data
			$data2['artist_name']=$row2->artist_name;
			$data2['title']=$row2->title;
			$data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			
			$data['banner_image'] = $this->m_member->get_image_banner_profile();
			$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		

			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
			//load ads data
			$data['ads_data']=$this->m_artist->get_image_ads();
			
			/****================================ tambahan ===================================****/ 
			//Adi 26 Dec 2011
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
			$data['count_notification']=$this->m_member->get_count_notification($this->uid);
			
			$data2['song_list']=$this->m_artist->get_song_by_artist($id,5);
			
			//music sidebar, suggest song drop down menu		
			$fav_genre=$this->m_member->get_fav_genre($this->uid);
			$fav_genre=explode(", ", $fav_genre);
			$genres=$this->m_member->get_genre();
			$i=0;
			$fav_genre_display='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}				
			}	
			$data['suggest_song_option']=$fav_genre_display;

			$genre_id=$fav_genre_display[0]['id'];
			$limit=$this->config->item('limit_sidebar_suggest_song_list');		
			$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);
			/*================================end tambahan ========================================*/
			
			//render view
			$this->template->write('head_title', 'Profile');
			$this->template->write('keywords', '');
			$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		    $this->template->write_view('sidebar_left', 'pm_side_artist_profile', $data2, '');
			if($this->m_artist->artist_member($this->uid,$id)){
				$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $id, '');
			}else{
				$this->template->write_view('sidebar_right', 'pm_artist_side_right', $data, '');
			}
			$this->template->write_view('middle_content', 'pm_artist', $data, '');
			$this->template->render();
	      			
		}
	} 
	//------------------------------------------------------------------------------------------------------------------------end profile
	
	function become_fan($artist_id=''){
		$data = array(
				'artist_id' => $artist_id,
				'member_id' => $this->uid,
				
		);
		$this->m_artist->become_fans($data);
		redirect("artist/","refresh");
	}
	
	function stop_become_fan($artist_id=''){
		$this->m_artist->unfans($artist_id,$this->uid);
		redirect("artist/","refresh");
	}

	function create_artist(){
		$data['a_name'] = $this->input->post('name');
		$data['a_category'] = $this->input->post('category');
		$data['user_id']=$this->uid;
		
		redirect('artist/upload_crop/'.$this->m_artist->save_artist($data));
	}
	
	
	//nama : Adi
	//tanggal : 3 Jan 2012
	//deskripsi : create artist - upload & crop photo
	//------------------------------------------------------------------------------------------------------------------------upload crop
	function upload_crop($id='')
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('member/frontpage', 'refresh');
		}
		else
		{
			$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			
			/*
			$data['a_name'] = $this->input->post('name');
			$data['a_category'] = $this->input->post('category');
			$data['user_id']=$this->uid;
			if(!$this->input->post('name')){
				//if($id==''){
					redirect('artist','refresh');
				//}else{
				//	$row2=$this->m_artist->getArtistProfile($id);
				//	$data['artist_id']=$id;
				//	$data2['artist_id']=$id;
				//}
			}else{
				$row2=$this->m_artist->getArtistProfile($this->m_artist->save_artist($data));
			}
			*/
			$row2=$this->m_artist->getArtistProfile($id);
			
			if($this->input->post('email')){
				$msg = '<html><body><h1><a href="ideafieldproject.com/popmaya">Popmaya</a></h1></body></html>';
				$subject = "join popmaya";
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: popmaya@example.com' . "\r\n";
				mail($this->input->post('email'), $subject,$msg,$headers);
			}else{
			}
			
			//setting up viewer
			$page_req="pm_artist";
			$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js');
	      	$data['foot_script']='
		        var sics = $(".share_icons"); sics.hide();
		        $(\'#slider\').nivoSlider({speed:5000});
		        
            $("#tab ul li.middle a").addClass("active").show(); 
            $("#tab2").show();  

		      ';	      

			//if($id=='') //my profile
			//{
				$row=$this->m_artist->getUserProfile($this->uid);
			//}
			//else //other member profile
			//{
				//$row=$this->m_artist->getUserProfile($id);
			//}
			//if($id==''){
			//	$row2=$this->m_artist->getArtistProfile(1);
			//}
			//else{
			//	$row2=$this->m_artist->getArtistProfile($this->m_artist->save_artist($data));
			//}
			
						
			//setting data
			$data2['artist_name']=$row2->artist_name;
			$data2['title']=$row2->title;
			$data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			$data2['artist_id']=$row2->ID;
			$data['artist_id']=$row2->ID;
			
			$data['banner_image'] = $this->m_member->get_image_banner_profile();
			$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			$data['user_id']=$this->uid;
			
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		

			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
			//load ads data
			$data['ads_data']=$this->m_artist->get_image_ads();
			
			/****================================ tambahan ===================================****/ 
			//Adi 26 Dec 2011
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
			$data['count_notification']=$this->m_member->get_count_notification($this->uid);
			
			$data2['song_list']=$this->m_artist->get_song_by_artist($id,5);
			
			//music sidebar, suggest song drop down menu		
			$fav_genre=$this->m_member->get_fav_genre($this->uid);
			$fav_genre=explode(", ", $fav_genre);
			$genres=$this->m_member->get_genre();
			$i=0;
			$fav_genre_display='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}				
			}	
			$data['suggest_song_option']=$fav_genre_display;

			$genre_id=$fav_genre_display[0]['id'];
			$limit=$this->config->item('limit_sidebar_suggest_song_list');		
			$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);
			/*================================end tambahan ========================================*/	
			
			
			/*****  UPLOAD AND CROP FOTO ********/
			
			$data['upload_path']        = $upload_path          = "assets/images/artist/" ;
			$data['destination_thumbs'] = $destination_thumbs   = "assets/images/artist/thumbnail/" ;
			$data['destination_medium'] = $destination_medium   = "assets/images/artist/medium/" ;

			$size_medium_width  = 300 ;
			$size_medium_height = 300 ;


			$data['large_photo_exists'] = $data['thumb_photo_exists'] = $data['error'] = NULL ;
			$data['thumb_width']        = $thumb_width  = 100 ;
			$data['thumb_height']       = $thumb_height = 100 ;
			
			if (($this->input->post('upload'))) {
				$config['upload_path']  = $upload_path ;
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['max_size']     = '2000';
				$config['max_width']    = '2000';
				$config['max_height']   = '2000';
				$config['file_name'] = $id;

				$this->load->library('upload', $config);

				if ($this->upload->do_upload("image")) {
					$data['img']	 = $this->upload->data();

					$resize = $this->image_moo
								->load($upload_path . $data['img']['file_name'])
								->resize($size_medium_width,$size_medium_height)
								->save($destination_medium . $data['img']['file_name']) ;

					if ($resize) {
						list($medium_width,$medium_height) = getimagesize($destination_medium . $data['img']['file_name']);

						$data['img']['image_width']    = $medium_width ;
						$data['img']['image_height']   = $medium_height ;

						// For Auto Selection, we must define X1, X2, Y1, and Y2
						$data['img']['x1']  = ($medium_width/2) - ($thumb_width/2) ;
						$data['img']['x2']  = ($medium_width/2) + ($thumb_width/2) ;
						$data['img']['y1']  = ($medium_height/2) - ($thumb_height/2) ;
						$data['img']['y2']  = ($medium_height/2) + ($thumb_height/2) ;
					}
					
					$data['large_photo_exists']  = "<img src=\"".base_url() . $destination_medium.$data['img']['file_name']."\" alt=\"Medium Image\"/>";
				}
			}
			elseif (($this->input->post('upload_thumbnail'))) {
				$x1 = $this->input->post('x1',TRUE) ;
				$y1 = $this->input->post('y1',TRUE) ;
				$x2 = $this->input->post('x2',TRUE) ;
				$y2 = $this->input->post('y2',TRUE) ;
				$w  = $this->input->post('w',TRUE) ;
				$h  = $this->input->post('h',TRUE) ;

				$file_name                  = $this->input->post('file_name',TRUE) ;
				
				if ($file_name) {
					$this->image_moo
						->load($destination_medium . $file_name)
						->crop($x1,$y1,$x2,$y2)
						->save($destination_thumbs . $file_name)
						->load($destination_thumbs . $file_name)
						->resize($data['thumb_width'],$data['thumb_height'])
						->save($destination_thumbs . $file_name,TRUE) ;


					if ($this->image_moo->errors) {
						$data['error'] = $this->image_moo->display_errors() ;
					}
					else {
						$data['thumb_photo_exists'] = "<img src=\"".base_url() . $destination_thumbs . $file_name."\" alt=\"Thumbnail Image\"/>";
						$data['large_photo_exists'] = "<img src=\"".base_url() . $destination_medium . $file_name."\" alt=\"Medium Image\"/>";
						redirect('artist/profile/'.$id);
					}
				}

			}
			$template_crop['template'] = 'template_crop.php';
			$template_crop['regions'] = array(
				'keyword',
			   'middle_content',
			   'sidebar_left',
			   'sidebar_right',
			   'profile_menu'
			);
			$template_crop['parser'] = 'parser';
			$template_crop['parser_method'] = 'parse';
			$template_crop['parse_template'] = FALSE;
			$template_crop['regions']['keywords'] = array('content' => array('Popmaya'));
			$template_crop['regions']['header'] = array('content' => array(anchor('', '<h1 class="ir" id="logo">Popmaya</h1>','')));
			$template_crop['regions']['head_title'] = array('content' => array('Popmaya &rsaquo; '));
			$template_crop['regions']['footer'] = array('content' => array('Copyright &copy; 2011 Popmaya. '.anchor(prep_url('www.theideafield.com'), 'POWERED BY THE IDEAFIELD', 'class="web_dev" title="web developent by www.theideafield.com"')));

			$this->template->add_template('template_crop',$template_crop,true);
		}				
					
		//render view
		$this->template->write('head_title', 'Profile');
		$this->template->write('keywords', '');
		$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		$this->template->write_view('sidebar_left', 'pm_side_artist_profile', $data2, '');
		if($this->m_artist->artist_member($this->uid,$id)){
			$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $id, '');
		}else{
			$this->template->write_view('sidebar_right', 'pm_artist_side_right', $data, '');
		}
		//$this->template->write_view('middle_content', 'pm_artist', $data, '');
		$this->template->write_view('middle_content','pm_upload_crop',$data) ;
		$this->template->render();
				
		
	} 
	//------------------------------------------------------------------------------------------------------------------------end upload crop
}
?>
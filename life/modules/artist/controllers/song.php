<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
//Name : adi
//Date : 21 Des 2011
//Desc : controller for song album
class Song extends MX_Controller 
{
	var $user_id;
	var $limit_sidebar_friend_list;
	private $limit = 20;
	
   	function __construct()
	{
   		parent::__construct();

   		$this->load->library('form_validation');
   		$this->load->model('member/m_member', '', TRUE);
   		$this->load->model('home/m_home', '', TRUE);
		$this->load->model('artist/m_artist', '', TRUE);
		$this->load->model('track/m_track', '', TRUE);

		$this->user_id=$this->session->userdata('user_id');
		
		//$this->template->add_region('ttl');
		  
		$this->config->load('my_config');
  		$this->limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
//   		$this->output->enable_profiler(FALSE);
   	}

	function index($id='')
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}
		else
		{
			//setting up viewer
			$page_req="pm_profile_idx";
			$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').  js_asset('jwysiwyg/jquery.wysiwyg.js').js_asset('jRecorder/jRecorder.js').js_asset('fileuploader.js').js_asset('jPlayer/jquery.jplayer.min.js');
	      	$data['foot_script']='
		        var sics = $(".share_icons"); sics.hide();
		        $(\'#slider\').nivoSlider({speed:5000});

		        $("#tab ul li.middle a").addClass("active").show(); 
 	           	$("#tab2").show();
 	           	/* ^zy */
 	           	var the_player = $("#song_player");
 	           	
 	           	the_player.jPlayer({
 	           		ready: function(){
						console.log("player ready."); 	           			
 	           		},
 	           		play: function(){
 	           			console.log("play file.");	
 	           		},
 	           		volume: "0.4",
 	           		swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});

				var prev_val=0;
 	           	$("#wrapper .btn_play").click(function(e){
 	           		the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
		        });
		        /* ^zy.end */
		      ';	      

			if($id=='') //my profile
			{
				$row=$this->m_member->get_user_profile($this->user_id);
			}
			else //other member profile
			{
				$row=$this->m_member->get_user_profile($id);
			}
			
			$row2=$this->m_artist->getArtistProfile(1);
			
			$data2['artist_name']=$row2->artist_name;
			$data2['title']=$row2->title;
			$data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			
			//setting data
			$data['user_id']=$this->user_id;
			$data['image_path']=$this->m_member->get_profile_path($this->user_id);
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
			$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
			
			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
			
			//music sidebar, my song list
  			$limit=$this->config->item('limit_sidebar_fave_song_list');
  			$data['song_list']=$this->m_member->get_fav_song($this->user_id, $limit);		

  			//music sidebar, suggest song drop down menu		
	  		$fav_genre=$this->m_member->get_fav_genre($this->user_id);
	  		$fav_genre=explode(", ", $fav_genre);
	  		$genres=$this->m_member->get_genre();
	  		$i=0;
	  		$fav_genre_display='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}				
			}
		
			$data['suggest_song_option']=$fav_genre_display;

		    $genre_id=$fav_genre_display[0]['id'];
	  		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
	  		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);

	  		$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			
			//save album
			if($this->input->post('createalbum')){
				$album = array(
					'memberID' => $this->user_id ,
					'pageID' => 0,
					'articleID' => 0,
					'title' => $this->input->post('title'),
					'image' => 'adam_thumb.jpg',
					'description' => $this->input->post('desc'),
					'status' => 0,
					'post' => 0
				);
				$this->m_member->save_album($album);
			}
			$data['user_album'] = $this->m_member->get_album($id);
			
			//render view
			$this->template->write('head_title', 'Profile');
			$this->template->write('keywords', '');
		    $this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
			$this->template->write_view('sidebar_left', 'pm_side_artist', $data2, '');			
			$this->template->write_view('middle_content', 'pm_artist_create_album', $data, '');
			$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $data, '');
			$this->template->render();
		}
	}
	
	function album_list($id='')
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}
		else
		{
			//setting up viewer
			$page_req="pm_profile_idx";
			$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').  js_asset('jwysiwyg/jquery.wysiwyg.js').js_asset('jRecorder/jRecorder.js').js_asset('fileuploader.js').js_asset('jPlayer/jquery.jplayer.min.js');
	      	$data['foot_script']='
		        var sics = $(".share_icons"); sics.hide();
		        $(\'#slider\').nivoSlider({speed:5000});

		        $("#tab ul li.middle a").addClass("active").show(); 
 	           	$("#tab2").show();
 	           	/* ^zy */
 	           	var the_player = $("#song_player");
 	           	
 	           	the_player.jPlayer({
 	           		ready: function(){
						console.log("player ready."); 	           			
 	           		},
 	           		play: function(){
 	           			console.log("play file.");	
 	           		},
 	           		volume: "0.4",
 	           		swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});

				var prev_val=0;
 	           	$("#wrapper .btn_play").click(function(e){
 	           		the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
		        });
		        /* ^zy.end */
		      ';	      

			if($id==''){
				redirect('artist', 'refresh');
			}
			else{
				$row2=$this->m_artist->getArtistProfile($id);
			}
			
			// belom ngambil by user session, ini biar jalan dulu aja
			//$row=$this->m_member->get_user_profile($this->user_id);
			
			//save album
			if($this->input->post('create_album')){
				$album = array(
					'memberID' => $id ,
					'title' => $this->input->post('album_name'),
					'image' => $this->input->post('cover_album'),
					'description' => '',
					'status' => 0
				);
				$this->m_artist->save_album_song($album);
			}
			
			$row=$this->m_member->get_user_profile($this->user_id);
			
			
			$data2['artist_name']=$row2->artist_name;
			$data2['title']=$row2->title;
			$data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			
			//setting data
			$data['user_id']=$this->user_id;
			$data['image_path']=$this->m_member->get_profile_path($this->user_id);
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
			$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
			
			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
			
			//music sidebar, my song list
  			$limit=$this->config->item('limit_sidebar_fave_song_list');
  			$data['song_list']=$this->m_member->get_fav_song($this->user_id, $limit);
			$data['artist_id']=$id;			
			
			$data['artist_album'] = $this->m_artist->get_album_song($id);
			$song_count = array();
			foreach($data['artist_album'] as $album){
				array_push($song_count,$this->m_artist->get_count_album_song($album->as_id));
			}
			$data['song_count'] = $song_count;
			
			
			$data['artist_name']=$row2->artist_name;

  			//music sidebar, suggest song drop down menu		
	  		$fav_genre=$this->m_member->get_fav_genre($this->user_id);
	  		$fav_genre=explode(", ", $fav_genre);
	  		$genres=$this->m_member->get_genre();
	  		$i=0;
	  		$fav_genre_display='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}				
			}
		
			$data['suggest_song_option']=$fav_genre_display;

		    $genre_id=$fav_genre_display[0]['id'];
	  		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
	  		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);

	  		$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);

			//render view
			$this->template->write('head_title', 'Profile');
			$this->template->write('keywords', '');
		    $this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
			$this->template->write_view('sidebar_left', 'pm_side_artist', $data2, '');			
			$this->template->write_view('middle_content', 'pm_artist_album_idx', $data, '');
			$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $data, '');
			$this->template->render();
		}
	}

	function create($id='')
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}
		else
		{
			if($id==''){
				redirect('artist/profile', 'refresh');
			}else{
				$row2=$this->m_artist->getArtistProfile($id);
			}
			
			//setting up viewer
			$page_req="pm_profile_idx";
			$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').  js_asset('jwysiwyg/jquery.wysiwyg.js').js_asset('jRecorder/jRecorder.js').js_asset('fileuploader.js').js_asset('jPlayer/jquery.jplayer.min.js');
	      	$data['foot_script']='
		        var sics = $(".share_icons"); sics.hide();
		        $(\'#slider\').nivoSlider({speed:5000});

		        $("#tab ul li.middle a").addClass("active").show(); 
 	           	$("#tab2").show();
 	           	/* ^zy */
 	           	var the_player = $("#song_player");
 	           	
 	           	the_player.jPlayer({
 	           		ready: function(){
						console.log("player ready."); 	           			
 	           		},
 	           		play: function(){
 	           			console.log("play file.");	
 	           		},
 	           		volume: "0.4",
 	           		swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});

				var prev_val=0;
 	           	$("#wrapper .btn_play").click(function(e){
 	           		the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
		        });
		        /* ^zy.end */
		      ';	      

			$row=$this->m_member->get_user_profile($this->user_id);
			
			
			
			$data2['artist_name']=$row2->artist_name;
			$data2['title']=$row2->title;
			$data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			
			//setting data
			$data['user_id']=$this->user_id;
			$data['image_path']=$this->m_member->get_profile_path($this->user_id);
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
			$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
			
			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
			
			//music sidebar, my song list
  			$limit=$this->config->item('limit_sidebar_fave_song_list');
  			$data['song_list']=$this->m_member->get_fav_song($this->user_id, $limit);		

  			//music sidebar, suggest song drop down menu		
	  		$fav_genre=$this->m_member->get_fav_genre($this->user_id);
	  		$fav_genre=explode(", ", $fav_genre);
	  		$genres=$this->m_member->get_genre();
	  		$i=0;
	  		$fav_genre_display='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}				
			}
		
			$data['suggest_song_option']=$fav_genre_display;

		    $genre_id=$fav_genre_display[0]['id'];
	  		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
	  		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);

	  		$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			$data['artist_id']=$id;
			
			
			//$data['user_album'] = $this->m_member->get_album($id);
			
			//render view
			$this->template->write('head_title', 'Profile');
			$this->template->write('keywords', '');
		    $this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
			$this->template->write_view('sidebar_left', 'pm_side_artist', $data2, '');			
			$this->template->write_view('middle_content', 'pm_artist_create_album', $data, '');
			$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $data, '');
			$this->template->render();
		}
	}

	function edit_album($id=''){
		//render view
        $this->template->write('head_title', 'Edit album');
        $data['foot_js']=$data['warn_js']='';

        /* pagination */ 
        $uri_segment = 4;
        //if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($uri_segment); }
        //$data['start_no']= $offset+1;

        $this->load->library('pagination');
        $config['base_url'] = site_url().'track/manage/list_song';
        $config['total_rows'] = $this->m_track->count_all('');
        $config['per_page'] = $this->limit;
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        /* pagination.end */

		$row=$this->m_member->get_user_profile($this->user_id);
		//$row2=$this->m_artist->getArtistProfile($id);
		
		//setting data
		$data['user_id']=$this->user_id;
		$data['image_path']=$this->m_member->get_profile_path($this->user_id);
		$data['name']=$row->up_name;
		$data['alias']=$row->up_alias;	
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);	
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
		$data['count_notification']=$this->m_member->get_count_notification($this->user_id);

		$data['artist_id']=$this->m_artist->get_artist_id($id);
		$data['album_id']=$id;
		
		$data['gender_code']=$row->up_gender;
		$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
		$data['city']=$row->up_city;
		$data['country']=$row->up_country;
		$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
		
		$row2=$this->m_artist->getArtistProfile($this->m_artist->get_artist_id($id));
		
		$data2['artist_name']=$row2->artist_name;
		$data2['title']=$row2->title;
		$data2['genre']=$this->m_artist->get_genre_name($row2->genre);
		
		if($id==''){
			redirect('artist','refresh');
		}else{
			$row3=$this->m_artist->getAlbum($id);
		}
		$data['album_name']=$row3->title;
		$data['album_image']=$row3->image;		
		
		$data['list_songs']=$this->m_track->get_paged_list($this->limit, 0, array('albumID' => $id));
		
        $this->template->write('head_title', 'Profile');
		$this->template->write('keywords', '');
		$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		$this->template->write_view('sidebar_left', 'pm_side_artist', $data2, '');
        $this->template->write_view('middle_content', 'pm_edit_album', $data, '');
		$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $data, '');
        $this->template->render();  
    }
	
	function edit_album_proccess($id=''){
		$err_ev='';
        $err_type='success';
        $arr2 = array('genreID'=>0, 'albumID'=>$id, 'time_length'=>0, 'title'=>'new song', 'post'=>'', 'memberID'=>$this->session->userdata('user_id'));
		
		if($this->input->post('submit')){
			$data['album_name']=$this->input->post('album_name');
			$data['album_image']='';
			$this->m_artist->updateAlbum($id,$data);		

			$song_list = $this->m_artist->get_song_by_album($id);
			
			foreach ($song_list as $items=>$item){
				$s_id = $item->s_id;
				$data_update['title'] = $this->input->post('cb_prev_'.$s_id);
			}
			
			redirect('artist/song/edit_album/'.$id, 'refresh');
		}else if($this->input->post('cancel')){
			redirect('artist/song/album_list/'.$this->m_artist->get_artist_id($id), 'refresh');
		}else if($this->input->post('upload_song')){
			redirect('artist/song/upload_song/'.$id);
		}
	}
	
	function upload_song($id=''){
        $err_ev='';
        $err_type='success';
        $arr2 = array('genreID'=>0, 'albumID'=>$id, 'time_length'=>0, 'title'=>'new song', 'post'=>'', 'memberID'=>$this->session->userdata('user_id'));
        
        if($this->input->post('item_id')){
            $item_id=$this->input->post('item_id');
            $arr1 = array('s_id'=>$item_id);
            $this->m_track->save('edit', $arr1, $arr2);
            $err_ev='Data updated. ';
            $row=$this->m_track->item_info($item_id);
            $arr=explode(' ', $row->created_on);
            $arr2=explode('-', $arr[0]);
            $yr=$arr2[0];
            $mon=$arr2[1];
            $day=$arr2[2];
        }else{
            $item_id=$this->m_track->save('add', '', $arr2);
            $err_ev='Data saved. ';
            $yr=date('Y');
            $mon=date('m');
            $day=date('d');
        }

        $mon=sprintf("%02s",$mon);
        $day=sprintf("%02s",$day);

        $dir_path='./assets/media/';
        
        if(!is_dir($dir_path.'/'.$yr.'/')){ mkdir($dir_path.'/'.$yr); }
        if(!is_dir($dir_path.'/'.$yr.'/'.$mon.'/')){ mkdir($dir_path.$yr.'/'.$mon.'/'); }
        if(!is_dir($dir_path.'/'.$yr.'/'.$mon.'/'.$day.'/')){ mkdir($dir_path.$yr.'/'.$mon.'/'.$day.'/'); }

        $dir_path=$dir_path.$yr.'/'.$mon.'/'.$day.'/';
        $config['upload_path'] = $dir_path;
        $config['overwrite'] = TRUE;
        $config['file_name'] = $item_id;
        $config['allowed_types'] = 'mp3';
        $config['max_size'] = '7168';

        $this->load->library('upload', $config);
        if(!$this->upload->do_upload()){
            $err2=$this->upload->display_errors();
        }else{
            $err2=' File uploaded.';
            $info = $this->upload->data();
            $this->m_track->save('edit', array('s_id'=>$item_id), array('file_size'=> $info['file_size']));
            $input_file = $info['full_path'];
            $output_file = $info['file_path'] .'sample_'. $info['orig_name'];
            exec("ffmpeg -ss 0 -t 30 -i $input_file $output_file");
        }

        set_flash('warn', $err_type, $err_ev.' '.$err2);
        
        redirect('artist/song/edit_album/'.$id, 'refresh');
    }
	
}
<div class="sidebar-right">
    <div class="friend-page">
        ADMIN TOOLS</p>
        <ul>
            <li><?= anchor('#', 'Use Popmaya as'); ?></li>
            <li><?= anchor('#', 'Edit Profile'); ?></li>
			<li><?= anchor(site_url().'artist/song/create/'.$artist_id, 'Upload Album/Song'); ?></li>
			<li><?= anchor('#', 'Blast Email to Fans'); ?></li>
			<li><?= anchor('#', 'Application'); ?></li>
			<li><?= anchor('#', 'Mobile Premium'); ?></li>
        </ul>
	</div>
</div>
<div class="divider">&nbsp;</div>
<div class="sidebar-right">
    <div class="friend-page">
        Invite People by:</p>
        <ul>
            <li><?= anchor('#', 'Google', array('class'=>'google')); ?></li>
            <li><?= anchor('#', 'Yahoo', array('class'=>'yahoo')); ?></li>
        </ul>
        
        <form action="#" method="post" id="submit_email">
            <label for="email">or type an email:</label>
            <input type="text" name="email" placeholder="email" class="text-input" id="email"/>
            <input type="submit" name="submit" value="Submit" class="submit"/>
        </form>
    </div>
</div>	
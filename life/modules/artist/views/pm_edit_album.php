<? 
	$attr=array();
	$hidden=array('prev_page'=>current_url());
	echo form_open_multipart('artist/song/edit_album_proccess/'.$album_id,$attr, $hidden);  ?>
<div class="form_add">
	<table>
	<?php 
		$attr=array();
		$hidden=array('prev_page'=>current_url());
		
		echo '<tr><td></td><td><h3>Edit Album</h3></td></tr>';
		echo '<tr><td>'.image_asset('album_art.png', '', array('alt'=>'profile')).'</td></tr>';
		echo '<tr><td>Album Name</td><td>'.form_input(array('name'=>'album_name','value'=>$album_name)).'</td></tr>';
		echo '<tr><td>Cover Album</td><td>'.form_upload('cover_album', '</td></tr>');
		echo '<tr><td>Upload Song</td><td>'.form_upload('song', '');
		echo form_submit(array('name'=>'upload_song', 'value'=>'Upload Song')).'</td></tr>';
		echo '<tr><td></td></tr><tr><td></td></tr>';
	?>
	</table>
</div>
<?= $pagination ?>
<table class="list_song">
	<tr>
		<!--<th></th>--><!--<th>No</th>--><th>Nama</th><th>Prev Only</th><th>Download</th><th>Price</th><th></th>
	</tr>
	<?php
		
		if(count($list_songs)>0){
			$i=0;
			foreach ($list_songs as $items => $item) {
				$cls='';
				if(($i%2)==1){
					$cls='class="odd"';
				}
				echo '
			<tr '.$cls.'>
				<!--<td>'.$i.'</td>-->
				<td>'.form_input(array('name'=>'song_name_'.$item->s_id, 'value'=>$item->title)).'</td>
				<td>'.form_checkbox('cb_prev_'.$item->s_id,'',$item->isprevonly==0 ? FALSE : TRUE).'</td>
				<td>'.form_checkbox('cb_down_'.$item->s_id,'',$item->isdownload==0 ? FALSE : TRUE).'</td>
				<td>'.form_dropdown('Price',array('1' => 'free','2' => '$1.00'), '2').'</td>
				<td>'.anchor('track/manage/delete/'.$item->s_id, 'Delete', array('class'=>'delConf')).'</td>
			</tr>
				';
				$i++;
			}
		}
	?>
</table>
<div>
<?
	echo form_submit(array('name'=>'submit', 'value'=>'Submit'));
	echo form_submit(array('name'=>'cancel', 'value'=>'Cancel'));
	echo form_close();
?>
</div>
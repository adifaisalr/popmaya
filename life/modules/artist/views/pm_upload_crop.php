	  <?= js_asset('jquery-1.3.2.min.js'); ?>
	<?= js_asset('jquery.imgareaselect.min.js'); ?>
  
    <?php if($large_photo_exists && $thumb_photo_exists == NULL):?>
	<?= js_asset('jquery.imgpreview.js'); ?>
    <script type="text/javascript">
    // <![CDATA[
        var thumb_width    = <?php echo $thumb_width ;?> ;
        var thumb_height   = <?php echo $thumb_height ;?> ;
        var image_width    = <?php echo $img['image_width'] ;?> ;
        var image_height   = <?php echo $img['image_height'] ;?> ;

        var auto_x1 = <?php echo $img['x1'] ;?> ;
        var auto_x2 = <?php echo $img['x2'] ;?> ;
        var auto_y1 = <?php echo $img['y1'] ;?> ;
        var auto_y2 = <?php echo $img['y2'] ;?> ;
    // ]]>
    </script>
    <?php endif ;?>

<h1>Photo Upload and Crop</h1>
<?php if($error) :?>
    <ul><li><strong>Error!</strong></li><li><?php echo $error ;?></li></ul>
<?php endif ;?>

<?php if($large_photo_exists && $thumb_photo_exists) :?>
	<?php echo $large_photo_exists."&nbsp;".$thumb_photo_exists; ?>
	<p><a href="<?=site_url().'artist/upload_crop/'.$artist_id?>">Upload another</a></p>

<?php elseif($large_photo_exists && $thumb_photo_exists == NULL) :?>

<h2>Create Thumbnail</h2>
<div align="center">
        <img src="<?php echo base_url() . $destination_medium . $img['file_name'];?>" style="float: left; margin-right: 10px;" id="thumbnail" alt="Create Thumbnail" />
        <div style="border:1px #e5e5e5 solid; float:left; position:relative; overflow:hidden; width:<?php echo $thumb_width;?>px; height:<?php echo $thumb_height;?>px;">
                <img src="<?php echo base_url() . $destination_medium .$img['file_name'];?>" style="position: relative; " alt="Thumbnail Preview" />
        </div>
        <br style="clear:both;"/>
        <form name="thumbnail" action="<?=site_url().'artist/upload_crop/'.$artist_id?>" method="post">
                <input type="hidden" name="x1" value="0" id="x1" />
                <input type="hidden" name="y1" value="0" id="y1" />
                <input type="hidden" name="x2" value="0" id="x2" />
                <input type="hidden" name="y2" value="0" id="y2" />
                <input type="hidden" name="file_name" value="<?php echo $img['file_name'] ;?>" />
                <input type="submit" name="upload_thumbnail" value="Save Thumbnail" id="save_thumb" />
        </form>
</div>

<hr />
<?php 	else : ?>

<h2>Upload Photo</h2>
<form name="photo" enctype="multipart/form-data" action="<?=site_url().'artist/upload_crop/'.$artist_id?>" method="post">
Photo <input type="file" name="image" size="30" /> <input type="submit" name="upload" value="Upload" />
</form>
<?php 	endif ?>

</body>
</html>

<div class="share">
    <input type="text" name="share" class="input-share" placeholder="Share what's new..." title="Share what's new..."/>
    <div class="temp_share">
        
        <div class="share_photo_link share_box">
              <?= image_asset('general/upload_photo.png','',array('class'=>'upload_photo','onclick'=>'js:showUploadPhoto()')).' &nbsp; '.image_asset('general/create_album.png','', array('class'=>'create_album','onclick'=>'js:showCreateAlbum()')); ?>
              <div class="share_photo_box">
                  <div class="box_create_album">
                      <?= image_asset('closebutton.png','',array('class'=>'btn_close','onclick'=>'js:showCreateAlbum()'))?>
                      <h4>Create Album</h4>
                      <div class="input">
                          <div class="row">
                              <label for="title">Title</label><input class="title" type="text" name="title"/>
                          </div>
                          <div class="row">
                              <label for="desc">
                                  Description
                              </label>
                              <textarea name="desc"></textarea>
                          </div>
                      </div>
                      <div class="action">
                          <input type="button" class="button" value="Create Now" onclick="js:createAlbum()"/>
                      </div>
                  </div>
                  <div class="box_upload_photo">
                      <?= image_asset('closebutton.png','',array('class'=>'btn_close','onclick'=>'js:showUploadPhoto()'))?>
                      <h4>Upload Photo</h4>
                      <div class="input">
                          <div class="row">
                          <label for="album">Select Album</label>
                          <select class="album" name="album">
                              <option value="1">Mobile</option>
                              <option value="2">Public</option>
                          </select>
                          </div>
                          <div class="row">
                              <label for="photo">
                                  Select Photo
                              </label>
                              <div id="photo_file">       
                                    <noscript>          
                                        <p>Please enable JavaScript to use file uploader.</p>
                                        <!-- or put a simple form for upload here -->
                                    </noscript>         
                                </div>
                          </div>
                          <div class="row">
                              <label for="desc">
                                  Description
                              </label>
                              <textarea class="desc" name="desc"></textarea>
                          </div>
                      </div>
                      <div class="action">
                          <input type="button" class="button" value="Upload Now" onclick="js:submitPhoto();return false;"/>
                      </div>
                  </div>
              </div>
            </div>
            
            <div class="share_voice_link share_box">
                <?= image_asset('general/record_voice.png', '', array('class'=>'btn_start_record','onclick'=>'js:recordAudio()'));?>
                <div id="audio-record">
                    <span class="btn-audio-record" onclick="js:recordAudio();">&nbsp;</span>
                    <div class="audio-level-off">
                        <div class="audio-level-on">
                            
                        </div>
                    </div>
                    <span class="audio-time">0</span>
                </div>
            </div>
        
            <div class="share_video_link share_box">
              <input type="text" name="video_link" class="video_link" />
              <input type="button" name="submit" value="submit" class="btn_submit" onclick="js:submitVideo();" />
            </div>
        
            <div class="share_note_link share_box">
                <?php $attr=array('class'=>'form_note'); $hidden=''; ?>
                <?= form_open_multipart('member/share_note', $attr, $hidden); ?>
                <?= form_textarea(array('name'=>'note', 'class'=>'note_content')); ?>
                <div id="note_file">       
                    <noscript>          
                        <p>Please enable JavaScript to use file uploader.</p>
                        <!-- or put a simple form for upload here -->
                    </noscript>         
                </div>
                <small>(Note:Maximum photo size is 1024 Kb, *.jpg, *.gif, *.png)</small><br />
                Keywords Tag <?= form_input(array('name'=>'tags','class'=>'tags')); ?><br />
                <?= form_checkbox('cb_draft', 1, FALSE, array('class'=>'draft')); ?> Save as draft (artikel kamu tidak langsung terpublish jika pilih ini)<br />
                <?= form_checkbox('cb_agree', 1, FALSE, array('class'=>'agree')); ?> I have read and agree the TOS.<br />
                <input type="button" name="buton" value="submit" class="button" onclick="js:submitNote(); return false;" />
                <?= form_close(); ?>
              </div>
    </div>
    <div class="share_icons">
       <span class="status_ttl">Share</span>
       <ul class="share_links">
          <li><?= anchor('#', image_asset('icon/icon_share_status.png'), array('class'=>'share_status')); ?></li>
          <li><?= anchor('#share_voice', image_asset('icon/icon_share_voice.png'), array('class'=>'share_voice')); ?>
            
          </li>
          <li><?= anchor('#share_photo', image_asset('icon/icon_share_photo.png'), array('class'=>'share_photo')); ?>
              
          </li>
          <li><?= anchor('#share_movie', image_asset('icon/icon_share_movie.png'), array('class'=>'share_movie')); ?>
            
          </li>
          <li><?= anchor('#share_note', image_asset('icon/icon_share_note.png'), array('class'=>'share_note')); ?>
              
          </li>
       </ul>
       <?php
        echo form_open('member/status_update'); 
        echo '<input type="hidden" name="status_text" class="status_text" />';
        ?>
       <?= form_submit('submit', 'Submit', 'class="share_submit"'); ?>
       <?= form_close(); ?>
    </div>    
</div>
<div id="tab">
    <ul>
        <li class="first"><?= anchor(current_url().'#tab1', 'Update'); ?></li>
        <li class="middle"><?= anchor(current_url().'#tab2', 'Music Track'); ?></li>
        <li class="last"><?= anchor(current_url().'#tab3', 'Playlist'); ?></li>
    </ul>
</div>
<div id="load_tab">
    <div id="tab1" class="tab_content">
        <ul class="status-list">
            <li>
                <div class="status-id">3</div>
                <div class="author clearfix">
                    <?= image_asset('cd_thumb.jpg','',array('class'=>'thumb')); ?>
                    <div class="author-name">Yohan</div>
                    <span class="author-id">12</span>
                </div>
                <div class="status-content">
                    <div class="status-text">
                        Fighting #AIDS for 30 years. Photo timeline: bit.ly/rNk1sU #WorldAIDSDay #WAD2011
                    </div>
                </div>
				<div class="button" style="cursor:pointer;">
					Comment
				</div>
                <div class="clear"></div>
            </li>
            <li>
                <div class="status-id">2</div>
                <div class="author clearfix">
                    <?= image_asset('cd_thumb.jpg','',array('class'=>'thumb')); ?>
                    <div class="author-name">Yohan</div>
                    <span class="author-id">12</span>
                    <div class="clear"></div>
                </div>
                <div class="status-content">
                    <div class="status-video">
                        <iframe width="378" height="250" src="http://www.youtube.com/embed/FKKVMxfkJWo?wmode=transparent" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="clear"></div>
            </li>
            <li>
                <div class="status-id">1</div>
                <div class="author">
                    <?= image_asset('cd_thumb.jpg','',array('class'=>'thumb')); ?>
                    <div class="author-name">Yohan</div>
                    <span class="author-id">12</span>
                </div>
                <div class="status-content">
                    <div class="status-text">
                        Fighting #AIDS for 30 years. Photo timeline: bit.ly/rNk1sU #WorldAIDSDay #WAD2011
                    </div>
                </div>
                <div class="clear"></div>
            </li>
        </ul>
        <div class="btn-more">
            Load More...
        </div>
    </div>
    <div id="tab2" class="tab_content">
       
        <div id="slider-wrapper">
            <div id="slider" class="nivoSlider">
				<? foreach($banner_image as $banner){ ?>
					<?= image_asset('banner/profile/'.$banner->image, '',array('alt'=>'banner1')); ?>
				<? }?>               
		   </div>
        </div>
        <div id="track_list">
            <h2>Recommended Track</h2>

            <!-- Tambahan
			Adi 26 Dec 2011 
			Link buat song detail -->
            <?php foreach($recommended_track as $row) { ?>
              <div class="list_box">
                  <div class="square">
				  	<?= image_asset('efek_thumb.jpg', '',array('alt'=>'efek', 'class'=>'thumb-box'))?>
				    <h3><a href="<?=site_url().'track/detail/'.$row->s_id?>"><?= $row->title ?></a></h3>
				    <span class="title">Band Name</span>
				    <span  class='st_google' st_url="<?= site_url().'assets/media/sample.mp3' ?>" st_title="Yohan shared a link" st_summary="Fighting #AIDS for 30 years. Photo timeline: bit.ly/rNk1sU #WorldAIDSDay #WAD2011"></span>
					<span  class='st_twitter' st_url="<?= site_url().'assets/media/sample.mp3' ?>" st_title="Yohan shared a link" st_summary="Fighting #AIDS for 30 years. Photo timeline: bit.ly/rNk1sU #WorldAIDSDay #WAD2011"></span>
					<span  class='st_facebook' st_url="<?= site_url().'assets/media/sample.mp3' ?>" st_title="Yohan shared a link" st_summary="Fighting #AIDS for 30 years. Photo timeline: bit.ly/rNk1sU #WorldAIDSDay #WAD2011"></span>
					
					<div class="button-share">
				    	<?= anchor(site_url().'assets/media/sample.mp3', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play', 'class'=>'btn_play')) ?>
				        <!-- tambahan Adi 27 Dec 2011 - link download music-->
						<?= anchor(site_url().'member/download_music/sample.mp3', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
				    </div>
					
				    <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play <i class="ttl_play">23</i></span>
					
					
				  </div>
              </div>
           <?php } ?>
            <div class="clear"></div>
        </div>
        <!-- Tambahan
			Adi 26 Dec 2011 
			Link buat song detail -->
        <h2 class="track-head">New Track</h2>
        <div class="new_track">          
          <?php foreach($new_track as $row) { ?>
             <div class="list_box">
                  <div class="square">
                     <?= image_asset('viera_thumb.jpg', '',array('alt'=>'vierra', 'class'=>'thumb-box'))?>
                      <h3><a href="<?=site_url().'track/detail/'.$row->s_id?>"><?= $row->title ?></a></h3>
                      <span class="title">Vierra</span>
					  <span  class='st_google' st_url="<?= site_url().'assets/media/sample.mp3' ?>" st_title="Yohan shared a link" st_summary="Fighting #AIDS for 30 years. Photo timeline: bit.ly/rNk1sU #WorldAIDSDay #WAD2011"></span>
					  <span  class='st_twitter' st_url="<?= site_url().'assets/media/sample.mp3' ?>" st_title="Yohan shared a link" st_summary="Fighting #AIDS for 30 years. Photo timeline: bit.ly/rNk1sU #WorldAIDSDay #WAD2011"></span>
					  <span  class='st_facebook' st_url="<?= site_url().'assets/media/sample.mp3' ?>" st_title="Yohan shared a link" st_summary="Fighting #AIDS for 30 years. Photo timeline: bit.ly/rNk1sU #WorldAIDSDay #WAD2011"></span>
						
                      <div class="button-share">
                         <?= anchor(site_url().'assets/media/sample.mp3', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play', 'class'=>'btn_play')) ?>
                         <!-- tambahan Adi 27 Dec 2011 - link download music-->
						 <?= anchor(site_url().'member/download_music/sample.mp3', image_asset('download_ico.png', '', array('alt'=>'Download', 'class'=>'last')), array('title'=>'Download')) ?>
                      </div>
                      <span class="pronoun-link"><a href="#">Share</a>&nbsp;|&nbsp;Total play 23</span>
                  </div>
              </div>
          <?php } ?>
          <div class="clear"></div>
        </div>
        <!--  music track tab.end -->
    </div>
    <div id="tab3" class="tab_content">
        <p>Playlists Tab</p>
    </div>
    <div class="clear"></div>
</div>

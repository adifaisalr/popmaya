<div class="head-title">
    <span><strong>All Album By : <?=$artist_name?></strong></span>
</div>
<div id="album-list">
<?php 
$i=0;
foreach($artist_album as $album) { 
?>
	<ul class="album">
		<li class="thumb"><a href="<?= site_url().'artist/song/edit_album/'.$album->as_id ?>"><img src="images/<?= $album->image ?>" alt="<?= $album->title ?>"/></a></li>
		<li class="title"><a href="<?= site_url().'artist/song/edit_album/'.$album->as_id ?>"><h2><?= $album->title ?></h2></a></li>
		<li class="resume"><?=$song_count[$i]?> song(s)</li>
	</ul>
<?php
$i++;
} ?>
</div>	
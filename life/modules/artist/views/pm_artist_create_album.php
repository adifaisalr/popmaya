<!--<form method="post" action="<?=site_url().'artist/song/album_list/'.$artist_id?>">
	<div class="row-input">
		<label for="album_name"><span>Album Name</span>:</label>
		<div class="input">
			<input type="text" name="album_name" id="first_name" class="text-input"/>
		</div>
	</div>
	<div class="row-input">
		<label for="cover_album"><span>Cover album</span>:</label>
		<div class="input">
			<input type="file" name="cover_album" size="20" />
		</div>
	</div>
	<div class="row-input">
		<label for="upload_song"><span>Upload song</span>:</label>
		<div class="input">  
			<input type="file" name="upload_song" size="20" />	
		</div>
	</div>
	<input type="submit" name="create_album" value="Submit" />	
	<input type="submit" value="Cancel" />		
</form>-->

<div class="form_add">
	<table>
	<?php 
		$attr=array();
		$hidden=array('prev_page'=>current_url());
		echo form_open_multipart('artist/song/album_list/'.$artist_id, $attr, $hidden); 
		echo '<tr><td></td><td><h3>Create Album Song</h3></td></tr>';
		echo '<tr><td>'.image_asset('album_art.png', '', array('alt'=>'profile')).'</td></tr>';
		echo '<tr><td>Album Name</td><td>'.form_input(array('name'=>'album_name')).'</td></tr>';
		echo '<tr><td>Cover Album</td><td>'.form_upload('cover_album', '').'</td></tr>';
		echo '<tr><td>Upload Song</td><td>'.form_upload('upload_song', '').'</td></tr>';
		echo '<tr><td></td></tr><tr><td></td></tr>';
		echo '<tr><td>'.form_submit(array('name'=>'create_album', 'value'=>'Submit')).'</td>';
		echo '<td>'.form_submit(array('name'=>'cancel', 'value'=>'Cancel')).'</td></tr>';
		echo form_close();
	?>
	</table>
</div>
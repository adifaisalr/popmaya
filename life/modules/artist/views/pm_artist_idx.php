<div class="head-title">
          <span><strong>Artist List</strong>832.853</span>
      </div>
      
      <div id="track_list">
          <div class="sort-form">
              <label for="category-music">Sort list by</label>
              <select id="category-music">
                  <option>Rap</option>
              </select>
          </div>
		  <? foreach($artist_list as $artist){ ?>
          <div class="list_box_large">
			 
              <?= image_asset('general/layout/efek_thumb.jpg', '', array('alt'=>'efek', 'class'=>'thumb-box')); ?>
              <h3><a href="artist/profile/<?= $artist->ID ?>"><?= $artist->artist_name ?></a></h3>
              <span class="title">Genre</span>
			  <? if(strcmp($artist->member_id,$user_id)!=0){
					echo anchor('artist/become_fan/'.$artist->ID, image_asset('icon/star2.png', '',array('alt'=>'star')).' Become fans' , array('class'=>'uiButton'));
				}else{
					echo anchor('artist/stop_become_fan/'.$artist->ID, image_asset('icon/star2.png', '',array('alt'=>'star')).' Stop Become fans' , array('class'=>'uiButton'));
				}?>
		 </div>
		  
		  <? } ?>
          <div class="clear"></div>
		  <?= $pagination ?>
		  <div class="clear"></div>
      </div>
<div class="note pad520"><?php echo anchor('mobile/member/photos','Photo',array('class'=>'image left'));?><label class="right"><i><?php echo $album;?> Album</i></label><br class="clear"/></div>
    <div class="pad1020 listfren" style="color:#6d6e71;">
      <?php if(!empty($albums)):?>
      <ul>
         <?php foreach($albums as $rAlbum):?>
            <li>
                <a href="<?php echo base_url();?>mobile/photo/album_previews/<?php echo $rAlbum[0];?>/<?php echo $rAlbum[1];?>">
                <div class="left mar0_10_0"><?php echo image_asset('profile/what.jpg','',array('alt'=>$rAlbum[1],'style'=>'max-width:41px;'));?></div>
                <div class="left" style="padding-top:30px;"><?php echo $rAlbum[1];?> (<?php echo $rAlbum[3];?>)</div>
                <br class="clear"/>
                </a>
            </li>
         <?php endforeach;?>
      </ul>
      <?php else:?>
        <p>Album is empty.</p>
      <?php endif;?>
    </div>
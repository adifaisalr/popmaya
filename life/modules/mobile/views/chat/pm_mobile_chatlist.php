<div class="pad1020 bgx">
    <form method="post">
            <input type="text" class="text left w100" style="height:14px; border-right:1px #B6B6B6 solid;" name="comment_chat" /><input type="submit" class="btnsend right" value=""/>
        </form>
        <br class="clear" />
    </div>

	<div class="note pad520">
		<label><?php echo $room->cr_title;?> Room</label>
	</div>
	<div class="chatlist">
        <div class="odd">&nbsp;</div>
        <?php if(@$chatlist):?>
        <?php $i=1; foreach($chatlist as $r_msg):?>
        <div class="<?php echo ($i%2==0)?'odd':'even';?>">
          <strong><?php echo $r_msg->up_name.' '.$r_msg->up_lastname;?></strong>
          <p><?php echo $r_msg->c_mssg;?></p>
        </div>
        <?php $i++; endforeach;?>
        <?php else:?>
        <p style="margin: 10px;">Chat in this room is empty.</p>
        <?php endif;?>
    </div>
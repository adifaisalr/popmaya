
    <div class="pad1020 listfren" style="color:#6d6e71;">

      <ul>

        <li><?php echo anchor('mobile/member/profile_info','Info',array('class'=>'info'));?></li>
        <li><?php echo anchor('mobile/post/','Posts',array('class'=>'wall left'));?><br class="clear"/></li>
        <li><?php echo anchor('mobile/friend/','Friends',array('class'=>'fren left'));?><span class="right"><i><?php echo $friend;?></i></span><br class="clear"/></li>
        <li><?php echo anchor('mobile/message/inbox','Messages',array('class'=>'message left'));?><span class="right"><i><?php echo $message;?></i></span><br class="clear"/></li>
        <li><?php echo anchor('mobile/note/','Notes',array('class'=>'notes left'));?><span class="right"><i><?php echo $notes;?> Notes</i></span><br class="clear"/></li>
        <li><?php echo anchor('mobile/photo/','Photos',array('class'=>'image left'));?><span class="right"><i><?php echo $photo;?> Albums</i></span><br class="clear"/></li>
        <li><?php echo anchor('mobile/video/','Videos',array('class'=>'video left'));?><span class="right"><i><?php echo $video;?> Videos</i></span><br class="clear"/></li>
        <li><?php echo anchor('mobile/track/mysongs','My Songs',array('class'=>'song left'));?><span class="right"><i><?php echo $song;?> Tracks</i></span><br class="clear"/></li>

      </ul>


    </div>
    <div class="listfren pad1020">
    <?php if(!empty($artistResults)):?>
        <label class="art">Artist Results</label>
        <hr/>
        <ul>
            <?php foreach($artistResults as $rArtist):?>
            <li><?php echo anchor('mobile/artist/profile/'.$rArtist->ID,$rArtist->artist_name);?></li>
            <?php endforeach;?>
        </ul>
        <br class="clear"/>
    <?php endif;?>
    <?php if(!empty($friendResults)):?>
        <label class="fren">Friend Results</label>
        <hr/>
        <ul>
            <?php foreach($friendResults as $rFriend):?>
            <li><?php echo anchor('mobile/post/'.$rFriend->up_uid,$rFriend->up_name.' '.$rFriend->up_lastname);?></li>
            <?php endforeach;?>
        </ul>
        <br class="clear"/>
    <?php endif;?>    
    <?php if(!empty($songResults)):?>
        <label class="song">Song Results</label>
        <hr/>
        <ul>
            <?php foreach($songResults as $rSong):?>
            <li><?php echo $rSong->title;?></li>
            <?php endforeach;?>
        </ul>
        <br class="clear"/>
    <?php endif;?>    
    </div>

    <div class="pad1020 bgx">
        <p><?php foreach(range('A', 'Z') as $letter) 
                {
                    echo anchor('mobile/friend/search/'.$letter,$letter.' ');
                } ?></p>
        <form method="post" action="<?php echo base_url();?>mobile/friend/search">
    		<input type="text" value="" class="text left w100" name="keyword_friend"/><input type="submit" class="btnshare right" value="Search" />
            <br class="clear" />

    	</form>
<!--        <ul id="musiclist">
                <li><input type="checkbox" /><label>Friends</label></li>
                <li><input type="checkbox" /><label>All Fans</label></li>
            </ul>-->
            <br class="clear" />
    </div>

    <div class="listfren pad1020">
        <ul>
          <label class="getfren">Friend Lists</label>
          <hr />
          <?php if(!empty($friends)):?>
          <?php foreach($friends as $rowF):?>
            <li>
                <div class="pad1020">
                    <?php echo image_profile($rowF[3], array('alt'=>$rowF[1],'style'=>'max-width:30px;float:left;margin-right:10px;')); ?>
                    <b><?php echo anchor('mobile/post/'.$rowF[0],$rowF[1]);?></b><br/>
                    <span><?php echo (!empty($rowF[2]))?$rowF[2]:'&nbsp;';?></span>
                    <?php echo anchor('mobile/friend/remove/'.$rowF[0],image_asset('mobile/del-btn.png','',array('alt'=>'remove')),array('style'=>'float:right;'));?>
                    <div class="clear"></div>
                </div>
            </li>         
            <?php endforeach;?>
            <li><a href="" style="color:#719c1c; float:right;">More <?php echo image_asset('mobile/r-arrow.png','',array('alt'=>'more'));?></a></li>
            <?php else:?>
            <li><p>Friend list is empty.</p></li>
            <?php endif;?>
        </ul>
         <br class="clear" />


    </div>
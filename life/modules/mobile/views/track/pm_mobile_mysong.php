
    <div class="note pad520">
        <label>My Songs</label>
    </div>
    <div class="pad1020">
        <?php if(!empty($track_list)):?>
        <ul class="track">
            <?php foreach($track_list as $rtrack):?>
            <li>
                <div class="left w20"><?php echo image_asset('mobile/sid.jpg');?></div>
                <div class="left w45" style="padding:0 0 0 10px;">
                    <p style="color:#669900; text-transform:uppercase;"><?php echo $rtrack->artist_name;?></p>
                    <p style="color:#404040;"><?php echo $rtrack->title;?></p>
<!--                    <p><label style="color:#99cc33;">54 </label> <label style="color:#669900;"> Like </label><label style="padding:0 0 0 10px;"> | </label> <a href="" style="padding:0 0 0 10px;"><?php //echo image_asset('mobile/fren.png','',array('alt'=>'add'));?></a></p>-->
                </div>
                <div class="left w30">
                    <span>Total Play <?php echo $rtrack->play_count;?></span>
                </div>
                <br class="clear" />
            </li>
            <?php endforeach;?>            
        </ul>
        <?php else:?>
        <p>Your song(s) is empty.</p>
        <?php endif;?>
    </div>
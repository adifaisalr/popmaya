<div class="note pad520">
		<label class="msg left">Read <?php echo ($message->fromID !== $this->session->userdata('user_id')?'Inbox':'Sent');?> Messages</label><label class="num right"><i><?php echo $messages;?></i></label>
        <br class="clear"/>
	</div>
	<div class="pad1020" style="height:auto; background:#fafafa;">        
        <div class="chatlist">
            <?php if(@$message):?>            
            <div style="margin: 8px 0;">
                <div class="left w95">                    
                    <strong><?php echo ($message->fromID !== $this->session->userdata('user_id')?'From: ':'To: ');?><?php echo $message->up_name.' '.$message->up_lastname;?></strong> <small> (<?php echo $message->timestamp;?>)</small>
                    <p><?php echo $message->message;?></p>
                </div>
                <br class="clear"/><br class="clear"/>
                <?php if($message->fromID !== $this->session->userdata('user_id')):?>
                <?php echo anchor('mobile/message/reply/'.$message->ID,'Reply',array('style'=>'color:#444444; text-decoration:underline;'));?><br/>
                <?php endif;?>
                <?php echo anchor('mobile/message/delete/'.$message->ID,'Delete',array('style'=>'color:#444444; text-decoration:underline;'));?>
                
            </div>
            <?php else:?>
            <p style="margin: 10px;">Inbox Message is empty.</p>
            <?php endif;?>            

        </div>
        <?php //if(@$messages):?>
<!--        <div class="note">
    		<p>< 1,2,3...10 ></p>
    	</div>-->
        <?php //endif;?>     
        <div class="msgmenu"><?php echo anchor('mobile/message/inbox','Inbox',array('class'=>'inbox'));?></div>
        <div class="msgmenu"><?php echo anchor('mobile/message/compose', 'New Message', array('class'=>'newmsg'));?></div>
        <div class="msgmenu"><?php echo anchor('mobile/message/outbox', 'Sent Message', array('class'=>'newmsg'));?></div>
        <div class="msgmenu"><?php echo anchor('mobile/message/trash', 'Trash Message', array('class'=>'newmsg'));?></div>
	</div>
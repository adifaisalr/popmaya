<div class="note pad520">
		<label class="msg left">Inbox Messages</label><label class="num right"><i><?php echo $messages;?></i></label>
        <br class="clear"/>
	</div>
	<div class="pad1020" style="height:auto; background:#fafafa;">
            <div style="color: red;margin-bottom: 5px;"><?php echo $this->session->flashdata('message');?></div>
        <div class="msgmenu"><a href="" class="inbox">Inbox</a></div>
        <div class="chatlist">
            <?php if(@$messages_list):?>
            <?php $i=1; foreach($messages_list as $r_msg):?>
            <div class="<?php echo ($i%2==0)?'odd':'even';?>">
                <div class="left w95">
                    <a href="<?php echo base_url();?>mobile/message/view/<?php echo $r_msg->ID;?>" style="color:<?php echo ($r_msg->isread == 0)?'blue':'black';?>">
                    <strong><?php echo $r_msg->up_name.' '.$r_msg->up_lastname;?></strong> <small> (<?php echo $r_msg->timestamp;?>)</small>
                    <p><?php echo character_limiter($r_msg->message,50);?></p>
                    </a>
                </div>
                <div class="right w5">
                    <?php echo anchor('mobile/message/delete/'.$r_msg->ID,image_asset('mobile/del-btn.png','',array('alt'=>'delete')));?>
                </div>
                <br class="clear"/>
            </div>
            <?php $i++; endforeach;?>
            <?php else:?>
            <p style="margin: 10px;">Inbox Message is empty.</p>
            <?php endif;?>            

        </div>
        <?php //if(@$messages):?>
<!--        <div class="note">
    		<p>< 1,2,3...10 ></p>
    	</div>-->
        <?php //endif;?>     

        <div class="msgmenu"><?php echo anchor('mobile/message/compose', 'New Message', array('class'=>'newmsg'));?></div>
        <div class="msgmenu"><?php echo anchor('mobile/message/outbox', 'Sent Message', array('class'=>'newmsg'));?></div>
        <div class="msgmenu"><?php echo anchor('mobile/message/trash', 'Trash Message', array('class'=>'newmsg'));?></div>
	</div>
<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends MX_Controller {
    
    var $user_id;
    
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->user_id=$this->session->userdata('user_id');
        $this->load->model('m_member', '', TRUE);
        $this->load->model('m_wall', '', TRUE);
        $this->load->library('form_validation');
	$this->load->helper('form');
        $this->template->set_master_template('mobile');        
    }
    
    function index($id=false)
    {
        if(!$id)
        {
            $id=$this->user_id;
        }
        else
        {
            $profile=$this->m_member->getUserProfile($id);
            if(empty($profile))
            {
                redirect('mobile/home');
                exit ();
            }
            $data['submenu']=$id;
        }
            
        $posts=$this->m_wall->get_all_wall_type($id,1);
        $postsArray=array();
        if(!empty($posts))
        {
            foreach($posts as $rN)
            {
                $com=  $this->m_wall->get_count_comment($rN->ID);
                $postsArray[]=array($id,$rN->up_name,$rN->ID,$rN->title,$rN->info,$rN->like,$com);
            }
        }
        else
            $postsArray=NULL;
        
        $data['posts']=$postsArray;
        $data['image_path']=$this->m_wall->get_profile_path($id);
        $this->template->write('head_title', 'Post Lists');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'post/pm_mobile_index',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function view($id)
    {
        if($id)
        {
            $post=$this->m_wall->get_post_detail($id);
            if($post)
            {
                if($post->memberID == $this->user_id)
                {
                    $this->m_wall->update_notif_post($id);
                }
                $data['post']=$post;
                if($this->input->post('post_comment'))
                {
                    $set=array(
                        'wall_itemID'=>$id,
                        'memberID'=>$this->user_id,
                        'comment'=>$this->input->post('post_comment'),
                        'host'=>$_SERVER['REMOTE_ADDR']
                    );
                    $this->m_wall->add_comment($set);
                    if($this->user_id !== $data['post']->memberID)
                    {
                        $comset=array(
                            'n_type'=>1,
                            'n_itemid'=>$id,
                            'n_fromuserid'=>$this->user_id,
                            'n_foruserid'=>$data['post']->memberID
                        );
                        $this->m_wall->add_notif($comset);
                    }                
                }

                $data['image_path']=$this->m_member->get_profile_path($data['post']->up_uid);
                $comStatus=$this->m_member->get_comment_status($data['post']->ID);
                $dataComStatus=array();
                if(!empty($comStatus))
                {
                    foreach($comStatus as $rCom)
                    {                
                        $imgP=$this->m_member->get_profile_path($rCom->up_uid);
                        if(@$imgP)
                        {
                            $imgPath=$imgP;
                        }
                        else
                        {
                            $imgPath=NULL;
                        }
                        $dataComStatus[]=array($rCom->up_name,$rCom->comment,$imgPath."/".$rCom->up_uid.'_thumb.jpg',$rCom->up_uid,$rCom->ID);
                    }
                }
                else
                {
                    $dataComStatus=NULL;
                }
                $data['comment_status']=$dataComStatus;
                $this->template->write('head_title', 'Post');
                $this->template->write_view('middle_content', 'post/pm_mobile_post',$data);
                $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
                $this->template->render();
            }
            else
                redirect('mobile/home');
        }
        else
            redirect('mobile/home');
    }    
    
    function image()
    {
        
    }
    
    function video()
    {
        //$this->form_validation->set_rules('title', 'Friend', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|required|xss_clean');
        $this->form_validation->set_rules('desc', 'Message', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|required|xss_clean');
        
        $this->form_validation->set_message('required', '%s is required.');
        
        $this->form_validation->set_error_delimiters('', '');
        
        if($this->input->post('desc'))
        {
            if($this->form_validation->run()==TRUE)
            {
                $title=$this->input->post('title');
                $desc=$this->input->post('desc');
                $set=array(
                    'memberID'=>$this->user_id,
                    'title'=>$title,
                    'info'=>$desc,
                    'type'=>2,
                    'timestamp'=>$_SERVER['REQUEST_TIME'],
                    'host'=>$_SERVER['REMOTE_ADDR'],
                );
                $this->m_wall->add_wall($set);
                redirect('mobile/home');  
            }
        }
        $this->template->write('head_title', 'Post Video');
        $this->template->write_view('middle_content', 'post/pm_mobile_ugc');
        $this->template->write_view('profile_menu', 'pm_mobile_top');
        $this->template->render();
    }
    
    function note()
    {
        $this->form_validation->set_rules('title', 'Friend', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|required|xss_clean');
        $this->form_validation->set_rules('desc', 'Message', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|required|xss_clean');
        
        $this->form_validation->set_message('required', '%s is required.');
        
        $this->form_validation->set_error_delimiters('', '');
        
        if($this->input->post('desc'))
        {
            if($this->form_validation->run()==TRUE)
            {
                $title=$this->input->post('title');
                $desc=strip_tags($this->input->post('desc'));
                $set=array(
                    'memberID'=>$this->user_id,
                    'title'=>$title,
                    'info'=>$desc,
                    'type'=>4,
                    'timestamp'=>$_SERVER['REQUEST_TIME'],
                    'host'=>$_SERVER['REMOTE_ADDR'],
                );
                $this->m_wall->add_wall($set);
                redirect('mobile/home');  
            }
        }
        $this->template->write('head_title', 'Post Note');
        $this->template->write_view('middle_content', 'post/pm_mobile_ugc');
        $this->template->write_view('profile_menu', 'pm_mobile_top');
        $this->template->render();
    }
    
    function like($id)
    {
        if($id)
        {
            $post=$this->m_wall->get_post_detail($id);
            if($post)
            {
                $valLike=  $this->m_wall->get_like_post($this->user_id,$post->ID);
                if(empty($valLike))
                {
                    $like=$post->like + 1;
                    $likeNum=array(
                        'like'=>$like
                    );
                    $this->m_wall->update_like_post($id,$likeNum);
                    
                    $set=array(
                        'l_member_id'=>$this->user_id,
                        'l_wall_id'=>$post->ID
                    );
                    $this->m_wall->add_like_post($set);
                    
                    if($this->user_id !== $post->memberID)
                    {
                        $comset=array(
                            'n_type'=>3,
                            'n_itemid'=>$id,
                            'n_fromuserid'=>$this->user_id,
                            'n_foruserid'=>$post->memberID
                        );
                        $this->m_wall->add_notif($comset);
                    }  
                }
                
                if($post->type==2)
                {
                    redirect('mobile/video/view/'.$id, 'refresh');
                }
                elseif($post->type==4)
                {
                    redirect('mobile/note/view/'.$id, 'refresh');
                }
                else
                    redirect('mobile/post/view/'.$id, 'refresh');
            }
            else
                redirect('mobile/home');
        }
        else
            redirect('mobile/home');
    }
    
    function delete_post($id)
    {
        if($id)
        {
            $getPost=$this->m_wall->get_own_post($id);
            if(!empty($getPost))
            {
                $type=$getPost->type;
                $this->m_wall->delete_post($id);
                if($type==2)
                {
                    redirect('mobile/video', 'refresh');
                }
                elseif($type==4)
                {
                    redirect('mobile/note', 'refresh');
                }
                else
                    redirect('mobile/post', 'refresh');
            }
            else
                redirect('mobile/home');
        }
        else
            redirect('mobile/home');
    }
    
    function delete_comment($id)
    {
        if($id)
        {
            $getComment=$this->m_wall->get_own_comment($id);
            if(!empty($getComment))
            {
                $this->m_wall->delete_comment($id);
                if($getComment->type==2)
                {
                    redirect('mobile/video/view/'.$getComment->wall_itemID, 'refresh');
                }
                elseif($getComment->type==4)
                {
                    redirect('mobile/note/view/'.$getComment->wall_itemID, 'refresh');
                }
                else
                    redirect('mobile/post/view/'.$getComment->wall_itemID, 'refresh');
            }
            else
                redirect('mobile/home');
        }
        else
                redirect('mobile/home');
    }
}
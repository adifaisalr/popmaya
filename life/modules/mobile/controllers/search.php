<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends MX_Controller {
    
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->load->model('m_search', '', TRUE);
    }
    
    function index()
    {
        if($this->input->post('serach_q'))
        {
            $key=$this->input->post('serach_q');
            
            /*
             * serach artist
             */
            $artistResults=  $this->m_search->get_artist_result($key);
            
            /*
             * serach friend
             */
            $friendResults=$this->m_search->get_friend_result($key);
            
            /*
             * serach song
             */
            $songResults=$this->m_search->get_song_result($key);            
        }
        $data['artistResults']=$artistResults;
        $data['friendResults']=$friendResults;
        $data['songResults']=$songResults;
        $this->template->write('head_title', 'Search Results');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'search/pm_mobile_search',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
}
<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {
    
    function __construct() {
        parent::__construct();
        if ($this->ion_auth->logged_in()){
            redirect('mobile/home');
            exit();
        } 
        $this->template->set_master_template('mobile');
        $this->load->library('form_validation');
	$this->load->helper('form');
        
    }
    
    function index()
    {
        $fb_appid=$this->config->item('facebook_app_id');
        $fb_secret=$this->config->item('facebook_app_secret');
        $this->form_validation->set_rules('email', 'Email', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|required|valid_email|min_length[5]|max_length[50]|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|alpha_numeric|prep_for_form|htmlspecialchars|encode_php_tags|required|min_length[5]|max_length[20]|xss_clean');
		
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
    	$this->form_validation->set_message('alpha_numeric', 'Only alphanumeric characters are allowed.');
	$this->form_validation->set_message('valid_email', 'Wrong email format.');
    	$this->form_validation->set_message('min_length[5]', 'Min. character is 5 chars.');    	
    	$this->form_validation->set_message('max_length[20]', 'Max. character is 20 chars.');
    	$this->form_validation->set_message('max_length[50]', 'Max. character is 50 chars.');
        
        $this->form_validation->set_error_delimiters('', '');
        
        if($this->form_validation->run()==TRUE)
        {
            if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password')))
            {
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("mobile/home", 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
            }
        }        
        
        $data['fb_appid']=$fb_appid;
	$data['fb_secret']=$fb_secret;
        
        $this->template->write('head_title', 'Login');
        $this->template->write_view('middle_content', 'pm_mobile_login',$data);
        $this->template->render();
    }
    
}
/* End of file login.php */
/* Location: ./life/modules/mobile/controllers/login.php */
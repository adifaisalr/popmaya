<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MX_Controller {
    
    function __construct() {
        parent::__construct();
        if ($this->ion_auth->logged_in()){
            redirect('mobile/home');
            exit();
        }  
        $this->load->model('m_member', '', TRUE);
        $this->load->library('form_validation');
    }
    
    function index()
    {
        $this->form_validation->set_rules('fullname', 'Fullname', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean|min_length[5]|max_length[20]');
    	$this->form_validation->set_rules('email', 'Email', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|valid_email|callback_new_email|min_length[5]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|alpha_numeric|required|prep_for_form|htmlspecialchars|encode_php_tags|min_length[5]|max_length[20]|xss_clean');
    	
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
    	$this->form_validation->set_message('alpha_numeric', 'Only alphanumeric characters are allowed.');
		$this->form_validation->set_message('valid_email', 'Wrong email format.');
    	$this->form_validation->set_message('min_length[5]', 'Min. character is 5 chars.');    	
    	$this->form_validation->set_message('max_length[20]', 'Max. character is 20 chars.');
    	$this->form_validation->set_message('max_length[50]', 'Max. character is 50 chars.');
		
		$this->form_validation->set_error_delimiters('','');

		
		if ($this->form_validation->run() == true)
		{
			if($this->input->post('agree')=="agreed")
			{			
				$fullname = $this->input->post('fullname');
				$email = $this->input->post('email');
				$password = $this->input->post('password');

				$additional_data = array('gender' => $this->input->post('gender'),
					'update' => $this->input->post('update')
				);
			
			
				if($this->ion_auth->register($fullname, $password, $email, $additional_data))
				{
					//check to see if we are creating the user
					$this->session->set_flashdata('register_success', "Your account has been created");
					redirect('mobile/register', 'refresh');
				}
				else
				{ 
					//display the register user form
					$this->session->set_flashdata('register_error', $this->ion_auth->errors());
					redirect('mobile/register', 'refresh'); 
				}
			}
			else
			{
				$this->session->set_flashdata('register_error', "You must agree with the Term & Service");
				redirect('mobile/register', 'refresh'); 				
			}
		}
//		else
//		{
//			$this->session->set_flashdata('register_error', validation_errors());
//			redirect('home');			
//		}
        $this->template->write('head_title', 'Register');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'pm_mobile_register');
        $this->template->render();
    }
}
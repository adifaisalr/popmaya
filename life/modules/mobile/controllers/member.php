<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends MX_Controller {
    
    var $user_id;
    
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        }        
        $this->user_id=$this->session->userdata('user_id');
        $this->load->model('m_member', '', TRUE);
    }
    
    function profile($id=false)
    {
        if(!$id)
        {
            $id=$this->user_id;
        }
            
        $data['active_menu']='profile';
        $data['friend']=$this->m_member->get_count_friend($id);
        $data['message']=$this->m_member->get_count_unread_messages();
        $data['notes']=$this->m_member->get_count_post_type($id,4);
        $data['video']=$this->m_member->get_count_post_type($id,2);
        $data['photo']=$this->m_member->get_count_photos($id);
        $data['song']=$this->m_member->get_count_songs($id);
        $this->template->write('head_title', 'Profile');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'member/pm_mobile_profile',$data);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function profile_info()
    {
        $data['active_menu']='profile';
        $data['profile'] = $this->m_member->getUserProfile($this->user_id);
        $this->template->write('head_title', 'Profile Info');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'member/pm_mobile_profileinfo',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function logout()
    {
        $logout = $this->ion_auth->logout();
        $this->session->set_flashdata('message', "Logout success");
        redirect('mobile/login', 'refresh');
    }
    
}
/* End of file member.php */
/* Location: ./life/modules/mobile/controllers/member.php */
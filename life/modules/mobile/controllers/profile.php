<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MX_Controller {
    
    var $user_id;
    
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->user_id=$this->session->userdata('user_id');
        $this->load->model('m_member', '', TRUE);
        $this->template->set_master_template('mobile');
    }
    
    function index($id=false)
    {
        if(!$id)
        {
            $id=$this->user_id;
        }
        else
        {
            $profile=$this->m_member->getUserProfile($id);
            if(empty($profile))
            {
                redirect('mobile/home');
                exit ();
            }
            $data['submenu']=$id;
        }
        $data['profile'] =$profile;
        $data['image_path']=$this->m_member->get_profile_path($profile->up_uid);
        $this->template->write('head_title', 'Profile Info');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'profile/pm_mobile_index',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();        
    }
    
}
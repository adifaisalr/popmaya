<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Photo extends MX_Controller {
    
    var $user_id;
    
    function __construct() 
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->user_id=$this->session->userdata('user_id');
        $this->load->model('m_member', '', TRUE);
        $this->load->model('m_photos', '', TRUE);
    }
    
    function index()
    {
        $data['active_menu']='profile';
        $data['album'] = $this->m_photos->get_count_album($this->user_id);
        $albums = $this->m_photos->get_user_album($this->user_id);
        $dataAlbums=array();
        if(!empty($albums))
        {
            foreach($albums as $row)
            {
                $cover=$this->m_photos->get_photo_cover($this->user_id,$row->ID);
                $count= $this->m_photos->get_count_photo_album($this->user_id,$row->ID);
                $dataAlbums[]=array($row->ID,$row->title,$cover,$count);
            }
        }
        else
        {
            $dataAlbums=NULL;
        }
        $imgP=$this->m_member->get_profile_path('19450923');
                if(@$imgP)
                {
                    $imgPath=$imgP;
                }
                else
                {
                    $imgPath=NULL;
                }
        $data['path']=$imgP;
        $data['albums'] = $dataAlbums;
        $this->template->write('head_title', 'Photo Album');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'photo/pm_mobile_photo',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function album_previews($id)
    {
        if($id)
        {
            
        }
        else
        {
            redirect('mobile/photo');
            exit();
        }
        $data['active_menu']='profile';
        $data['profile'] = $this->m_photos->getUserProfile($this->user_id);
        $this->template->write('head_title', 'Photo Album Preview');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'photo/pm_mobile_albumprev',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function upload_photo()
    {
        $data['active_menu']='profile';
        $data['profile'] = $this->m_photos->getUserProfile($this->user_id);
        $this->template->write('head_title', 'Upload Photo');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'photo/pm_mobile_uploadphoto',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
}
/* End of file photo.php */
/* Location: ./life/modules/mobile/controllers/photo.php */
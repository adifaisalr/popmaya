<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Message extends MX_Controller {
    
    var $user_id;
    
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->user_id=$this->session->userdata('user_id');
        $this->load->model('m_message', '', TRUE);
        $this->template->set_master_template('mobile');
        $this->load->library('form_validation');
	$this->load->helper('form');
    }
    
    function inbox()
    {
        $data['active_menu']='profile';
        $data['messages']=$this->m_message->get_count_messages();
        $data['messages_list']=$this->m_message->get_messages_list();
        $this->template->write('head_title', 'Inbox Message');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'message/pm_mobile_messages',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function outbox()
    {
        $data['active_menu']='profile';
        $data['messages'] = $this->m_message->get_count_sent_messages();
        $data['messages_list']=$this->m_message->get_sent_messages_list();
        $this->template->write('head_title', 'Outbox Message');
        $this->template->write_view('middle_content', 'message/pm_mobile_sentmsg',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function trash()
    {
        $data['active_menu']='profile';
        $data['messages'] = $this->m_message->get_count_trash_messages();
        $data['messages_list']=$this->m_message->get_trash_messages_list();
        $this->template->write('head_title', 'Trash Message');
        $this->template->write_view('middle_content', 'message/pm_mobile_trashmsg',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function compose()
    {
        $this->form_validation->set_rules('friend', 'Friend', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|required|xss_clean');
        $this->form_validation->set_rules('message', 'Message', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|required|xss_clean');
        
        $this->form_validation->set_message('required', '%s is required.');
        
        $this->form_validation->set_error_delimiters('', '');
        
        $data['active_menu']='profile';
        $data['friends'] = $this->m_message->get_all_friends();
        
        if($this->input->post('message'))
        {
            if($this->form_validation->run()==TRUE)
            {
                $message=array(
                    'message'=>  strip_tags($this->input->post('message'))
                );
                $this->m_message->add_message($message,$this->input->post('friend'));

                    $this->session->set_flashdata('message', 'Your message has been sent.');

                redirect('mobile/message/inbox');      
            }
        }
        $this->template->write('head_title', 'Compose Message');
        $this->template->write_view('middle_content', 'message/pm_mobile_composemsg',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function view($id)
    {
        if($id)
        {
            $cekMsg=$this->m_message->get_detail($id);
            if(!empty($cekMsg))
            {
                if($cekMsg->toID === $this->user_id)
                {
                    $msg=$this->m_message->get_detail_in($id);
                    if($msg->toID == $this->user_id)
                    {
                        $this->m_message->update_notif($id);
                    }
                    if($msg->isread==0)
                    {
                        $set=array(
                            'isread'=>1
                        );
                        $this->m_message->update_read_message($id,$set);
                    }
                }
                elseif($cekMsg->fromID === $this->user_id)
                {
                    $msg=$this->m_message->get_detail_out($id);
                }      
                else
                {
                    redirect('mobile/message/inbox');
                    exit();
                }
                $data['active_menu']='profile';
                $data['messages']=$this->m_message->get_count_messages();
                $data['message']=$msg;
                $this->template->write('head_title', 'Read Message');
                $this->template->write_view('middle_content', 'message/pm_mobile_viewmessage',$data,true);
                $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
                $this->template->render();
            }
            else
                redirect('mobile/message/inbox');
        }
        else
            redirect('mobile/message/inbox');
    }
    
    function reply($id)
    {
        if($id)
        {
            $msg=$this->m_message->get_detail_in($id);
            if(!empty($msg))
            {
                if($this->input->post('message'))
                {
                    $message=array(
                        'message'=>  strip_tags($this->input->post('message'))
                    );
                    $this->m_message->add_reply($message,$this->input->post('friend_id'),$id);
                    $this->session->set_flashdata('message', 'Your reply message has been sent.');

                    redirect('mobile/message/inbox');  
                }
                $data['active_menu']='profile';
                $data['messages']=$this->m_message->get_count_messages();
                $data['message']=$msg;
                $this->template->write('head_title', 'Reply Message');
                $this->template->write_view('middle_content', 'message/pm_mobile_replymessage',$data,true);
                $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
                $this->template->render();
            }
            else
                redirect('mobile/message/inbox');
        }
        else
            redirect('mobile/message/inbox');
    }
    
    function delete($id)
    {
        if($id)
        {
            $cekMsg=$this->m_message->get_detail($id);
            if(!empty($cekMsg))
            {
                if($cekMsg->toID === $this->user_id)
                {                    
                    if($cekMsg->isTrash === '1')
                    {
                        $this->m_message->delete_trash($id);
                        redirect('mobile/message/trash','refresh');
                    }
                    else
                    {
                        $this->m_message->delete_msg_in($id);
                        redirect('mobile/message/inbox','refresh');
                    }
                }
                elseif($cekMsg->fromID === $this->user_id)
                {
                    $this->m_message->delete_msg_out($id);
                    redirect('mobile/message/outbox','refresh');
                }
                else
                    redirect('mobile/message/inbox');
            }
        }
        else
            redirect('mobile/message/inbox');
    }
}
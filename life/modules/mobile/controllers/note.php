<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Note extends MX_Controller {
    
    var $user_id;
    
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->user_id=$this->session->userdata('user_id');
        $this->load->model('m_wall', '', TRUE);
        $this->template->set_master_template('mobile');
    }
    
    function index($id=false)
    {
        if(!$id)
        {
            $id=$this->user_id;
        }
        else
        {
            $profile=$this->m_wall->getUserProfile($id);
            if(empty($profile))
            {
                redirect('mobile/home');
                exit ();
            }
            $data['submenu']=$id;
        }
        
        $notes=$this->m_wall->get_all_wall_type($id,4);
        $notesArray=array();
        if(!empty($notes))
        {
            foreach($notes as $rN)
            {
                $com=  $this->m_wall->get_count_comment($rN->ID);
                $notesArray[]=array($id,$rN->up_name,$rN->ID,$rN->title,$rN->info,$rN->like,$com);
            }
        }
        else
            $notesArray=NULL;
        
        $data['notes']=$notesArray;
        $data['image_path']=$this->m_wall->get_profile_path($id);
        $this->template->write('head_title', 'Note Lists');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'note/pm_mobile_index',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function view($id)
    {
        if($id)
        {
            $post=$this->m_wall->get_post_detail($id);
            if($post)
            {
                if($post->memberID == $this->user_id)
                {
                    $this->m_wall->update_notif_post($id);
                }
                $data['post']=$post;
                if($this->input->post('post_comment'))
                {
                    $set=array(
                        'wall_itemID'=>$id,
                        'memberID'=>$this->user_id,
                        'comment'=>$this->input->post('post_comment'),
                        'host'=>$_SERVER['REMOTE_ADDR']
                    );
                    $this->m_wall->add_comment($set);
                    if($this->user_id !== $data['post']->memberID)
                    {
                        $comset=array(
                            'n_type'=>1,
                            'n_itemid'=>$id,
                            'n_fromuserid'=>$this->user_id,
                            'n_foruserid'=>$data['post']->memberID
                        );
                        $this->m_wall->add_notif($comset);
                    }                
                }

                $data['image_path']=$this->m_wall->get_profile_path($data['post']->up_uid);
                $comStatus=$this->m_wall->get_comment_status($data['post']->ID);
                $dataComStatus=array();
                if(!empty($comStatus))
                {
                    foreach($comStatus as $rCom)
                    {                
                        $imgP=$this->m_wall->get_profile_path($rCom->up_uid);
                        if(@$imgP)
                        {
                            $imgPath=$imgP;
                        }
                        else
                        {
                            $imgPath=NULL;
                        }
                        $dataComStatus[]=array($rCom->up_name,$rCom->comment,$imgPath."/".$rCom->up_uid.'_thumb.jpg',$rCom->up_uid,$rCom->ID);
                    }
                }
                else
                {
                    $dataComStatus=NULL;
                }
                $data['comment_status']=$dataComStatus;
                $this->template->write('head_title', 'Note');
                $this->template->write_view('middle_content', 'note/pm_mobile_post',$data);
                $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
                $this->template->render();
            }
            else
                redirect('mobile/note');
        }
        else
            redirect('mobile/note');
    }
}
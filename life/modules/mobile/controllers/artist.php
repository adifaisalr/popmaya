<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Artist extends MX_Controller {
    
    function __construct() 
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->load->library('form_validation');
        $this->load->model('m_artist', '', TRUE);
    }
    
    function index()
    {
        $data['active_menu']='artist';
        $data['genre']=$this->m_artist->get_genre_list();
        $data['artist_list']=$this->m_artist->get_artist_list();
        $this->template->set_master_template('mobile');
        $this->template->write('head_title', 'Artist');
        $this->template->write_view('middle_content', 'artist/pm_mobile_index',$data, TRUE);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function profile($id)
    {
        if($id)
        {
            $profile=$this->m_artist->get_detail_artist($id);
            if(!empty($profile))
            {
                $data['profile']=$profile;
            }
            else
                redirect('mobile/artist');
        }
        else
        {
            redirect('mobile/artist');
            exit();
        }
        
        //$status=$this->m_artist->get_new_status($id);
        
        
        $data['active_menu']='artist';
        $this->template->set_master_template('mobile');
        $this->template->write('head_title', 'Artist');
        $this->template->write_view('middle_content', 'artist/pm_mobile_profile',$data, TRUE);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function search($letter=false)
    {
        if($letter)
        {
            $dataArtists=array();
            $artists=$this->m_artist->get_all_artists_by($letter);            
            if(!empty($artists))
            {
                foreach($artists as $rArtist)
                {
                    $dataArtists[]=array($rArtist->ID,$rArtist->artist_name,$rArtist->g_name);
                }
                $data['key']=$letter;
            }
            else
            {
                $dataArtists=NULL;
                $data['key']=$letter;
            }
        }
        elseif($this->input->post('artist_q'))
        {
            $key=strip_tags($this->input->post('artist_q'));
            $gen=$this->input->post('genre');
            if($gen)
            {
                foreach($gen as $rGenre)
                {
                    $genre=array($rGenre);
                }
            }
            else
            {
                $genre=NULL;
            }
            $artists=$this->m_artist->get_all_artists_search($key,$genre);   
            if(!empty($artists))
            {
                foreach($artists as $rArtist)
                {
                    $dataArtists[]=array($rArtist->ID,$rArtist->artist_name,$rArtist->g_name);
                }
                $data['key']=$key;
            }
            else
            {
                $dataArtists=NULL;
                $data['key']=$key;
            }
        }
        else
        {
            redirect('mobile/artist/');
            exit();
        }
        $data['active_menu']='artist';
        $data['genre']=$this->m_artist->get_genre_list();
        $data['artist_list']=$dataArtists;
        $this->template->write('head_title', 'Search Artist');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'artist/pm_mobile_artistsearch',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
}
/* End of file artist.php */
/* Location: ./life/modules/mobile/controllers/artist.php */
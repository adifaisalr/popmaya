<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Track extends MX_Controller {
    
    var $user_id;
    
    function __construct() 
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->user_id=$this->session->userdata('user_id');
        $this->load->model('m_artist', '', TRUE);
        $this->load->model('m_track', '', TRUE);
    }
    
    function index()
    {
        $data['active_menu']='track';
        $data['genre']=$this->m_artist->get_genre_list();
        $data['track_list']=$this->m_track->get_track_list();
        $this->template->set_master_template('mobile');
        $this->template->write('head_title', 'Track List');
        $this->template->write_view('middle_content', 'track/pm_mobile_tracklist',$data,TRUE);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function mysongs()
    {
        $data['active_menu']='profile';
        $data['genre']=$this->m_artist->get_genre_list();
        $data['track_list']=$this->m_track->get_mysong_list($this->user_id);
        $this->template->set_master_template('mobile');
        $this->template->write('head_title', 'Track List');
        $this->template->write_view('middle_content', 'track/pm_mobile_mysong',$data,TRUE);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function search($letter=false)
    {
        $dataTrack=array();
        if($letter)
        {
            $track=$this->m_track->get_all_track_by($letter);            
            if(!empty($track))
            {
                foreach($track as $rTrack)
                {
                    $dataTrack[]=array($rTrack->artist_name,$rTrack->title,$rTrack->play_count);
                }
                $data['key']=$letter;
            }
            else
            {
                $dataTrack=NULL;
                $data['key']=$letter;
            }
        }
        elseif($this->input->post('track_q'))
        {
            $key=strip_tags($this->input->post('track_q'));
            $gen=$this->input->post('genre');
            if($gen)
            {
                foreach($gen as $rGenre)
                {
                    $genre=array($rGenre);
                }
            }
            else
            {
                $genre=NULL;
            }
            $track=$this->m_track->get_all_track_search($key,$genre);            
            if(!empty($track))
            {
                foreach($track as $rTrack)
                {
                    $dataTrack[]=array($rTrack->artist_name,$rTrack->title,$rTrack->play_count);
                }
                $data['key']=$key;
            }
            else
            {
                $dataTrack=NULL;
                $data['key']=$key;
            }
        }
        else
        {
            redirect('mobile/track/');
            exit();
        }
        $data['active_menu']='track';
        $data['genre']=$this->m_artist->get_genre_list();
        $data['tracks']=$dataTrack;
        $this->template->write('head_title', 'Search Track');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'track/pm_mobile_tracksearch',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
}
/* End of file track.php */
/* Location: ./life/modules/mobile/controllers/track.php */
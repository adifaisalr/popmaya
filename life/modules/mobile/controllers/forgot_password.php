<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Forgot_Password extends MX_Controller {
    
    function __construct() {
        parent::__construct();
        if ($this->ion_auth->logged_in()){
            redirect('mobile/home');
            exit();
        }  
        $this->load->model('m_member', '', TRUE);
        $this->load->library('form_validation');
    }
    
    function index()
    {
        $this->form_validation->set_rules('email', 'Email', 'xss_clean|required|valid_email|min_length[5]|max_length[50]');
		
		if ($this->form_validation->run() == true)
		{
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));					
			
			if ($forgotten)  //if there were no errors
			{
				$this->session->set_flashdata('message_confirm', $this->ion_auth->messages());
				redirect('mobile/forgot_password'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message_error', $this->ion_auth->errors());
				redirect('mobile/forgot_password', 'refresh');
			}			
		}
		else
		{
			//no email entered
			$this->session->set_flashdata('message', validation_errors());
			//redirect('mobile/forgot_password');			
		}
                
                $this->template->write('head_title', 'Forgot Password');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'pm_mobile_forgot');
        $this->template->render();
    }
}
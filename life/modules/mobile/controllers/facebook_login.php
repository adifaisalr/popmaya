<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Facebook_Login extends MX_Controller {
    
    function __construct() {
        parent::__construct();
        $this->config->load('my_config');
        
    }

    function index()
	{
		parse_str($_SERVER['QUERY_STRING'],$_GET);

		if(isset($_GET['code'])) 
		{
			$this->load->library("curl");
			
			$this->config->load('my_config');
			$fb_appid=$this->config->item('facebook_app_id');
			$fb_secret=$this->config->item('facebook_app_secret');

			// buat url untuk mengambil token
			$url = 'https://graph.facebook.com/oauth/access_token?client_id='.$fb_appid.'&redirect_uri='.base_url()."member/facebook_login".'&client_secret='.$fb_secret.'&code='.$_GET['code'];

			// ambil token lewat curl
			$token_data = $this->curl->simple_get($url);

			// ambil kode token saja, dengan regular expression
			// arti tanda ([^&]+) adalah:
			// ambil semua karakter asal bukan tanda &
			preg_match("/access_token=([^&]+)/",$token_data,$token);

			// kode token ada di variabel token[1]
			$access_token = $token[1];

			// pengambilan token selesai, sekarang ambil userid, nama
			$uri = 'https://graph.facebook.com/me?access_token='.$access_token;
			$data = $this->curl->simple_get($uri);

			// decode data
			$fb = json_decode($data);
			$fb_id = $fb->id;

			// ambil nama dan foto pengguna
			$fb_userdata = $this->curl->simple_get("https://graph.facebook.com/".$fb_id."?fields=name,picture,gender,email,id&access_token=".$access_token);

			//echo "https://graph.facebook.com/".$fb_id."?fields=name,picture&access_token=".$access_token;

			$fb_user = json_decode($fb_userdata);

			$data = array();
			//$data['fbuser'] = array('id'=>$fb_id, 'avatar'=> $fb_user->picture, 'nama'=> $fb_user->name, 'gender'=> $fb_user->gender, 'id'=> $fb_user->id, 'email'=> $fb_user->email);

			$name = $fb_user->name;
			$email = $fb_user->email;
			$fbid = $fb_user->id;
			$gender = strtoupper(substr($fb_user->gender,0,1));
			$pwd = '';

			$uid = $this->m_member->fb_id_exists($fbid);
			if($uid == FALSE) 
			{
				$this->save_register($name,$email,$pwd,$gender,1,$fbid,urlencode($fb_user->picture),'','');
				$this->session->set_userdata('fbwall',$name);
			} 
			else 
			{
				$this->session->set_userdata('uloginid', $uid);

				$ipaddress	= $this->input->ip_address();
				$browser	= $this->input->user_agent();
				$location 	= $this->m_member->get_geolocation($ipaddress);

				$this->m_front->add_session($uid,$ipaddress,$browser,$location,$session);
				$this->session->set_userdata('ulogin_id_session',$session);

			}
					redirect('member/profile', 'refresh');
		}
		elseif(isset($_GET['error_reason'])) 
		{
			// untuk menangkap user yang klik "Dont Allow" atau "Cancel di Facebook"
			// buat variabel untuk ditampilkan di view
			//$data['tolak'] = "Uh oh, saya ditolak T_T";
			$this->index();
		}

	}
        
        function save_register($name,$email,$pwd,$gender,$sendupdate,$fbid='',$picfb='',$twid='',$twpic='') 
	{
		$twimg 		= urldecode($twpic);
        $ipaddress	= $this->input->ip_address();
        $location 	= $this->m_member->get_geolocation($ipaddress);
        $browser	= $this->input->user_agent();

        $this->load->library('randomized');
        $vericode = $this->randomized->random_generator(10);
		
        $uid = $this->m_member->save_new_member($name,$email,$pwd,$gender,$sendupdate,$location,$vericode,$fbid,$twid);
        $this->session->set_userdata('uloginid', $uid);
        $this->m_member->add_session($uid,$ipaddress,$browser,$location,$session);
        $this->send_mail($name,$email,$vericode);
        $this->session->set_userdata('ulogin_id_session',$session);
        if($fbid != '') {
                $this->save_pic_fb($uid,$fbid,$picfb);
        }
        if($twimg != '') {
                $this->save_pic_fb($uid,$twid,$twimg);
        }

    }	
}
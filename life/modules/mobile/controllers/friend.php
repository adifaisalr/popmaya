<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Friend extends MX_Controller {
    
    var $user_id;
    
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->user_id=$this->session->userdata('user_id');
        $this->load->model('m_friends', '', TRUE);
        $this->template->set_master_template('mobile');
    }
    
    function index()
    {
        $data['active_menu']='profile';
        $friends=$this->m_friends->get_all_friends();
        if(!empty($friends))
        {
            foreach($friends as $rFriend)
            {
                $imgP=$this->m_friends->get_profile_path($rFriend->f_friendid);
                if(@$imgP)
                {
                    $imgPath=$imgP;
                }
                else
                {
                    $imgPath=NULL;
                }
                $dataFriends[]=array($rFriend->f_friendid,$rFriend->up_name,$rFriend->up_city,$imgPath."/".$rFriend->f_friendid.'_thumb.jpg');
            }
        }
        else
        {
            $dataFriends=NULL;
        }
        $data['friends']=$dataFriends;
        $this->template->write('head_title', 'Friends List');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'friend/pm_mobile_friends',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function search($letter=false)
    {
        if($letter)
        {
            $friends=$this->m_friends->get_all_friend_by($letter);
            if(!empty($friends))
            {
                foreach($friends as $rFriend)
                {
                    $imgP=$this->m_friends->get_profile_path($rFriend->f_friendid);
                    if(@$imgP)
                    {
                        $imgPath=$imgP;
                    }
                    else
                    {
                        $imgPath=NULL;
                    }
                    $dataFriends[]=array($rFriend->f_friendid,$rFriend->up_name,$rFriend->up_city,$imgPath."/".$rFriend->f_friendid.'_thumb.jpg');
                }
                $data['key']=$letter;
            }
            else
            {
                $dataFriends=NULL;
                $data['key']=$letter;
            }
        }
        elseif($this->input->post('keyword_friend'))
        {
            $key=strip_tags($this->input->post('keyword_friend'));
            $friends=$this->m_friends->get_all_friend_search($key);
            if(!empty($friends))
            {
                foreach($friends as $rFriend)
                {
                    $imgP=$this->m_friends->get_profile_path($rFriend->f_friendid);
                    if(@$imgP)
                    {
                        $imgPath=$imgP;
                    }
                    else
                    {
                        $imgPath=NULL;
                    }
                    $dataFriends[]=array($rFriend->f_friendid,$rFriend->up_name,$rFriend->up_city,$imgPath."/".$rFriend->f_friendid.'_thumb.jpg');
                }
                $data['key']=$key;
            }
            else
            {
                $dataFriends=NULL;
                $data['key']=$key;
            }
        }
        else
        {
            redirect('mobile/friend/');
            exit();
        }
        
        $data['friends']=$dataFriends;
        $this->template->write('head_title', 'Friends List');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'friend/pm_mobile_search',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function add_friend($id)
    {
        if($id)
        {
            $this->m_friends->add_request_friend($id);
            redirect('mobile/home');
        }
        else
            redirect('mobile/home');
    }
    
    function request($id)
    {
        if($id)
        {
            $filter=$this->m_friends->get_friend_request($id);
            if($filter === NULL)
            {
                redirect('mobile/notification');
            }
            $data['request']=$filter;
            $data['image_path']=$this->m_friends->get_profile_path($filter->up_uid);
            $this->template->write('head_title', 'Friend Request');
            $this->template->set_master_template('mobile');
            $this->template->write_view('middle_content', 'friend/pm_mobile_request',$data,true);
            $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
            $this->template->render();
        }
        else
        {
            redirect('mobile/notification');
        }
    }
    
    function approved($id)
    {
        if($id)
        {
            $filter=$this->m_friends->get_friend_request($id);
            if($filter === NULL)
            {
                redirect('mobile/notification');
            }
            else
            {
                $set=array(
                    'f_userid'=>$this->user_id,
                    'f_friendid'=>$filter->i_userid,
                    'f_isactive'=>1
                );
               $this->m_friends->add_approve_friend($set,$filter->i_userid,$filter->n_id,$filter->i_id);
                        redirect('mobile/notification');
            }
        }
        else
        {
            redirect('mobile/notification');
        }
    }
    
    function rejected($id)
    {
        if($id)
        {
            $filter=$this->m_friends->get_friend_request($id);
            if($filter !== NULL)
            {
                $this->m_friends->rejected_friend_request($id,$filter->n_id);                
            }
        }
        else
        {
            redirect('mobile/notification');
        }
        redirect('mobile/notification');
    }
}
<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Mobile extends MX_Controller {
    
    function __construct() 
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
        } 
        else
             redirect('mobile/home');
    }
    
    
}
/* End of file mobile.php */
/* Location: ./life/modules/mobile/controllers/mobile.php  */
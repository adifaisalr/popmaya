<?php
class M_Photos extends CI_Model 
{
        function getUserProfile($id)
	{
		$query=$this->db->select('tr_user_profile.*, tr_user_login.ul_email')
				->from('tr_user_profile')
				->join('tr_user_login', 'ul_id=up_uid')
				->where('up_uid', $id)
				->get();
		return $query->row();
	}    
        
        function get_profile_path($id)
	{
		$query=$this->db->get_where('tr_user_login', array('ul_id'=>$id));
		$row=$query->row();
		$row=substr($row->ul_createdon, 0, 10);
		$path=str_replace('-', '/', $row);
		return $path;
	}
        
        function get_count_album($id)
	{
		$count=$this->db->from('tr_album')
			->where('memberID', $id)
			->count_all_results();		
		return $count;
	}
        
        function get_user_album($id)
        {
            $query=$this->db->select('*')
                            ->from('tr_album')
                            ->where('memberID', $id)
                            ->get();
            return $query->result();
        }
        
        function get_photo_cover($user,$albID)
        {
            $query=$this->db->select('image')
                            ->from('tr_photo')
                            ->where('memberID',$user)
                            ->where('albumID',$albID)
                            ->get();
            return $query->row();
        }
        
        function get_count_photo_album($user,$albID)
	{
		$count=$this->db->from('tr_photo')
			->where('memberID', $user)
                        ->where('albumID',$albID)
			->count_all_results();		
		return $count;
	}
}
<?php
class M_Chat extends CI_Model 
{
    //protected $table        = 'tr_genre';
    
        function add_chat($post)
        {
             $this->db->insert('tr_chat', $post);
        }
        
        function get_detail($id)
        {
            $query=$this->db->select('*')
                        ->from('tr_chat_room')
                        ->where('cr_id',$id)
                        ->get();
            return $query->row();
        }

        function get_room_list()
        {
            $query=$this->db->select('*')
                        ->from('tr_chat_room')
                        ->where('cr_status',1)
                        ->order_by('cr_title','asc')
                        ->get();
            return $query->result();
        }
        
        function get_chat_list($id)
        {
            $query=$this->db->select('tr_chat.c_mssg, tr_user_profile.up_name, tr_user_profile.up_lastname')
                        ->from('tr_chat')
                        ->join('tr_chat_room','cr_id=tr_chat.c_room_id')
                        ->join('tr_user_profile', 'up_uid=tr_chat.c_sender_id')
                        ->where('tr_chat_room.cr_id',$id)
                        ->where('tr_chat.c_status',1)
                        ->order_by('tr_chat.c_created_on','desc')
                        ->get();
            return $query->result();
        }
}
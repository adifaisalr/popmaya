<?php
class M_Message extends CI_Model 
{
        var $id;
        
        function get_profile_path($id)
	{
		$query=$this->db->get_where('tr_user_login', array('ul_id'=>$id));
		$row=$query->row();
		$row=substr($row->ul_createdon, 0, 10);
		$path=str_replace('-', '/', $row);
		return $path;
	}
        
        function get_all_friends()
        {
            $query=$this->db->select('tr_friend.*, tr_user_profile.up_name, tr_user_profile.up_city')
			->from('tr_friend')
                        ->join('tr_user_profile', 'up_uid=f_friendid')
			->where('tr_friend.f_userid', $this->session->userdata('user_id'))
                        ->where('tr_friend.f_isactive',1)
                        ->get();
            return $query->result();
        }
        
        function update_notif($id)
        {
            $notif=array(
                    'n_isread'=>1
                );
            $this->db->where('n_itemid', $id);
            $this->db->where('n_foruserid',$this->session->userdata('user_id'));
            $this->db->where('n_type', 9);
                    $this->db->update('tr_notification', $notif);
        }
        
        /*
         * message's model
         */
        function add_message_content($array)
        {
            $this->db->insert('tr_message_content',$array);
            return mysql_insert_id();
        }
        
        function add_message($array,$toid)
        {
            $messsage=$this->add_message_content($array);
            if($messsage)
            {
                $set=array(
                    'fromID'=>$this->session->userdata('user_id'),
                    'toID'=>$toid,
                    'message_contentID'=>$messsage
                );
                $this->db->insert('tr_message',$set);
                
                $comset=array(
                            'n_type'=>9,
                            'n_itemid'=>$messsage,
                            'n_fromuserid'=>$this->session->userdata('user_id'),
                            'n_foruserid'=>$toid
                        );
                $this->db->insert('tr_notification', $comset);
            }
        }
        
        function add_reply($array,$toid,$id)
        {
            $messsage=$this->add_message_content($array);
            if($messsage)
            {
                $set=array(
                    'parentID'=>$id,
                    'fromID'=>$this->session->userdata('user_id'),
                    'toID'=>$toid,
                    'message_contentID'=>$messsage
                );
                $this->db->insert('tr_message',$set);
                
                $comset=array(
                            'n_type'=>9,
                            'n_itemid'=>$messsage,
                            'n_fromuserid'=>$this->session->userdata('user_id'),
                            'n_foruserid'=>$toid
                        );
                $this->db->insert('tr_notification', $comset);
            }
        }
        
        function get_detail($id)
        {
            $query=$this->db->select('*')
                            ->from('tr_message')
                            ->where('toID',$this->session->userdata('user_id'))
                            ->or_where('fromID',$this->session->userdata('user_id'))
                            ->where('ID',$id)
                            ->where('type','pm')
                            ->get();
            return $query->row();
        }
        
        function get_detail_in($id)
        {
            $query=$this->db->select('tr_message.*, tr_message_content.message, tr_user_profile.up_name, tr_user_profile.up_lastname')
                            ->from('tr_message')
                            ->join('tr_message_content','tr_message_content.ID=tr_message.message_contentID')
                            ->join('tr_user_profile', 'up_uid=tr_message.fromID')
                            ->where('tr_message.toID',$this->session->userdata('user_id'))
                            ->where('tr_message.ID',$id)
                            ->where('tr_message.hideto',0)
                            ->where('tr_message.type','pm')
                            ->get();
            return $query->row();
        }
        
        function get_detail_out($id)
        {
            $query=$this->db->select('tr_message.*, tr_message_content.message, tr_user_profile.up_name, tr_user_profile.up_lastname')
                            ->from('tr_message')
                            ->join('tr_message_content','tr_message_content.ID=tr_message.message_contentID')
                            ->join('tr_user_profile', 'up_uid=tr_message.fromID')
                            ->where('tr_message.fromID',$this->session->userdata('user_id'))
                            ->where('tr_message.ID',$id)
                            ->where('tr_message.hidefrom',0)
                            ->where('tr_message.type','pm')
                            ->get();
            return $query->row();
        }
        
        function update_read_message($id,$data)
        {
            $this->db->where('ID', $id);
		$this->db->update('tr_message', $data);
        }
        
        function get_count_unread_messages()
	{
		$count=$this->db->select('*')
			->from('tr_message')
			->where('toID', $this->session->userdata('user_id'))
			->where('isread', "0")
			->count_all_results();
		
		return $count;
	}
        
        function get_count_messages()
	{
		$count=$this->db->select('*')
			->from('tr_message')
			->where('toID', $this->session->userdata('user_id'))
                        ->where('type','pm')
			->count_all_results();
		
		return $count;
	}
        
        function get_count_sent_messages()
	{
		$count=$this->db->select('*')
			->from('tr_message')
			->where('fromID', $this->session->userdata('user_id'))
                        ->where('type','pm')
			->count_all_results();
		
		return $count;
	}
        
        function get_count_trash_messages()
	{
		$count=$this->db->select('*')
			->from('tr_message')
			->where('fromID', $this->session->userdata('user_id'))
                        ->where('type','pm')
                        ->where('tr_message.hideto','1')
			->count_all_results();
		
		return $count;
	}
        
        function get_messages_list()
        {
            $query=$this->db->select('tr_message.ID, tr_message.isread, tr_message.timestamp, tr_message_content.subject, tr_message_content.message, tr_user_profile.up_name, tr_user_profile.up_lastname')
			->from('tr_message')
                        ->join('tr_user_profile', 'up_uid=tr_message.fromID')
                        ->join('tr_message_content', 'tr_message_content.ID=tr_message.message_contentID')
			->where('tr_message.toID', $this->session->userdata('user_id'))
                        ->where('tr_message.isTrash',0)
                        ->where('tr_message.hideto',0)
                        ->where('tr_message.type','pm')
                        ->order_by('tr_message.timestamp','desc')
			->get();
		return $query->result();
        }
        
        function get_sent_messages_list()
        {
            $query=$this->db->select('tr_message.ID, tr_message.isread, tr_message.timestamp, tr_message_content.subject, tr_message_content.message, tr_user_profile.up_name, tr_user_profile.up_lastname')
			->from('tr_message')
                        ->join('tr_user_profile', 'up_uid=tr_message.toID')
                        ->join('tr_message_content', 'tr_message_content.ID=tr_message.message_contentID')
			->where('tr_message.fromID', $this->session->userdata('user_id'))
                        ->where('tr_message.hidefrom',0)
                        ->where('tr_message.type','pm')
                        ->order_by('tr_message.timestamp','desc')
			->get();
		return $query->result();
        }
        
        function get_trash_messages_list()
        {
            $query=$this->db->select('tr_message.ID, tr_message.isread, tr_message.timestamp, tr_message_content.subject, tr_message_content.message, tr_user_profile.up_name, tr_user_profile.up_lastname')
			->from('tr_message')
                        ->join('tr_user_profile', 'up_uid=tr_message.fromID')
                        ->join('tr_message_content', 'tr_message_content.ID=tr_message.message_contentID')
			->where('tr_message.toID', $this->session->userdata('user_id'))
                        ->where('tr_message.type','pm')
                        ->where('tr_message.isTrash',1)
                        ->where('tr_message.hideto',0)
                        ->order_by('tr_message.timestamp','desc')
			->get();
		return $query->result();
        }
        
        function delete_msg_in($id)
        {
            $set=array(
                'isTrash'=>1
            );
            $this->db->where('ID', $id);
		$this->db->update('tr_message', $set);
        }
        
        function delete_msg_out($id)
        {
            $set=array(
                'hidefrom'=>1
            );
            $this->db->where('ID', $id);
		$this->db->update('tr_message', $set);
        }
        
        function delete_trash($id)
        {
            $set=array(
                'hideto'=>1
            );
            $this->db->where('ID', $id);
		$this->db->update('tr_message', $set);
        }
}
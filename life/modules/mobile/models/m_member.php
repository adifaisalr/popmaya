<?php
class M_Member extends CI_Model 
{
        var $id;
        
        function get_last_status()
        {
            $query=$this->db->select('tr_wall_item.ID, tr_wall_item.title, tr_wall_item.info, tr_wall_item.like, tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_lastname')
                        ->from('tr_wall_item')
                        ->join('tr_user_profile', 'up_uid=memberID')
                        ->order_by('tr_wall_item.ID','desc')
                        ->limit(1)
                        ->get();
            return $query->row();
        }
        
        function get_comment_status($id)
        {
            $query=$this->db->select('tr_wall_item_comment.ID, tr_wall_item_comment.comment, tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_lastname')
                        ->from('tr_wall_item_comment')
                        ->join('tr_user_profile', 'up_uid=memberID')
                        ->where('wall_itemID', $id)
                        ->order_by('tr_wall_item_comment.ID','asc')
                        ->get();
            return $query->result();
        }
        
	function getUserProfile($id)
	{
		$query=$this->db->select('tr_user_profile.*, tr_user_login.ul_email')
				->from('tr_user_profile')
				->join('tr_user_login', 'ul_id=up_uid')
				->where('up_uid', $id)
				->get();
		return $query->row();
	}
        
        function get_profile_path($id)
	{
		$query=$this->db->get_where('tr_user_login', array('ul_id'=>$id));
		$row=$query->row();
		$row=substr($row->ul_createdon, 0, 10);
		$path=str_replace('-', '/', $row);
		return $path;
	}
        
        function get_count_unread_note()
	{
		$count=$this->db->select('*')
			->from('tr_notification')
			->where('n_foruserid', $this->session->userdata('user_id'))
			->where('n_isread', "0")
			->count_all_results();
		
		return $count;
	}
        
        function get_count_unread_note_list()
	{
		$query=$this->db->select('tr_notification.*, tr_user_profile.up_name, tr_user_profile.up_lastname, dr_notification_type.nt_name')
			->from('tr_notification')
                        ->join('tr_user_profile', 'up_uid=n_fromuserid')
                        ->join('dr_notification_type', 'nt_id=n_type')
			->where('n_foruserid', $this->session->userdata('user_id'))
			->where('n_isread', "0")
                        ->order_by('n_datetime','desc')
                        ->limit(5)
			->get();
		return $query->result();
	}
        
        function get_count_all_note_list()
	{
		$query=$this->db->select('tr_notification.*, tr_user_profile.up_name, tr_user_profile.up_lastname, dr_notification_type.nt_name')
			->from('tr_notification')
                        ->join('tr_user_profile', 'up_uid=n_fromuserid')
                        ->join('dr_notification_type', 'nt_id=n_type')
			->where('n_foruserid', $this->session->userdata('user_id'))
			->get();
		return $query->result();
	}    
        
        /*
         * message's model
         */
        function get_count_unread_messages()
	{
		$count=$this->db->select('*')
			->from('tr_message')
			->where('toID', $this->session->userdata('user_id'))
			->where('isread', "0")
			->count_all_results();
		
		return $count;
	}
        
        function get_mysong_list()
        {
            $query=$this->db->select('*')
			->from('tr_song')
			->where('memberID', $this->session->userdata('user_id'))
                        ->get();
            return $query->result();
        }
        
        function get_count_post_type($id,$type)
        {
            $count=$this->db->from('tr_wall_item')
			->where('memberID', $id)
                        ->where('type',$type)
			->count_all_results();
		
		return $count;
        }
        
        function get_count_photos($id)
	{
		$count=$this->db->from('tr_album')
			->where('memberID', $id)
			->count_all_results();
		
		return $count;
	}
        
        
        function get_count_songs($id)
	{
		$count=$this->db->select('*')
			->from('tr_fave_song')
			->where('memberID', $id)
			->count_all_results();
		
		return $count;
	}
        
        function get_email_address($email)
        {
            $query=$this->db->select('ul_id, ul_email')
                        ->from('tr_user_login')
                        ->where('ul_email',$email)
                        ->get();
            return $query->row();
        }
        
        function get_phone_number($phone)
        {
            $query=$this->db->select('tr_user_login.ul_id, tr_user_login.ul_email')
                        ->from('tr_user_profile')
                        ->join('tr_user_login', 'ul_id=	up_uid')
                        ->where('tr_user_profile.up_mobile',$phone)
                        ->get();
            return $query->row();
        }
        
        function add_user_invite($input)
        {
            $this->db->insert('tr_user_invited', $input);
            return mysql_insert_id();
        }
        
        function add_invite_email($input,$toID)
        {
            $invite=$this->add_user_invite($input);
            $data=array(
                'n_type'=>8,
                'n_itemid'=>$invite,
                'n_fromuserid'=>$this->session->userdata('user_id'),
                'n_foruserid'=>$toID
            );
            $this->db->insert('tr_notification', $data);
        }
        
        function get_photo_challenge()
        {
            $query=$this->db->select('tr_photo_challenge.*, tr_photo.title, tr_photo.image, tr_photo.description, tr_user_profile.up_name, tr_user_profile.up_lastname')
				->from('tr_photo_challenge')
				->join('tr_photo', 'tr_photo.ID=photoID')
                                ->join('tr_user_profile', 'up_uid=tr_photo.memberID')
                                ->where('tr_photo_challenge.status','1')
				->order_by('tr_photo_challenge.ID', 'desc')
                                ->limit(1)
				->get();
		return $query->row();
        }     
        
        function get_notif($id)
        {
            $query=$this->db->select('tr_notification.*, tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_lastname, dr_notification_type.nt_name')
                            ->from('tr_notification')
                            ->join('tr_user_profile', 'up_uid=n_fromuserid')
                            ->join('dr_notification_type', 'nt_id=n_type')
                            ->where('n_id',$id)
                            ->where('n_foruserid',$this->session->userdata('user_id'))
                            ->get();
            return $query->row();
        }
        
        function update_notif($id)
        {
            $notif=array(
                    'n_isread'=>1
                );
            $this->db->where('n_id', $id);
                    $this->db->update('tr_notification', $notif);
        }
        
        /*
         * friend's model
         */   
        function get_count_friend($id)
	{
		$count=$this->db->select('*')
			->from('tr_friend')
			->where('f_userid', $id)
			->where('f_isactive', "1")
			->count_all_results();
		
		return $count;
	}
        
        function get_friend($id)
        {
            $query=$this->db->select('*')
                            ->from('tr_friend')
                            ->where('f_userid',$this->session->userdata('user_id'))
                            ->where('f_friendid',$id)
                            ->get();
            return $query->row();
        }
        
        function get_all_friend()
        {
            $query=$this->db->select('f_friendid')
                            ->from('tr_friend')
                            ->where('f_userid',$this->session->userdata('user_id'))
                            ->get();
            //return $query->result();
            foreach ($query->result() as $row)
            {
               return $row->f_friendid;
            }
        }
        
        function get_a_member()
        {
            $friends=$this->get_all_friend();
            $query=$this->db->select('a.*, c.ul_email')
                            ->from('tr_user_profile a')
                            ->join('tr_user_login c', 'a.up_uid = c.ul_id', 'left')
                            ->where('a.up_uid !=',$this->session->userdata('user_id'))
                            ->where_not_in('a.up_uid',$friends)
                            ->limit(1)
                            ->get();
            return $query->row();
        }
        
        function get_friend_invited($email)
        {
            $query=$this->db->select('*')
                            ->from('tr_user_invited')
                            ->where('i_userid',$this->session->userdata('user_id'))
                            ->where('i_email',$email)
                            ->get();
            return $query->row();
        }
        
        function get_recommendation($id, $limit)
	{
            //$offset=  $this->get_random_recomended();
            $this->db->select('a.*, c.ul_email');
	    $this->db->from('tr_user_profile a');
            $this->db->join('tr_user_login c', 'a.up_uid = c.ul_id', 'left');
            $this->db->where('a.up_uid !=',$this->session->userdata('user_id'));
		$this->db->limit(10);
		$query = $this->db->get();
	    return $query->result();	
	}
        
        function get_random_recommended()
        {
            $friends=  $this->get_all_friend();
            $query=$this->db->count_all_results('tr_user_profile');
            do{
                $var=mt_rand(1, $query);
            }
            while(in_array($var,$friends,false));
            return $var;
        }
}
?>
<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>popmaya</title>
  <link  href="<?= site_url() ?>assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
  <meta name="description" content="popmaya � sosial network musik indonesia" />
  <meta name="keywords" content="Biggest sound based social media" />
  <meta name="author" content="www.theideafield.com" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <?= css_asset('reset.css'); ?>
  <?= css_asset('text.css'); ?>
  <?= css_asset('grid12.css'); ?>
  <?= css_asset('style.css'); ?>
  <?= css_asset('tango/skin.css'); ?>
</head>
<body class="general">
<div id="header_general">
  <div class="container_12 main-header">
      <div class="grid_4 logo">
        
          <a href="<?= site_url(); ?>"><?= image_asset('/general/pop_maya_logo.png', '',array('alt'=>'Popmaya', 'width'=>'70%')) ?></a>
        
      </div>
      <div class="grid_4 pad_t_10">
		<!--?= image_asset('general/layout/soc_login-.jpg'); ?-->
		<div class="tit">Login With </div>
		<!-- social network login -->
		<?php
	    $fb_target=base_url()."member/facebook_login";
	    $fb_login="http://www.facebook.com/connect/uiserver.php?method=permissions.request&app_id=".$fb_appid."&display=page&redirect_uri=".urlencode($fb_target)."&perms=read_stream,publish_stream,email,offline_access&response_type=code&fbconnect=1&from_login=1";
		?>
		<a href="<?php echo $fb_login; ?>"><div class="home-facebook"></div></a>
		<!--<?= anchor($fb_login, "Facebook"); ?>-->
		
		<a href="<?php echo base_url().'member/twitter_login'; ?>"><div class="home-twitter"></div></a>
		<!--<?= anchor(base_url()."member/twitter_login", "Twitter"); ?>-->
		<!-- end social network login -->		
		
	  </div>
      <div class="grid_4 pad_t_10"> 
	
		<!-- login -->
		<?php if($this->session->flashdata('login_error')):?>
			<?= $this->session->flashdata('login_error'); ?>
		<?php endif;?>

		<?= form_open(base_url()."member/login"); ?>

			<?= form_input(array('class'=>"input-text",'id'=>"email", 'name'=>"email", 'placeholder'=>"Email", 'tabindex'=>'1')); ?>
			<?= form_password(array('class'=>"input-text",'id'=>"password", 'name'=>"password", 'placeholder'=>"Password", 'tabindex'=>'2')); ?>

			<!--<?= form_submit('signin', "Login", array('tabindex'=>'3', 'class'=>"login-home")); ?>-->
			<input type="submit" name="signin" value="Login" class="login-home" tabindex="3" />
			
			<span class="remember_me">
			<?= form_checkbox(array('name'=>"remember", 'id'=>"remember", 'value'=>true, 'checked'=>false, 'title'=>"Remember Me", 'tabindex'=>'4')); ?> Keep me sign in
			<?= anchor(base_url()."home/forgot", "Forgot Password", array('class'=>'forgot_password')); ?>
			</span>

		<?= form_close(); ?>
		<!-- end login -->

      </div>      
  </div>
  <div class="clear"></div>
</div>
	<div class="grid_3 bg_forgot"></div>
  <div class="container_12" style="position: relative;">
    <div id="formForgot" class="grid_6" style="position: absolute; right: 75px;">
	<div class="forgotHeader">
		<span class="grid_5 titleForgotform"><strong>
		<?php if($this->session->flashdata('message')):?>
			<?= $this->session->flashdata('message'); ?>
		<?php endif;?>
		
		</strong></span>
	</div>
	<div class="forgotBody">
	<!--<b class="xtop" style="margin-top:20px;">
		<b class="xb1"></b>
		<b class="xb2"></b>
		<b class="xb3"></b>
		<b class="xb4"></b>
	</b>-->
	<div class="outLine" style="width: 317px;">
		Before we can reset your password, you need to enter the information below to help identify your account:
	</div>
	<!--<b class="xbottom" style="margin-bottom:20px;">
		<b class="xb4"></b>
		<b class="xb3"></b>
		<b class="xb2"></b>
		<b class="xb1"></b>
	</b>-->
	<!-- register -->
	<div class="grid_4" style="margin-left: 90px;">
	<?= form_open(base_url()."home/forgot_process"); ?>
		<label for="description">Enter your email or your phone number.</label>
		<div style="margin-left: 10px;">
		<?= image_asset('general/layout/amplop.png','',array('class'=>'amplop')); ?>
		<input class="input-text" style="width: 150px; border: 1px solid #D0D0D0;" type="text" name="email" placeholder="Email or Phone Number"/>
		</div>
	<!-- end register -->		
	</div>
      <br class="clear"/> 
    </div>
	<div class="forgotFooter">
		<input type="submit" value="send my password" name="submit" class="bt_forgot"/>
	<?= form_close(); ?>
	</div>
    
  </div>
  <div class="clear">&nbsp;</div>
</div>
	
<div class="container_12" style="margin-bottom: 200px">  <!--update by gilangaramadan 02Jan12 biar footernya di bawah-->
  <div class="grid_12">&nbsp;</div>
  <div class="clear">&nbsp;</div>
</div>

<div class="bg_footer">
<div class="container_12">  
  <div class="grid_3">Popmaya &copy; 2011</div>
  <div class="grid_9 align_r">
    <?= anchor('en', 'English(US)'); ?> &middot; 
    <?= anchor('mobile', 'Mobile'); ?> &middot; 
    <?= anchor('search/friends', 'Find Friends'); ?> &middot; 
    <?= anchor('page/badges', 'Badges'); ?> &middot;
    <?= anchor('member/list', 'People'); ?> &middot; 
    <?= anchor('page/about', 'About'); ?> &middot; 
    <?= anchor('page/advertising', 'Advertising'); ?> &middot; 
    <?= anchor('page/create_page', 'Create a page'); ?> &middot; 
    <?= anchor('page/developer', 'Developers'); ?> &middot; 
    <?= anchor('page/career', 'Careers'); ?> &middot; 
    <?= anchor('page/privacy', 'Privacy'); ?> &middot; 
    <?= anchor('page/terms', 'Terms'); ?> &middot; 
    <?= anchor('page/help', 'Help'); ?> 
  </div>
  <div class="clear">&nbsp;</div>
</div> 
</div>

<!-- end .container_12 -->
<br class="clear"/>
</body></html>
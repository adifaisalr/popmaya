<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN"
"http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $head_title ?></title>
    <meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
    <?php echo css_asset('mobile-style.css'); ?>
</head>
<body>
    <div id="header"><?php echo image_asset('/mobile/logo.png', '',array('alt'=>'Popmaya-mobile')) ?></div>
    <div id="content">
        <?php echo $profile_menu ?>
        <?php echo $middle_content;?>
        <br class="clear" />
    </div>
</body>
</html>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?= $head_title ?></title>
  <meta name="description" content="">
  <meta name="author" content="">

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- CSS concatenated and minified via ant build script-->
  <?= css_asset('style.css'); ?>
  <!-- end CSS-->

</head>
<body>

  <div id="cms_admin">
    <div id="cms_l">
      <ul>
        <li><?= anchor(site_url().'cms/dashboard', 'Dashboard'); ?></li>
        <li><?= anchor('album/manage', 'Track Album'); ?></li>
        <li><?= anchor('track/manage', 'Track'); ?></li>
        <li><?= anchor('track/manage/recommendation', 'Track Recommendation'); ?></li>
        <li><?= anchor('member/manage/list', 'Member'); ?></li>
        <li><?= anchor('member/manage/post_status', 'Status'); ?></li>
        <li><?= anchor('pages/list', 'Pages'); ?></li>
        <li><?= anchor('cms/change_password', 'Change Password'); ?></li>
      </ul>
    </div>

    <div id="cms_r">
    <?= display_flash('warn'); ?>
      <span class="top_bar"><?php
        switch($this->uri->segment(2)){
          case 'dashboard' : $ttl='Home'; break;
          case 'about' : $ttl='About Us'; break;
          case 'event' : $ttl='Events'; break;
          case 'merchant' : $ttl='Merchant List'; break;
          case 'contact' : $ttl='Contact Us'; break;
          case 'change_password' : $ttl='Change Password'; break;
        }
//        echo var_dump($this->session->userdata);
        echo '<h2>'.$ttl.'</h2>';
        if($this->session->userdata('user_id')){ 
          echo anchor(site_url().'cms/logout', 'Log out ('.$this->session->userdata('ul_email').')', array('class'=>'logout'));
        } ?>
      </span>
      <div id="cms_content"><?= $content; ?></div>
    </div>
    <br class="clear"/>
  </div>

	<?/* js_asset('modernizr.custom.92644.js'); */?>
  <?= js_asset('jquery-1.7.min.js'); ?>

  <?php 
      if($this->session->flashdata('warn')){
        $warn=$this->session->flashdata('warn');
         if($warn['type']=='notice'){$tipe_warn='notice'; }else{ $tipe_warn='success'; }
         $warn_js='$(\'#'.$tipe_warn.'\').delay(2500).slideUp(500);';
     }
  ?>

  <script type="text/javascript">
    $.fn.ready(function(){  // some code
      <?= $warn_js ?>
      <?= $foot_js ?>
        $(".delConf").click(function(){
          var url = $(this).attr("href");
          if(confirm("Anda yakin ingin menghapus?")) {
            location.href=url;
          } else {
            return false;
          }
      });
    });
  </script>
	<!--
  <script> // Change UA-XXXXX-X to be your site's ID
    window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
    Modernizr.load({
      load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
    });
  </script>
-->

  <!--[if lt IE 7 ]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->
  
</body>
</html>

 <div id="sidebar">
       <div class="profile-picture">
          <?= image_asset('profile-pict.jpg', '', array('alt'=>'profile')) ?>
       </div>
       <div class="profiler">
           <h2>Alam Mavalswe</h2>
           <span>Male</span>
           <span>Bandung - Indonesia</span>
       </div>  
       <div class="profile-link">
           <ul>
               <li><a href="#" class="info active" title="Info">Info</a></li>
               <li><a href="#" class="status" title="Status">Status</a></li>
               <li><a href="#" class="voice" title="Voice">Voice</a></li>
               <li><a href="#" class="photo" title="Photo">Photo</a></li>
               <li><a href="#" class="video" title="Video">Video</a></li>
               <li><a href="#" class="note" title="Note">Note</a></li>
           </ul>
       </div>
       <div class="list-left">
           <h3>My Song List</h3>
           <ul>
               <li>
                   <a href="#"><?= image_asset('thumb_song_list1.jpg', '', array('alt'=>'ERK', 'class'=>'thumb-list')) ?></a>
                   <h4><a href="#">ERK</a></h4>
                   <span>Dibuang Sayang</span>
                   <div class="button-share">
                      <a href="#" title="Play"><?= image_asset('play_ico.png', '', array('alt'=>'play')) ?></a>
                      <a href="#" title="Download"><?= image_asset('download_ico.png', '', array('alt'=>'download')) ?></a>
                      <a href="#" title="Share"><?= image_asset('share_ico.png', '', array('alt'=>'share')) ?></a>
                      <a href="#" class="last" title="Add"><?= image_asset('add_ico.png', '', array('alt'=>'add')) ?></a>
                   </div>
               </li>
               <li>
                   <a href="#"><?= image_asset('thumb_song_list2.jpg', '', array('alt'=>'Paramore', 'class'=>'thumb-list')) ?></a>
                   <h4><a href="#">Paramore</a></h4>
                   <span>The Only Exception</span>
                   <div class="button-share">
                       <a href="#" title="Play"><?= image_asset('play_ico.png', '', array('alt'=>'play')) ?></a>
                       <a href="#" title="Download"><?= image_asset('download_ico.png', '', array('alt'=>'download')) ?></a>
                       <a href="#" title="Share"><?= image_asset('share_ico.png', '', array('alt'=>'share')) ?></a>
                       <a href="#" class="last" title="Add"><?= image_asset('add_ico.png', '', array('alt'=>'add')) ?></a>
                    </div>
               </li>
               <li>
                   <a href="#"><?= image_asset('thumb_song_list3.jpg', '', array('alt'=>'Cold Play', 'class'=>'thumb-list')) ?></a>
                   <h4><a href="#">Cold Play</a></h4>
                   <span>Fix You</span>
                   <div class="button-share">
                       <a href="#" title="Play"><?= image_asset('play_ico.png', '', array('alt'=>'play')) ?></a>
                       <a href="#" title="Download"><?= image_asset('download_ico.png', '', array('alt'=>'download')) ?></a>
                       <a href="#" title="Share"><?= image_asset('share_ico.png', '', array('alt'=>'share')) ?></a>
                       <a href="#" class="last" title="Add"><?= image_asset('add_ico.png', '', array('alt'=>'add')) ?></a>
                    </div>
               </li>
               <li>
                   <a href="#"><?= image_asset('thumb_song_list4.jpg', '', array('alt'=>'Utopia', 'class'=>'thumb-list')) ?></a>
                   <h4><a href="#">Uthopia</a></h4>
                   <span>Hujan</span>
                   <div class="button-share">
                        <a href="#" title="Play"><?= image_asset('play_ico.png', '', array('alt'=>'play')) ?></a>
                        <a href="#" title="Download"><?= image_asset('download_ico.png', '', array('alt'=>'download')) ?></a>
                        <a href="#" title="Share"><?= image_asset('share_ico.png', '', array('alt'=>'share')) ?></a>
                        <a href="#" class="last" title="Add"><?= image_asset('add_ico.png', '', array('alt'=>'add')) ?></a>
                     </div>
               </li>
               <li>
                  <a href="#"><?= image_asset('thumb_song_list5.jpg', '', array('alt'=>'SID', 'class'=>'thumb-list')) ?></a>
                  <h4><a href="#">SID</a></h4>
                  <span>Kuat Kita Bersinar</span>
                  <div class="button-share">
                     <a href="#" title="Play"><?= image_asset('play_ico.png', '', array('alt'=>'play')) ?></a>
                     <a href="#" title="Download"><?= image_asset('download_ico.png', '', array('alt'=>'download')) ?></a>
                     <a href="#" title="Share"><?= image_asset('share_ico.png', '', array('alt'=>'share')) ?></a>
                     <a href="#" class="last" title="Add"><?= image_asset('add_ico.png', '', array('alt'=>'add')) ?></a>
                  </div>
               </li>
           </ul>
           <span><?= anchor('#', 'More song from this album', array('title'=>'More song', 'class'=>'more')); ?></span>
           <div class="clear"></div>
       </div>
       <div class="list-left">
           <h3>My Friend List</h3>
           <ul>
               <li>
                  <?= anchor('#', image_asset('thumb_f_list1.jpg', '',array('alt'=>'Wawan', 'class'=>'thumb-list'))); ?>
                   <h4><a href="#">Wawan K</a></h4>
                   <span>Bandung</span>
               </li>
               <li>
                   <?= anchor('#', image_asset('thumb_f_list2.jpg', '',array('alt'=>'Wawan', 'class'=>'thumb-list'))); ?>
                   <h4><a href="#">Ferin</a></h4>
                   <span>Bandung</span>
               </li>
               <li>
                   <?= anchor('#', image_asset('thumb_f_list3.jpg', '',array('alt'=>'Wawan', 'class'=>'thumb-list'))); ?>
                   <h4><a href="#">Akbar</a></h4>
                   <span>Bandung</span>
               </li>
           </ul>
           <span><?= anchor('#', 'View All', array('title'=>'View all', 'class'=>'more'))?></span>
           <div class="clear"></div>
       </div>
       <div class="list-left">
           <h3>My Brand List</h3>
           <ul>
               <li>
                  <?= anchor('#', image_asset('thumb_b_list1.jpg', '',array('alt'=>'Linkin Park', 'class'=>'thumb-list'))); ?>
                  <h4><a href="#">Linkin Park</a></h4>
               </li>
               <li>
                  <?= anchor('#', image_asset('thumb_b_list2.jpg', '',array('alt'=>'Mariah Carey', 'class'=>'thumb-list'))); ?>
                  <h4><a href="#">Mariah Carey</a></h4>
               </li>
               <li>
                  <?= anchor('#', image_asset('thumb_b_list3.jpg', '',array('alt'=>'Dave Grohl', 'class'=>'thumb-list'))); ?>
                  <h4><a href="#">Dave Grohl</a></h4>
               </li>
           </ul>
           <span><?= anchor('#', 'View All', array('title'=>'View all', 'class'=>'more')); ?></span>
           <div class="clear"></div>
       </div>
       <div class="list-left">
           <h3>My Brand List</h3>
           <p class="bottom">Likes of</p>
           <p class="bottom">Your List is Empty.<br/><?= anchor('#', 'Find Your Fav Brand Here', array('title'=>'Find Brand')); ?></p>
       </div>
   </div>
